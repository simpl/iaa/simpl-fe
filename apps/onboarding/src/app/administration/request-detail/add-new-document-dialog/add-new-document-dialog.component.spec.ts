import { TestBed, ComponentFixture, fakeAsync, tick } from '@angular/core/testing';
import { AddNewDocumentDialogComponent, ConfirmDialogNewDocumentData } from './add-new-document-dialog.component';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormBuilder } from '@angular/forms';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { By } from '@angular/platform-browser';
import { of } from 'rxjs';
import { OnboardingProceduresManagementService } from '../../onboarding-procedures-management/onboarding-procedures-management.service';
import { MIMEType } from '@fe-simpl/api-types';
import { TranslocoTestingModule } from '@jsverse/transloco';
import { AppDomain, translocoTestConfiguration } from '@test-environment';

describe('AddNewDocumentDialogComponent (Jest)', () => {
  let component: AddNewDocumentDialogComponent;
  let fixture: ComponentFixture<AddNewDocumentDialogComponent>;
  let dialogRefSpy: Partial<MatDialogRef<AddNewDocumentDialogComponent>>;

  let mockOnboardingService: Partial<OnboardingProceduresManagementService>;

  const mockData: ConfirmDialogNewDocumentData = {
    title: 'Mock Title',
    message: 'Mock Message',
  };

  const mockMimeTypes: MIMEType[] = [
    { id: 1, value: 'application/pdf', description: "PDF" },
    { id: 2, value: 'application/json', description: "JSON" },
    { id: 3, value: 'text/plain', description: "TEXT" }
  ];

  beforeEach(async () => {
    dialogRefSpy = {
      close: jest.fn()
    };

    mockOnboardingService = {
      getMIMETypeList: jest.fn().mockReturnValue(of(mockMimeTypes))
    };

    await TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        AddNewDocumentDialogComponent,
        TranslocoTestingModule.forRoot(
					translocoTestConfiguration(AppDomain.ONBOARDING)
        ),
      ],
      providers: [
        { provide: MAT_DIALOG_DATA, useValue: mockData },
        { provide: MatDialogRef, useValue: dialogRefSpy },
        { provide: OnboardingProceduresManagementService, useValue: mockOnboardingService },
        FormBuilder
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNewDocumentDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the dialog component', () => {
    expect(component).toBeTruthy();
  });

  it('should render title and message from data', () => {
    const titleEl = fixture.debugElement.query(By.css('h1[mat-dialog-title]')).nativeElement;
    const messageEl = fixture.debugElement.query(By.css('div[mat-dialog-content] p')).nativeElement;

    expect(titleEl.textContent).toContain(mockData.title);
    expect(messageEl.textContent).toContain(mockData.message);
  });

  it('should initialize the form with empty fields', () => {
    expect(component.form.value).toEqual({
      description: '',
      mandatory: true,
      mimeType: ''
    });
  });

  it('should load MIME types on init', () => {
    expect(mockOnboardingService.getMIMETypeList).toHaveBeenCalled();
    expect(component.mimeTypes).toEqual(mockMimeTypes);
  });

  it('should filter MIME types when user types', fakeAsync(() => {
    const inputEl = fixture.debugElement.query(By.css('input[formControlName="mimeType"]')).nativeElement;

    inputEl.value = 'application/p';  
    inputEl.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    tick();

    expect(component.filteredMimeTypes.length).toBe(1);
    expect(component.filteredMimeTypes[0].value).toBe('application/pdf');
  }));

  it('should close dialog on close button click', fakeAsync(() => {
    const buttons = fixture.debugElement.queryAll(By.css('button'));
    const confirmBtn = buttons[1];
    
    confirmBtn.triggerEventHandler('click', null);
    fixture.detectChanges();
    tick();

    expect(dialogRefSpy.close).toHaveBeenCalled();
  }));

  it('should close with form data when pressing confirm if valid', fakeAsync(() => {
    component.form.patchValue({
      description: 'Document description',
      mimeType: mockMimeTypes[0] 
    });
    fixture.detectChanges();
    expect(component.form.valid).toBe(true);

    const confirmBtn = fixture.debugElement.queryAll(By.css('button'))[1];
    confirmBtn.triggerEventHandler('click', null);
    fixture.detectChanges();
    tick();

    expect(dialogRefSpy.close).toHaveBeenCalledWith({
      description: 'Document description',
      mandatory: true,
      mimeType: { "id": 1, value: 'application/pdf', description: 'PDF' }
    });
  }));

  it('should close with null when pressing cancel', () => {
    const cancelBtn = fixture.debugElement.queryAll(By.css('button'))[0];
    cancelBtn.triggerEventHandler('click', null);
    fixture.detectChanges();

    expect(dialogRefSpy.close).toHaveBeenCalledWith(null);
  });
});
