import { Component, computed, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButton } from '@angular/material/button';
import { MatFormField } from '@angular/material/form-field';
import { MatIcon } from '@angular/material/icon';
import { MatInput } from '@angular/material/input';
import { MatMenu, MatMenuModule } from '@angular/material/menu';
import { MatOption } from '@angular/material/autocomplete';
import { MatSelect, MatSelectChange } from '@angular/material/select';
import {
  FormsModule,
  ReactiveFormsModule,
} from '@angular/forms';
import { OnboardingRequestStatus } from '@fe-simpl/api-types';
import { TranslocoDirective } from '@jsverse/transloco';
import { RequestListStore } from '@fe-simpl/shared/requests-store';

@Component({
  selector: 'app-filters-list',
  standalone: true,
  imports: [CommonModule, MatButton, FormsModule,  MatFormField, MatIcon, MatInput, MatMenu, MatOption, MatSelect, MatMenuModule, ReactiveFormsModule, TranslocoDirective, MatButton],
  templateUrl: './filters-list.component.html',
  styleUrl: './filters-list.component.css',
})
export class FiltersListComponent{
  statuses = Object.keys(OnboardingRequestStatus).map((key) => ({
    value: key,
    label: OnboardingRequestStatus[key as keyof typeof OnboardingRequestStatus]
  }));
  private readonly requestListStore = inject(RequestListStore);
  status = this.requestListStore.filters.status;
  email = this.requestListStore.filters.email;
  selectedFiltersLabel: string = "";
  protected filledFieldsCount = 0;

  protected readonly viewMode = computed(() => ({
    status: this.status(),
    email: this.email()
  }));

  get vm(): ReturnType<typeof this.viewMode> {
    return this.viewMode();
  }

  filterValue: 'email' | 'status' | null

  onStatusChange($event: MatSelectChange)  {
    this.requestListStore.updateFilters($event?.value ? {status: $event.value} : {status: null});
  }

  onMailChange($event: any)  {
    this.requestListStore.updateFilters($event?.target?.value ? {email: $event.target.value} : {email: null});
  }

  filter() {
    this.requestListStore.updateFilters({ page: 0 });
    this.requestListStore.getList()
    this.selectedFiltersUpdate();
  }

  selectedFiltersUpdate(){
    this.filledFieldsCount = 0;
    let verboseFilters = "";
    if(this.email()) this.filledFieldsCount += 1;
    if(this.status()) this.filledFieldsCount += 1;

    if(this.email() && this.email() !== "") {
      verboseFilters += "Email: ";
      verboseFilters += this.email() +"; ";
    }

    if(this.status() && this.status() !== "") {
      verboseFilters += "Status: ";
      verboseFilters += OnboardingRequestStatus[this.status() as keyof typeof OnboardingRequestStatus]  +"; ";
    }

    this.selectedFiltersLabel = verboseFilters;
  }

  get filledFields(): number {
    return this.filledFieldsCount;
  }

  reset(){
    this.filterValue = null
    this.requestListStore.reset();
    this.filter()
  }

}
