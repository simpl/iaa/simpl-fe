import { ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';
import { FiltersListComponent } from './filters-list.component';
import { By } from '@angular/platform-browser';
import { OnboardingRequestStatus, ParticipantDto } from '@fe-simpl/api-types';
import { of } from 'rxjs';
import { provideHttpClient } from "@angular/common/http";
import { TranslocoTestingModule } from "@jsverse/transloco";
import { AppDomain, provideApiUrl, translocoTestConfiguration } from "test-environment";
import { NoopAnimationsModule } from "@angular/platform-browser/animations";
import { RequestListStore } from '@fe-simpl/shared/requests-store';

describe('FiltersListComponent', () => {
  let component: FiltersListComponent;
  let fixture: ComponentFixture<FiltersListComponent>;
  let requestListStore: any;
  let searchSpy: jest.SpyInstance;

  const mockList: Array<ParticipantDto> = []

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        FiltersListComponent,
        TranslocoTestingModule.forRoot(
          translocoTestConfiguration(AppDomain.ONBOARDING)
        ),
        NoopAnimationsModule
      ],
      providers: [
        provideHttpClient(), provideApiUrl
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(FiltersListComponent);
    component = fixture.componentInstance;
    requestListStore = TestBed.inject(RequestListStore);
    fixture.detectChanges();
    searchSpy = jest
      .spyOn(requestListStore, "getList")
      .mockReturnValue(
        of(mockList)
      );
  });

  const openFiltersPanelAndSelectColumn = (fixture: ComponentFixture<FiltersListComponent>, column: string) => {
    const filterButton = fixture.debugElement.query(By.css('#filtersMenuButton'));
    filterButton.nativeElement.click();
    fixture.detectChanges();
    const select = fixture.debugElement.query(By.css('#filterColumn'));
    select.nativeElement.click();
    fixture.detectChanges();
    const columnOptions = fixture.debugElement.queryAll(By.css('mat-option'));
    const columnOption = columnOptions.find(option => option.nativeElement.textContent.trim() === column);
    expect(columnOption).toBeTruthy();
    columnOption?.nativeElement.click();
    fixture.detectChanges();
  };
  const insertFilterValue = (fixture: ComponentFixture<FiltersListComponent>, value: string) => {
    const input = fixture.debugElement.query(By.css('#filterEmail'))?.nativeElement;
    input.value = value;
    fixture.detectChanges();
  };

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render elements', fakeAsync(() => {
    const filtersMenuButton = fixture.debugElement.query(By.css('#filtersMenuButton'));
    expect(filtersMenuButton).not.toBeNull();

    const filterButton = fixture.debugElement.query(By.css('#filtersMenuButton'));
    filterButton.nativeElement.click();
    fixture.detectChanges();
    const select = fixture.debugElement.query(By.css('#filterColumn'));
    select.nativeElement.click();
    fixture.detectChanges();

    const titles = fixture.debugElement.queryAll(By.css('mat-option'));
    expect(titles.length).toBeGreaterThan(0);

    const expectedFilterOptions: Array<string> = [
      'email',
      'status'
    ];

    titles.forEach((title, index) => {
      const cleanTitle = title.nativeElement.textContent.trim();
      expect(cleanTitle).toBe(expectedFilterOptions[index]);
    });

    const filtersApplyButton = fixture.debugElement.query(By.css('#filtersApply'));
    const filtersResetButton = fixture.debugElement.query(By.css('#filtersReset'));
    expect(filtersApplyButton).not.toBeNull();
    expect(filtersResetButton).not.toBeNull();
  }));

  it('should open up the filters panel, select a filter and apply to search', () => {
    openFiltersPanelAndSelectColumn(fixture, 'email');
    insertFilterValue(fixture, 'mail@aruba.it');
    const applyButton = fixture.debugElement.query(By.css('#filtersApply'));
    applyButton.nativeElement.click();
    fixture.detectChanges();
    expect(component.filterValue).toBe('email');
    expect(searchSpy).toHaveBeenCalled();
  });

  it('should reset filters inserted', () => {
    openFiltersPanelAndSelectColumn(fixture, 'email');
    insertFilterValue(fixture, 'mail@aruba.it');
    const resetButton = fixture.debugElement.query(By.css('#filtersReset'));
    resetButton.triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(component.filterValue).toBe(null);
    expect(searchSpy).toHaveBeenCalled();
  });

  it('should update status filter on status change', () => {
    const mockEvent = { value: 'APPROVED' } as any;
    const updateFiltersSpy = jest.spyOn(requestListStore, 'updateFilters');
    component.onStatusChange(mockEvent);
    expect(updateFiltersSpy).toHaveBeenCalledWith({ status: 'APPROVED' });
  });

  it('should update email filter on email change', () => {
    const mockEvent = { target: { value: 'test@mail.com' } } as any;
    const updateFiltersSpy = jest.spyOn(requestListStore, 'updateFilters');
    component.onMailChange(mockEvent);
    expect(updateFiltersSpy).toHaveBeenCalledWith({ email: 'test@mail.com' });
  });

  it('should apply filters and call getList on filter', () => {
    const updateFiltersSpy = jest.spyOn(requestListStore, 'updateFilters');
    component.filter();
    expect(updateFiltersSpy).toHaveBeenCalledWith({ page: 0 });
    expect(searchSpy).toHaveBeenCalled();
  });

  it('should reset all filters and call filter', () => {
    const resetSpy = jest.spyOn(requestListStore, 'reset');
    const filterSpy = jest.spyOn(component, 'filter');
    component.reset();
    expect(resetSpy).toHaveBeenCalled();
    expect(filterSpy).toHaveBeenCalled();
    expect(component.filterValue).toBeNull();
  });

  it('should map statuses correctly', () => {
    expect(component.statuses).toEqual(
      Object.keys(OnboardingRequestStatus).map((key) => ({
        value: key,
        label: OnboardingRequestStatus[key as keyof typeof OnboardingRequestStatus]
      }))
    );
  });
});
