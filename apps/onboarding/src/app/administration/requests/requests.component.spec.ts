import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RequestsComponent } from './requests.component';
import { TranslocoTestingModule } from '@jsverse/transloco';
import { NoopAnimationsModule } from "@angular/platform-browser/animations";
import { KeycloakService } from 'keycloak-angular';
import { provideHttpClient } from '@angular/common/http';
import { AppDomain, provideApiUrl, translocoTestConfiguration } from "test-environment";
import enTranslations from '../../../assets/i18n/en.json'

describe('RequestsComponent', () => {
  let component: RequestsComponent;
  let fixture: ComponentFixture<RequestsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RequestsComponent,
        TranslocoTestingModule.forRoot(
          translocoTestConfiguration(AppDomain.ONBOARDING)
        ),
        NoopAnimationsModule
      ],
      providers: [KeycloakService, provideApiUrl, provideHttpClient()],
    }).compileComponents();

    fixture = TestBed.createComponent(RequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it("should render the component correctly", () => {
    fixture.detectChanges();

    const compiled = fixture.nativeElement as HTMLElement;
    expect(compiled.querySelector('h1')?.textContent).toBe(enTranslations.administration.title);
  });
});
