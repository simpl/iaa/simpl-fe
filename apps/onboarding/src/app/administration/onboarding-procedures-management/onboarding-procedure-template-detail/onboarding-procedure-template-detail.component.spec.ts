import { ComponentFixture, TestBed } from '@angular/core/testing';
import { OnboardingProcedureTemplateDetailComponent } from './onboarding-procedure-template-detail.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { TranslocoModule, TranslocoTestingModule } from '@jsverse/transloco';
import { translocoTestConfiguration, AppDomain } from '@test-environment';
import { OnboardingProcedureTemplate } from '@fe-simpl/api-types';

describe('OnboardingProcedureTemplateDetailComponent', () => {
  let component: OnboardingProcedureTemplateDetailComponent;
  let fixture: ComponentFixture<OnboardingProcedureTemplateDetailComponent>;

  const mockTemplate = {
    description: 'Test Template Description',
    expirationTimeframe: 3000,
    documentTemplates: [
      {
        description: 'Authorization',
        mimeType: { value: 'application/pdf' },
        mandatory: true,
      },
      {
        description: 'Passport',
        mimeType: { value: 'image/png' },
        mandatory: false,
      },
    ],
    participantType: 'CONSUMER',
    creationTimestamp: new Date().toISOString(),
    updateTimestamp: new Date().toISOString()
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        OnboardingProcedureTemplateDetailComponent,
        TranslocoTestingModule.forRoot(
          translocoTestConfiguration(AppDomain.ONBOARDING)
        ),
        NoopAnimationsModule
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OnboardingProcedureTemplateDetailComponent);
    component = fixture.componentInstance;
    component.template = mockTemplate as OnboardingProcedureTemplate;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
