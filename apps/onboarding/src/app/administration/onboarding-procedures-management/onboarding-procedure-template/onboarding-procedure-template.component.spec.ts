import { ComponentFixture, TestBed } from '@angular/core/testing';
import { OnboardingProcedureTemplateComponent } from './onboarding-procedure-template.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { TranslocoTestingModule } from '@jsverse/transloco';
import { translocoTestConfiguration, AppDomain } from '@test-environment';
import { of } from 'rxjs';
import { OnboardingProceduresManagementService } from '../onboarding-procedures-management.service';
import { MimeTypeDialogComponent } from './mime-type-dialog/mime-type-dialog.component';

describe('OnboardingProcedureTemplateComponent', () => {
  let component: OnboardingProcedureTemplateComponent;
  let fixture: ComponentFixture<OnboardingProcedureTemplateComponent>;

  const mockOnboardingProceduresManagementService = {
    getMIMETypeList: jest.fn().mockReturnValue(of([])),
    editOnboardingTemplate: jest.fn().mockReturnValue(of({}))
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        OnboardingProcedureTemplateComponent,
        HttpClientTestingModule,
        TranslocoTestingModule.forRoot(
          translocoTestConfiguration(AppDomain.ONBOARDING)
        ),
        NoopAnimationsModule
      ],
      providers: [
        { provide: OnboardingProceduresManagementService, useValue: mockOnboardingProceduresManagementService }
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OnboardingProcedureTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call getMIMETypeList on ngOnInit', () => {
    expect(mockOnboardingProceduresManagementService.getMIMETypeList).toHaveBeenCalled();
  });

  it('should add a new document template when addDocumentTemplate is called', () => {
    const initialLength = component.documentTemplates.length;
    component.addDocumentTemplate();
    expect(component.documentTemplates.length).toBe(initialLength + 1);
  });

  it('should remove a document template when removeDocumentTemplate is called', () => {
    component.addDocumentTemplate();
    const initialLength = component.documentTemplates.length;
    component.removeDocumentTemplate(0);
    expect(component.documentTemplates.length).toBe(initialLength - 1);
  });

  it('should emit updatedProcedure when onUpdatedProcedure is called', () => {
    jest.spyOn(component.updatedProcedure, 'emit');
    component.onUpdatedProcedure();
    expect(component.updatedProcedure.emit).toHaveBeenCalledWith(true);
  });

  it('should emit cancel when onCancel is called', () => {
    jest.spyOn(component.cancel, 'emit');
    component.onCancel();
    expect(component.cancel.emit).toHaveBeenCalledWith(true);
  });

  it('should update expirationConversion correctly', () => {
    component.updateConvertedExpirationTimeframe('3600');
    expect(component.expirationConversion).toBe('1 h');

    component.updateConvertedExpirationTimeframe('');
    expect(component.expirationConversion).toBe('');
  });

  it('should filter MIME types correctly', () => {
    component.mimeTypes = [
      { value: 'application/pdf', description: 'PDF' },
      { value: 'image/jpeg', description: 'JPEG' },
    ];
    component.filterMimeTypes({ target: { value: 'application' } });
    expect(component.filteredMimeTypes).toEqual([
      { value: 'application/pdf', description: 'PDF' },
    ]);

    component.filterMimeTypes({ target: { value: '' } });
    expect(component.filteredMimeTypes).toEqual(component.mimeTypes);
  });

  it('should reset a document template correctly', () => {
    component.addDocumentTemplate();
    component.documentTemplates.at(0).patchValue({
      id: '123',
      description: 'Test description',
      mandatory: true,
      mimeType: { value: 'application/pdf', description: 'PDF' },
    });
    component.resetDocumentTemplate(0);
    expect(component.documentTemplates.at(0).value).toEqual({
      id: null,
      creationTimestamp: null,
      updateTimestamp: null,
      name: '',
      description: '',
      mandatory: false,
      mimeType: '',
    });
  });

  it('should open MIME type dialog and update MIME type list', () => {
    const dialogSpy = jest.spyOn(component.mimeDialog, 'open').mockReturnValue({
      afterClosed: () => of(true),
    } as any);
    const updateMimeTypeListSpy = jest.spyOn(component, 'updateMimeTypeList');

    component.insertMIMEType();
    expect(dialogSpy).toHaveBeenCalledWith(MimeTypeDialogComponent);
    expect(updateMimeTypeListSpy).toHaveBeenCalled();
  });

  it('should clean up on destroy', () => {
    const mimeDialogCloseAllSpy = jest.spyOn(component.mimeDialog, 'closeAll');
    const resetSpy = jest.spyOn(component.onboardingTemplateForm, 'reset');
    const patchValueSpy = jest.spyOn(component.documentTemplates, 'patchValue');

    component.ngOnDestroy();

    expect(mimeDialogCloseAllSpy).toHaveBeenCalled();
    expect(resetSpy).toHaveBeenCalled();
    expect(patchValueSpy).toHaveBeenCalledWith([]);
  });

  it('should reset all document templates', () => {
    component.addDocumentTemplate();
    component.addDocumentTemplate();
    component.resetDocumentTemplates();
    expect(component.documentTemplates.length).toBe(0);
  });
});
