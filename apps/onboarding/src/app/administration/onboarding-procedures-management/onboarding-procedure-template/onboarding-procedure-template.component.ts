import { Component, EventEmitter, inject, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  FormArray,
  FormControl,
  FormGroup,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import {
  MatButtonModule,
} from '@angular/material/button';
import {
  MatFormFieldModule,
  MatHint,
  MatLabel,
} from '@angular/material/form-field';
import { MatInput, MatError, MatInputModule } from '@angular/material/input';
import { MatAutocompleteModule, MatOption } from '@angular/material/autocomplete';
import { MatSelectModule } from '@angular/material/select';
import { TranslocoDirective, TranslocoService } from '@jsverse/transloco';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatIconModule } from '@angular/material/icon';
import { Tab, TabsComponent } from '@fe-simpl/tabs';
import { MatCheckbox } from '@angular/material/checkbox';
import { MatMenuModule } from '@angular/material/menu';
import { OnboardingProceduresManagementService } from '../onboarding-procedures-management.service';
import { MimeTypeDialogComponent } from './mime-type-dialog/mime-type-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { AlertBannerService } from '@fe-simpl/alert-banner';
import { MIMEType, OnboardingProcedureTemplate } from '@fe-simpl/api-types';

@Component({
  selector: 'app-onboarding-procedure-template',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule,
    TranslocoDirective,
    MatProgressSpinnerModule,
    MatFormFieldModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatSelectModule,
    MatError,
    MatInput,
    MatHint,
    MatOption,
    MatLabel,
    TabsComponent,
    MatCheckbox,
    MatMenuModule,
    MatIconModule,
    MatAutocompleteModule
  ],
  templateUrl: './onboarding-procedure-template.component.html',
  styles: ``
})
export class OnboardingProcedureTemplateComponent implements OnInit, OnDestroy {
  @Input() participantType: string;
  @Input() template: OnboardingProcedureTemplate;
  @Output() updatedProcedure: EventEmitter<boolean> = new EventEmitter<boolean>(false)
  @Output() cancel: EventEmitter<boolean> = new EventEmitter<boolean>(false)
  onboardingTemplateForm: FormGroup;
  loading = false;
  expirationConversion = '';
  tabs: Array<Tab> = [
    {
      id: '0',
      label: 'documents',
    },
  ];

  filteredMimeTypes: Array<MIMEType> = [];
  mimeTypes:Array<MIMEType> =[];

  readonly mimeDialog = inject(MatDialog);
  private readonly alertBannerService = inject(AlertBannerService);
  private readonly translocoService = inject(TranslocoService);

  constructor(
    private readonly service: OnboardingProceduresManagementService
  ) {}

  ngOnInit(): void {
    this.updateMimeTypeList();

    this.onboardingTemplateForm = new FormGroup({
      description: new FormControl('', [Validators.required]),
      expirationTimeframe: new FormControl('', [
        Validators.required,
        Validators.min(0),
        Validators.max(15552000),
      ]),
      documentTemplates: new FormArray([])
    });

    this.onboardingTemplateForm.patchValue(this.template);
    this.updateMimeTypeList(true);


    this.onboardingTemplateForm
      .get('expirationTimeframe')
      ?.valueChanges.subscribe((value) => {
        this.updateConvertedExpirationTimeframe(value);
      });
  }

  updateMimeTypeList(firstUpdate?: boolean){
    this.service.getMIMETypeList().subscribe({
      next: list => {
        this.mimeTypes = list;
        this.filterMimeTypes();
        if(firstUpdate){
          this.createDocumentTemplates();
        }
      }, error: () => {
        this.loading = false;
      }
    })
  }

  createDocumentTemplates(){
    this.template?.documentTemplates?.forEach(documentTemplate => {
      const selectedMime = this.mimeTypes?.find(mime => String(mime.value) === documentTemplate.mimeType.value)
      this.documentTemplates.push(
        new FormGroup({
          id: new FormControl(documentTemplate.id ?? null),
          creationTimestamp: new FormControl(null),
          updateTimestamp: new FormControl(null),
          name: new FormControl(documentTemplate.name ?? '', Validators.required),
          description: new FormControl(documentTemplate.description ?? '', Validators.required),
          mandatory: new FormControl(documentTemplate.mandatory ?? false),
          mimeType: new FormControl(selectedMime ?? '', Validators.required)
        })
      );
    });
    this.onboardingTemplateForm?.updateValueAndValidity();
    this.addDocumentTemplate();
  }

  updateConvertedExpirationTimeframe(timeFrame: string) {
    this.expirationConversion = timeFrame
      ? secondsToTimeString(Number(timeFrame))
      : '';
  }

  get documentTemplates(): FormArray {
    return this.onboardingTemplateForm?.get('documentTemplates') as FormArray;
  }

  addDocumentTemplate() {
    this.documentTemplates.push(
      new FormGroup({
        id: new FormControl(null),
        creationTimestamp: new FormControl(null),
        updateTimestamp: new FormControl(null),
        name: new FormControl('', Validators.required),
        description: new FormControl('', Validators.required),
        mandatory: new FormControl(false),
        mimeType: new FormControl('', Validators.required)
      })
    );
  }

  removeDocumentTemplate(index: number) {
    this.documentTemplates.removeAt(index);
  }

  updateDocumentTemplate(index: number, updatedData: any) {
    const document = this.documentTemplates.at(index);
    if (document) {
      document.patchValue(updatedData);
    }
  }

  resetDocumentTemplates() {
    this.documentTemplates.clear();
  }

  resetDocumentTemplate(index: number) {
    const document = this.documentTemplates.at(index);

    if (document) {
      document.reset({
        id: null,
        creationTimestamp: null,
        updateTimestamp: null,
        name: '',
        description: '',
        mandatory: false,
        mimeType: ''
      });
    }
  }

  insertMIMEType(){
    const mimeDialogRef = this.mimeDialog.open(MimeTypeDialogComponent);

    mimeDialogRef.afterClosed().subscribe(updated => {
      this.updateMimeTypeList();
    })
  }

  onSubmit() {
    if (this.onboardingTemplateForm.valid) {
      this.loading = true;
      let formValue = this.onboardingTemplateForm.value
      const documentsWithMimeNames = formValue.documentTemplates.map((document: any) => {
        return {
          ...document,
          mimeType: this.mimeTypes?.find(mime => String(mime.value) === document.mimeType.value)
        }
      })

      formValue = {
        ...formValue,
        documentTemplates: documentsWithMimeNames
      }

      this.service.editOnboardingTemplate(formValue, this.participantType).subscribe({
        next: () => {
          this.loading = false;
          this.alertBannerService.show({
            message: this.translocoService.translate(
              "onboardingTemplate.saveSuccess"
            ),
            type: "info",
          })
          this.onUpdatedProcedure();
        }
      })
    }
  }

  filterMimeTypes(event?: any) {
    if (event) {
      const filter = event.target?.value?.toLowerCase() ?? ""
      this.filteredMimeTypes = this.mimeTypes.filter(mimeType => mimeType.value.toLowerCase().startsWith(filter));
    } else {
      this.filteredMimeTypes = this.mimeTypes
    }
  }

  getMimeValue(event: any) : string{
    return event?.value ?? ''
  }

  onUpdatedProcedure(){
    this.updatedProcedure.emit(true);
  }

  onCancel(){
    this.cancel.emit(true);
  }

  ngOnDestroy(): void {
    this.mimeDialog.closeAll();
    this.onboardingTemplateForm.reset();
    this.documentTemplates.patchValue([]);
  }
}

export function secondsToTimeString(seconds: number) {
  const SECONDS_IN_MINUTE = 60;
  const SECONDS_IN_HOUR = SECONDS_IN_MINUTE * 60;
  const SECONDS_IN_DAY = SECONDS_IN_HOUR * 24;
  const SECONDS_IN_WEEK = SECONDS_IN_DAY * 7;
  const SECONDS_IN_MONTH = SECONDS_IN_DAY * 30;

  const months = Math.floor(seconds / SECONDS_IN_MONTH);
  seconds %= SECONDS_IN_MONTH;

  const weeks = Math.floor(seconds / SECONDS_IN_WEEK);
  seconds %= SECONDS_IN_WEEK;

  const days = Math.floor(seconds / SECONDS_IN_DAY);
  seconds %= SECONDS_IN_DAY;

  const hours = Math.floor(seconds / SECONDS_IN_HOUR);
  seconds %= SECONDS_IN_HOUR;

  const minutes = Math.floor(seconds / SECONDS_IN_MINUTE);
  seconds %= SECONDS_IN_MINUTE;

  let timeString = '';
  if (months > 0) timeString += `${months} month${months > 1 ? 's' : ''}, `;
  if (weeks > 0) timeString += `${weeks} week${weeks > 1 ? 's' : ''}, `;
  if (days > 0) timeString += `${days} day${days > 1 ? 's' : ''}, `;
  if (hours > 0) timeString += `${hours} h, `;
  if (minutes > 0) timeString += `${minutes} m, `;
  if (seconds > 0) timeString += `${seconds} s`;

  return timeString.trim().replace(/,\s*$/, '');
}
