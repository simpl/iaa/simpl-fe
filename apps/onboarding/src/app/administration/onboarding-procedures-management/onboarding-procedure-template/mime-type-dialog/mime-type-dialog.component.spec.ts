import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MimeTypeDialogComponent } from './mime-type-dialog.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { TranslocoTestingModule } from '@jsverse/transloco';
import { translocoTestConfiguration, AppDomain } from '@test-environment';
import { provideHttpClient } from '@angular/common/http';
import { of } from 'rxjs';
import { OnboardingProceduresManagementService } from '../../onboarding-procedures-management.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

describe('MimeTypeDialogComponent', () => {
  let component: MimeTypeDialogComponent;
  let fixture: ComponentFixture<MimeTypeDialogComponent>;

  const mockOnboardingProceduresManagementService = {
    addMIMEType: jest.fn().mockReturnValue(of({}))
  };

  const mockDialogRef = {
    updateSize: jest.fn(),
    close: jest.fn()
  }

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        MimeTypeDialogComponent,
        HttpClientTestingModule,
        TranslocoTestingModule.forRoot(
          translocoTestConfiguration(AppDomain.ONBOARDING)
        ),
        NoopAnimationsModule
      ],
      providers: [
        provideHttpClient(),
        { provide: OnboardingProceduresManagementService, useValue: mockOnboardingProceduresManagementService },
        { provide: MAT_DIALOG_DATA, useValue: {} },
        { provide: MatDialogRef, useValue: mockDialogRef },
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MimeTypeDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call addMIMEType on submit when form is valid', () => {
    component.mimeTypeForm.setValue({
      value: 'application/json',
      description: 'JSON MIME type'
    });

    component.onSubmit();

    expect(mockOnboardingProceduresManagementService.addMIMEType).toHaveBeenCalledWith({
      value: 'application/json',
      description: 'JSON MIME type'
    });
  });

  it('should close the dialog with true on successful addMIMEType', () => {
    const dialogRefSpy = jest.spyOn(component.dialogRef, 'close');

    component.mimeTypeForm.setValue({
      value: 'application/json',
      description: 'JSON MIME type'
    });

    component.onSubmit();
    expect(dialogRefSpy).toHaveBeenCalledWith(true);
  });
});
