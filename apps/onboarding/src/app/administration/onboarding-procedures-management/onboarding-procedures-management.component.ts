import { Component, OnInit, inject } from '@angular/core';
import { CommonModule } from "@angular/common";
import { TabsComponent, Tab } from "@fe-simpl/tabs";
import { TranslocoDirective, TranslocoService } from "@jsverse/transloco";
import { ActivatedRoute, RouterModule } from "@angular/router";
import { LoaderComponent } from "@fe-simpl/loader";
import { OnboardingProceduresManagementService } from './onboarding-procedures-management.service';
import { MatIcon, MatIconModule } from '@angular/material/icon';
import { MatButton, MatButtonModule } from '@angular/material/button';
import { OnboardingProcedureTemplateComponent } from './onboarding-procedure-template/onboarding-procedure-template.component';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from '@fe-simpl/shared-ui/confirm-dialog';
import { OnboardingProcedureTemplateDetailComponent } from './onboarding-procedure-template-detail/onboarding-procedure-template-detail.component';
import { OnboardingProcedureTemplate, ParticipantType } from 'libs/core/api-types/src/lib/participantDto';

@Component({
  selector: 'app-onboarding-procedures-management',
  standalone: true,
  imports: [
    CommonModule,
    TabsComponent,
    TranslocoDirective,
    RouterModule,
    LoaderComponent,
    MatIconModule,
    MatIcon,
    MatButtonModule,
    MatButton,
    OnboardingProcedureTemplateComponent,
    OnboardingProcedureTemplateDetailComponent
  ],
  templateUrl: './onboarding-procedures-management.component.html',
  styles: ``
})

export class OnboardingProceduresManagementComponent implements OnInit{
  private dialog = inject(MatDialog);
  private translocoService = inject(TranslocoService);
  onboardingProceduresManagementService = inject(OnboardingProceduresManagementService);
  private activatedRoute: ActivatedRoute = inject(ActivatedRoute);

  loading = false;
  activeTab = "";
  selectedParticipantType = "";
  participantTypeList: Array<ParticipantType> = [];
  tabs: Array<Tab>
  participantTabs: Array<any> = [];
  onboardingProcedureTemplate: OnboardingProcedureTemplate
  mode: 'new' | 'edit' | 'detail' = 'detail'

  ngOnInit(): void {
    this.activeTab = this.activatedRoute.snapshot.queryParams["tab"] ?? '1';
    this.loading = true;
    this.onboardingProceduresManagementService.getParticipantTypeList()
      .subscribe({
        next: (list) => {
          this.participantTypeList = list;
          if(this.participantTypeList?.length > 0 && this.participantTypeList[0]?.id){
            this.participantTabs = this.participantTypeList.map((participantType, index) => {
              return {
                id: participantType.id,
                label: participantType?.label,
                value: participantType.value,
                index: String(index + 1)
              }
            })
            this.tabs = this.participantTypeList.map((participantType, index) => {
              return {
                id: String(index + 1),
                label: participantType?.label
              }
            })
            this.getOnboardingProcedureTemplate();
          }
        },
        error: () => {
          this.loading = false;
        }
      });
  }

  clickTab(event?: any) {
    if(this.mode === 'edit' || this.mode === 'new') {
      const dialogRef = this.dialog.open(ConfirmDialogComponent, {
        data: {
          title: "onboardingProcedures.changeTab",
          message: this.translocoService.translate(
            "onboardingProcedures.changeTabMessage"
          ),
          confirm: "onboardingProcedures.changeTabApprove",
          cancel: "onboardingProcedures.changeTabCancel",
        },
      });
      dialogRef.afterClosed().subscribe((result) => {
        if (result) {
          this.changeTab(event);
        }
      });
    } else {
      this.changeTab(event);
    }
  }

  changeTab(tab: string){
    this.activeTab = tab;
    this.getOnboardingProcedureTemplate();
  }

  getOnboardingProcedureTemplate(){
    this.selectedParticipantType = this.participantTabs.find(participantType => participantType.index == this.activeTab)?.id ?? '';
    this.onboardingProceduresManagementService.getOnboardingProcedureTemplate(this.selectedParticipantType).subscribe({
      next: (template) => {
        this.onboardingProcedureTemplate = template;
        this.mode = 'detail';
        this.loading = false;
      },
      error: () => {
        this.loading = false;
      }
    })
  }

  onAddTemplate(){
    this.mode = 'new';
  }

  onUpdatedTemplate(){
    this.mode = 'detail';
    this.loading = true;
    this.getOnboardingProcedureTemplate();
  }

  onEditTemplate(){
    this.mode = 'edit';
  }

  onBack(){
    this.mode = 'detail'
  }
}
