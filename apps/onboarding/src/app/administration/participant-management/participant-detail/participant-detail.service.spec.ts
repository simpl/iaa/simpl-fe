import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ParticipantDetailService } from './participant-detail.service';
import { API_URL, ApiService } from '@fe-simpl/api';

describe('ParticipantDetailService', () => {
  let service: ParticipantDetailService;
  const apiServiceMock = {
    deleteWithBody: jest.fn(),
    put: jest.fn(),
  };

  const mockApiUrl = 'http://mock-api-url';
  let httpClientMock: jest.Mocked<HttpClient>;

  beforeEach(() => {
    httpClientMock = {
      put: jest.fn(),
      post: jest.fn(),
      get: jest.fn(),
      delete: jest.fn(),
    } as unknown as jest.Mocked<HttpClient>;

    TestBed.configureTestingModule({
      providers: [
        { provide: HttpClient, useValue: httpClientMock },
        { provide: API_URL, useValue: mockApiUrl },
        { provide: ApiService, useValue: apiServiceMock }
      ],
    });
    service = TestBed.inject(ParticipantDetailService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should call deleteWithBody with correct URL and identityAttributeIds', () => {
    const participantId = '0190bacd-25c4-73fb-8dad-8605593c451f';
    const identityAttributeIds = [
      '0190c0fd-42f9-70df-9eab-b73ca43547c0',
      '0190c0fd-a076-7d27-b8e7-4cc7626796c9',
    ];
    apiServiceMock.deleteWithBody.mockReturnValue(of({}));

    service
      .removeIdentityMultipleAttributeFromParticipant(
        participantId,
        identityAttributeIds
      )
      .subscribe();

    expect(apiServiceMock.deleteWithBody).toHaveBeenCalledWith(
      `/sap-api/identity-attribute/unassign-participant/${participantId}`,
      identityAttributeIds
    );
  });

  it('should call put with correct URL and identityAttributeIds', () => {
    const participantId = '0190bacd-25c4-73fb-8dad-8605593c451f';
    const identityAttributeIds = [
        '0190c0fd-42f9-70df-9eab-b73ca43547c0',
        '0190c0fd-a076-7d27-b8e7-4cc7626796c9',
      ];
      httpClientMock.put.mockReturnValue(of({}));

    service.addIdentityAttributesToParticipant(participantId, identityAttributeIds).subscribe();

    expect(httpClientMock.put).toHaveBeenCalledWith(
      `${mockApiUrl}/sap-api/identity-attribute/assign-participant/${participantId}`,
      identityAttributeIds
    );
  });
});
