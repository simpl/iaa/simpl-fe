import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { ParticipantDetailsManagementComponent } from './participant-details-management.component';
import { TranslocoTestingModule } from '@jsverse/transloco';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialogModule } from '@angular/material/dialog';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { By } from '@angular/platform-browser';
import enTranslations from "../../../../../assets/i18n/en.json";
import { AppDomain, translocoTestConfiguration } from '@test-environment';
import { of } from 'rxjs';
import { ApiService } from '@fe-simpl/api';
import { TextFilter } from '@fe-simpl/search-page';

describe('ParticipantDetailsManagementComponent', () => {
  let component: ParticipantDetailsManagementComponent;
  let fixture: ComponentFixture<ParticipantDetailsManagementComponent>;
  let mockDialogRef: any;
  let mockDialogData: any;
  beforeEach(async () => {
    mockDialogRef = {
      close: jest.fn()
    };

    mockDialogData = {
      name: "participantIdNotIn",
      value: "0190bacd-25c4-73fb-8dad-8605593c451f"
    };

    const mockApiService = {
      get: jest.fn().mockReturnValue(of([]))
    };

    await TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        TranslocoTestingModule.forRoot(
          translocoTestConfiguration(AppDomain.ONBOARDING)
        ),
        ParticipantDetailsManagementComponent
      ],
      providers: [
        { provide: MatDialogRef, useValue: mockDialogRef },
        { provide: MAT_DIALOG_DATA, useValue: mockDialogData },
        { provide: ApiService, useValue: mockApiService },
        MatDialogModule
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(ParticipantDetailsManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render elements', () => {
    fixture.detectChanges();
    const compiled = fixture?.nativeElement as HTMLElement;

    const attributesList = fixture.debugElement.query(By.css('lib-search-page'));
    expect(attributesList).toBeTruthy();

    const title = compiled?.querySelector("h2[mat-dialog-title]") as HTMLElement;
    expect(title).not.toBeNull();

    const titleText = title?.textContent?.trim()
    expect(titleText)?.toContain(enTranslations?.participantType?.addAttribute);

    const addButton = compiled?.querySelector("#addButton") as HTMLElement;
    const cancelButton = compiled?.querySelector("#cancelButton") as HTMLElement;

    expect(addButton).toBeTruthy();
    expect(cancelButton).toBeTruthy();

    expect(cancelButton?.textContent?.trim()).toBe(enTranslations?.simpleInput?.cancel);
    expect(addButton?.textContent?.trim())?.toBe(enTranslations?.simpleInput?.add);

  });

  it('should close the dialog when cancel button is clicked', fakeAsync(() => {
    tick();
    fixture.detectChanges();
    const compiled = fixture.nativeElement as HTMLElement;

    const cancelButton = compiled.querySelector("#cancelButton") as HTMLElement;
    cancelButton.click();
    fixture.detectChanges();
    expect(mockDialogRef.close).toHaveBeenCalled();
  }));

  it('should close the dialog and return selected ids when add button is clicked', fakeAsync(() => {
    tick();
    fixture.detectChanges();
    const compiled = fixture.nativeElement as HTMLElement;

    component.idAttrIds = ['0190c0fd-42f9-70df-9eab-b73ca43547c0', '0190c0fd-a076-7d27-b8e7-4cc7626796c9'];
    fixture.detectChanges();
    const addButton = compiled.querySelector("#addButton") as HTMLElement;
    addButton?.click();

    expect(mockDialogRef.close).toHaveBeenCalledWith(['0190c0fd-42f9-70df-9eab-b73ca43547c0', '0190c0fd-a076-7d27-b8e7-4cc7626796c9']);
  }));

  it('should handle checkbox selection correctly', fakeAsync(() => {
    tick();
    fixture.detectChanges();
    const mockIdSelected = ['0190c0fd-42f9-70df-9eab-b73ca43547c0', '0190c0fd-a076-7d27-b8e7-4cc7626796c9'];
    const searchPage = fixture?.debugElement.query(By?.css('lib-search-page'));

    searchPage.triggerEventHandler('checkboxSelectionEmitter', mockIdSelected);
    tick();
    fixture.detectChanges();

    expect(component.idAttrIds).toEqual(mockIdSelected);
  }));

  it('should set filter and call initializeTableInfo in ngOnInit', () => {
    const spyInitializeTableInfo = jest.spyOn(component, 'initializeTableInfo');
    component.ngOnInit();

    expect(component.filter).toEqual(mockDialogData);

    expect(spyInitializeTableInfo).toHaveBeenCalled();
  });

  it('should correctly set pageInfo in initializeTableInfo', () => {
    component.initializeTableInfo();

    expect(component.pageInfo).toBeDefined();
    expect(component.pageInfo.endpoint).toBe('/sap-api/identity-attribute/search');
    expect(component.pageInfo.columns).toBeDefined();
    expect(component.pageInfo.filters).toBeDefined();

    expect(component.pageInfo.columns?.['checkbox'].header).toBe('checkbox');
    expect(component.pageInfo.columns?.['checkbox'].isCheckbox).toBe(true);

    expect(component.pageInfo.columns?.['id']).toBeDefined();
    expect(component.pageInfo.columns?.['id'].header).toBe('participantType.id');

    expect(component.pageInfo.filters?.['name']).toBeInstanceOf(TextFilter);
    expect(component.pageInfo.filters?.['name'].name).toBe('name');
    expect(component.pageInfo.filters?.['name'].label).toBe('filters.name');
    expect(component.pageInfo.columns?.['assignableToRoles']).toBeDefined();
    expect(component.pageInfo.columns?.['assignableToRoles'].header).toBe('participantType.assignableToRoles');

    expect(component.pageInfo.columns?.['enabled']).toBeDefined();
    expect(component.pageInfo.columns?.['enabled'].header).toBe('participantType.enabled');

    expect(component.pageInfo.columns?.['outcomeUserEmail']).toBeDefined();
    expect(component.pageInfo.columns?.['outcomeUserEmail'].header).toBe('participantType.outcomeUserEmail');

    expect(component.pageInfo.columns?.['creationTimestamp']).toBeDefined();
    expect(component.pageInfo.columns?.['creationTimestamp'].header).toBe('participantType.creationTimestamp');
    expect(component.pageInfo.columns?.['creationTimestamp'].mapper).toBeDefined();
  });

  it('should correctly map creationTimestamp and updateTimestamp', () => {
    component.initializeTableInfo();

    const testTimestamp = '2023-09-01T00:00:00.000Z';
    const mappedCreation = component?.pageInfo?.columns?.['creationTimestamp'].mapper;
    const mappedUpdate = component?.pageInfo?.columns?.['updateTimestamp'].mapper


    if (mappedCreation) {
      const mappedDateNull = mappedCreation(null);
      const mappedDateUndefined = mappedCreation(undefined);
      const mappedDate = mappedCreation(testTimestamp);
      expect(mappedDate).toEqual(new Date(testTimestamp));
      expect(mappedDateNull).toEqual('');
      expect(mappedDateUndefined).toEqual('');
    }

    if (mappedUpdate) {
      const mappedDateNull = mappedUpdate(null);
      const mappedDateUndefined = mappedUpdate(undefined);
      const mappedDate = mappedUpdate(testTimestamp)
      expect(mappedDate).toEqual(new Date(testTimestamp));
      expect(mappedDateNull).toEqual('');
      expect(mappedDateUndefined).toEqual('');
    }
  });

  it('should close the dialog with no selected ids when cancel button is clicked', fakeAsync(() => {
    tick();
    fixture.detectChanges();
    const cancelButton = fixture.nativeElement.querySelector("#cancelButton") as HTMLElement;
    cancelButton.click();

    expect(mockDialogRef.close).toHaveBeenCalled();
  }));


  it('should handle missing or empty filters correctly', () => {
    component.pageInfo.filters = { name: new TextFilter('', ''), code: new TextFilter('', '') };
    expect(component.pageInfo.filters?.['name'].name).toBe('');
    expect(component.pageInfo.filters?.['code'].name).toBe('');
  });

});
