import { Component, Inject, OnInit } from "@angular/core";
import { CommonModule } from "@angular/common";
import {
  SearchPageComponent,
  SearchPageInfo,
  TextFilter,
} from "@fe-simpl/search-page";
import { TranslocoDirective } from "@jsverse/transloco";
import {
  MAT_DIALOG_DATA,
  MatDialogModule,
  MatDialogRef,
} from "@angular/material/dialog";
import { MatButtonModule } from "@angular/material/button";

@Component({
  selector: "app-participant-details-management",
  standalone: true,
  imports: [
    CommonModule,
    SearchPageComponent,
    TranslocoDirective,
    MatDialogModule,
    MatButtonModule,
  ],
  templateUrl: "./participant-details-management.component.html",
  styles: ``
})
export class ParticipantDetailsManagementComponent implements OnInit {
  idAttrIds: string[] = [];
  pageInfo: SearchPageInfo;
  filter: {name: string, value: string};

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { name: string, value: string },
    public dialogRef: MatDialogRef<ParticipantDetailsManagementComponent>
  ) {}

  ngOnInit(): void {
    this.filter = this.data;
    this.initializeTableInfo();
  }

  initializeTableInfo() {
    this.pageInfo = {
      endpoint: `/sap-api/identity-attribute/search`,
      columns: {
        checkbox: {
          header: "checkbox",
          isCheckbox: true,
        },
        id: {
          header: "participantType.id",
        },
        code: {
          header: "participantType.code",
        },
        name: {
          header: "participantType.name",
        },
        assignableToRoles: {
          header: "participantType.assignableToRoles",
        },
        enabled: {
          header: "participantType.enabled",
        },
        outcomeUserEmail: {
          header: "participantType.outcomeUserEmail",
        },
        creationTimestamp: {
          header: "participantType.creationTimestamp",
          mapper: (v) => (v ? new Date(v) : ""),
        },
        updateTimestamp: {
          header: "participantType.updateTimestamp",
          mapper: (v) => (v ? new Date(v) : ""),
        },
      },
      filters: {
        name: new TextFilter("name", "filters.name"),
        code: new TextFilter("code", "filters.code"),
      },
    };
  }
}
