import { CommonModule } from "@angular/common";
import { Component, computed, inject, OnInit, signal } from "@angular/core";
import { MatButton } from "@angular/material/button";
import { MatIcon } from "@angular/material/icon";
import { MatDialog } from "@angular/material/dialog";
import { ActivatedRoute } from "@angular/router";
import { AlertBannerService } from "@fe-simpl/alert-banner";
import {
  DetailsEntity,
  DetailsPageComponent,
  TextDetail,
} from "@fe-simpl/details-page";
import { LoaderComponent } from "@fe-simpl/loader";
import {
  ButtonActions,
  DateRangeFilter,
  PilotType,
  SearchPageComponent,
  SearchPageInfo,
  TextFilter,
} from "@fe-simpl/search-page";
import { ConfirmDialogComponent } from "@fe-simpl/shared-ui/confirm-dialog";
import { TranslocoDirective, TranslocoService } from "@jsverse/transloco";
import { finalize, tap } from 'rxjs';
import { ParticipantDetailService } from "./participant-detail.service";
import { ParticipantDetailsManagementComponent } from "./participant-details-management/participant-details-management.component";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { API_URL } from '@fe-simpl/api';
import { MatTooltipModule } from "@angular/material/tooltip";
import { MatDivider } from '@angular/material/divider';

@Component({
  selector: "app-participant-detail",
  standalone: true,
  imports: [
    MatIcon,
    MatButton,
    CommonModule,
    TranslocoDirective,
    DetailsPageComponent,
    LoaderComponent,
    SearchPageComponent,
    MatTooltipModule,
    MatDivider
  ],
  templateUrl: "./participant-detail.component.html",
  styleUrl: "./participant-detail.component.css",
})
export class ParticipantDetailComponent implements OnInit {
  private readonly activatedRoute = inject(ActivatedRoute);
  private readonly dialog = inject(MatDialog);
  private readonly translocoService = inject(TranslocoService);
  private readonly participantDetailsService = inject(ParticipantDetailService);
  private readonly banner = inject(AlertBannerService);
  private readonly _httpClient = inject(HttpClient);
  private readonly api_url = inject(API_URL);

  pilot: PilotType = {};

  loading = signal(false);
  participantId = "";
  participantDetails = signal<any | null | undefined>(null);
  hasCredentials = false;

  entity = computed<DetailsEntity | null>(() => {
    const response = this.participantDetails();
    if (response) {
      return {
        id: new TextDetail("administration.id", response.id),
        participantName: new TextDetail(
          "administration.participantName",
          response.organization
        ),
        participantType: new TextDetail(
          "administration.participant",
          response.participantType
        ),
        onboardingDate: new TextDetail(
          "administration.onboardingDate",
          new Date(response.updateTimestamp).toDateString()
        ),
        expiryDate: new TextDetail(
          "administration.expiryDate",
          response.expiryDate
            ? new Date(response.expiryDate).toDateString()
            : "-"
        ),
      };
    } else {
      return null;
    }
  });

  attributeList: SearchPageInfo = {
    endpoint: "/sap-api/identity-attribute/search",
    columns: {
      name: {
        header: "administration.attributeName",
        sort: true,
      },
      code: {
        header: "administration.attributeCode",
        sort: true,
      },
      assignableToRoles: {
        header: "administration.attributeAssignable",
      },
      enabled: {
        header: "administration.attributeEnabled",
      },
      creationTimestamp: {
        header: "administration.attributeCreationDate",
        mapper: (date) => new Date(date),
      },
      updateTimestamp: {
        header: "administration.attributeLastUpdateDate",
        mapper: (date) => new Date(date),
        sort: true,
      },
      actions: {
        header: "simpleInput.actions",
        mapper: () =>
          new ButtonActions([
            {
              icon: "delete",
              theme: "secondary",
              action: (row) => this.removeRemoveAttribute(row),
            },
          ]),
      },
    },
    filters: {
      name: new TextFilter("name", "filters.name"),
      code: new TextFilter("code", "filters.code"),
      updateTimestampFrom: new DateRangeFilter(
        "updateTimestamp",
        "filters.updateTimestamp",
        "updateTimestampFrom",
        "updateTimestampTo"
      ),
    },
  };

  ngOnInit(): void {

    this.participantId = this.activatedRoute.snapshot.paramMap.get("id")!;
    this.loading.set(true);
    this._httpClient.get<any>(this.api_url + `/identity-api/participant/${this.participantId}`)
      .pipe().subscribe({
        next: details => {
          this.loading.set(false);
          this.participantDetails.set(details)},
        error: console.error,
      });
    this.checkCredentials();
  }

  removeRemoveAttribute(row: any) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: "simpleInput.confirmDeletion",
        message: this.translocoService.translate(
          "administration.confirmDeletionIdentityAttributeMessage",
          { code: row.code }
        ),
        confirm: "simpleInput.delete",
        cancel: "simpleInput.cancel",
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        const details = this.participantDetails();
        if (details) {
          this.confirmDeleteIAAction(details.id, row);
        }
      }
    });
  }

  confirmDeleteIAAction(participantId: string, row: any) {
    this.loading.set(true);
    this.participantDetailsService
      .removeIdentityMultipleAttributeFromParticipant(participantId, [row.id])
      .pipe(finalize(() => this.loading.set(false)))
      .subscribe({
        next: () => {
          this.banner.show({
            type: "info",
            message: this.translocoService.translate(
              "administration.deleteIdentityAttribueSuccess"
            ),
          });
          this.pilot?.["reload"]();
        },
      });
  }

  openIdAttrDialog() {
    const details = this.participantDetails();
    if (details) {
      this.dialog
        .open(ParticipantDetailsManagementComponent, {
          data: {
            name: "participantIdNotIn",
            value: details.id,
          },
        })
        .afterClosed()
        .subscribe((idAttrIds: string[]) => {
          if (idAttrIds && idAttrIds.length > 0) {
            this.addAllIdentityAttributes(details.id, idAttrIds);
          }
        });
    }
  }

  addAllIdentityAttributes(participantId: string, idAttrIds: string[]) {
    this.loading.set(true);
    this.participantDetailsService
      .addIdentityAttributesToParticipant(participantId, idAttrIds)
      .pipe(finalize(() => this.loading.set(false)))
      .subscribe({
        next: () => {
          this.banner.show({
            type: "info",
            message: this.translocoService.translate(
              "administration.successAddAttribute"
            ),
          });
          this.pilot?.["reload"]();
        },
      });
  }

  checkCredentials(){
    this.participantDetailsService.checkCredential(this.participantId).subscribe({
      next: () => {
        this.hasCredentials = true;
      },
      error: () => {
        this.hasCredentials = false;
      }
    })
  }

  renewCredentials(){
    this.loading.set(true);
    this.participantDetailsService.renewCredentials(this.participantId).pipe(
      tap(res => {
        const fileName = this.extractFileName(res.headers);
        this.downloadCertificate(res.body, fileName);
      })
    ).subscribe(res => {
      this.banner.show({
        type: "info",
        message: this.translocoService.translate(
          "administration.successRenewCredentials"
        ),
      });
      this.checkCredentials();
      this.loading.set(false);
    })
  }

  downloadCertificate(content: string, fileName: string) {

    const blob = new Blob([content], { type: 'application/x-pem-file' });
    const url = window.URL.createObjectURL(blob);
    const a = document.createElement('a');
    a.href = url;
    a.download = fileName;

    a.click();
    window.URL.revokeObjectURL(url);
  }

  extractFileName(headers: HttpHeaders): string {
    const contentDisposition = headers.get('Content-Disposition');
    if (contentDisposition) {
      const matches = contentDisposition.match(/filename="?([^"]+)"?/);
      if (matches && matches[1]) {
        return matches[1];
      }
    }
    return 'certificate.pem';
  }
}
