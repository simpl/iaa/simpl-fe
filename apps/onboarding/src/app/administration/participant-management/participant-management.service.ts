import { inject, Injectable } from "@angular/core";
import { ApiService } from "@fe-simpl/api";
import { Observable } from "rxjs";

@Injectable({ providedIn: "root" })
export class ParticipantManagementService {
  private readonly apiService = inject(ApiService);

  revokeAccess(id: string): Observable<void> {
    return this.apiService.delete(
      `/identity-api/certificate/${id}`
    );
  }
}
