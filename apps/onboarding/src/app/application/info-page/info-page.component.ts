import { Component, inject, NgZone, ViewEncapsulation } from '@angular/core';
import { CommonModule } from "@angular/common";
import { MatButton } from "@angular/material/button";
import { ReactiveFormsModule } from "@angular/forms";
import { ActivatedRoute, Router } from '@angular/router';
import { TranslocoDirective } from "@jsverse/transloco";

@Component({
  selector: 'app-info-page',
  standalone: true,
  imports: [
    CommonModule,
    MatButton,
    ReactiveFormsModule,
    TranslocoDirective,
  ],
  templateUrl: "./info-page.component.html",
  styleUrl: "./info-page.component.css"
})
export class InfoPageComponent {

  private readonly ngZone = inject(NgZone);

  constructor(
    private readonly router: Router,
    private readonly route: ActivatedRoute) {
  }

  navigateToRequest() {
    this.ngZone.run(() => {
      this.router.navigate(['/application/request'], { relativeTo: this.route });
    })
  }
}
