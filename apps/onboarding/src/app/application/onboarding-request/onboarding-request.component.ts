import { Component, inject, OnInit, signal } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import {
  AbstractControl,
  FormArray,
  FormControl,
  FormGroup,
  ReactiveFormsModule, ValidationErrors, ValidatorFn,
  Validators
} from '@angular/forms';
import { MatButton } from '@angular/material/button';
import { TranslocoDirective, TranslocoService } from '@jsverse/transloco';
import { HttpClient } from '@angular/common/http';
import { API_URL } from '@fe-simpl/api';
import { LoaderComponent } from '@fe-simpl/loader';
import { finalize, tap } from 'rxjs';
import { OnboardingStatusService } from '../onboarding-status/onboarding-status.service';
import { MatChip, MatChipSet } from '@angular/material/chips';
import { CommentsComponent } from '@fe-simpl/comments';
import { DocumentDTO, OnboardingRequestDTO } from '../../models/onboarding.types';
import { KeycloakService } from 'keycloak-angular';
import { MatIcon } from '@angular/material/icon';
import { AlertBannerService } from '@fe-simpl/alert-banner';
import { ConfirmDialogComponent } from '@fe-simpl/shared-ui/confirm-dialog';
import { MatDialog } from '@angular/material/dialog';
import {
  ProcessingRequestDialogComponent
} from './processing-request-dialog/processing-request-dialog.component';
import { ToDatePipe } from '../../override-date.pipe';
import { MatDivider } from '@angular/material/divider';
import { MatProgressSpinner } from '@angular/material/progress-spinner';

@Component({
  selector: 'app-onboarding-request',
  standalone: true,
  imports: [
    CommonModule,
    MatButton,
    ReactiveFormsModule,
    TranslocoDirective,
    LoaderComponent,
    MatChip,
    MatChipSet,
    CommentsComponent,
    MatIcon,
    DatePipe,
    ToDatePipe,
    MatDivider,
    MatProgressSpinner,
  ],
  templateUrl: './onboarding-request.component.html',
  styleUrl: './onboarding-request.component.css',
})
export class OnboardingRequestComponent implements OnInit {
  private readonly api_url = inject(API_URL);
  private readonly keycloakService = inject(KeycloakService);
  private readonly onboardingStatusService = inject(OnboardingStatusService);
  private readonly alertBannerService = inject(AlertBannerService);
  private readonly translocoService = inject(TranslocoService);
  private readonly dialog = inject(MatDialog);

  participantType = new FormControl('');
  onboardingFormGroup: FormGroup;
  $loading = signal(false);
  onboardingRequest!: OnboardingRequestDTO;
  user: string;
  roles: string[] = [];
  today = new Date();
  loading = false;

  constructor(private _httpClient: HttpClient) {}

  ngOnInit(): void {
    this.user =
      this.keycloakService.getKeycloakInstance()?.tokenParsed?.['name'];
    this.roles = this.keycloakService.getUserRoles();

    this.onboardingFormGroup = new FormGroup({
      documents: new FormArray([]),
    });

    this.getOnboardingStatus().subscribe((res) => {
      if (res.content[0]) {
        this.onboardingRequest = res.content[0];
        this.onboardingRequest.expirationTimeframe =
          new Date(
            this.onboardingRequest.lastStatusUpdateTimestamp
          ).getTime() +
          this.onboardingRequest.expirationTimeframe * 1000;
        this.onboardingRequest.documents.forEach((template: DocumentDTO) => {
          const templateToCreate = this.createOnboardingFormGroup(template);
          this.fileFormArray.push(templateToCreate);
        });
      }
    });
  }

  createOnboardingFormGroup(template: DocumentDTO): FormGroup {
    let formGroup: FormGroup;
    if (template.documentTemplate) {
      formGroup = new FormGroup({
        id: new FormControl(template.id),
        mimeType: new FormControl(template.documentTemplate.mimeType.value, [
          Validators.required,
        ]),
        description: new FormControl(template.documentTemplate.description, [
          Validators.required,
        ]),
        mandatory: new FormControl(template.documentTemplate.mandatory),
        content: new FormControl(template.content),
        hasContent: new FormControl(template.hasContent),
        name: new FormControl(template.filename),
        fileSize: new FormControl(template.filesize),
        lastUpdate: new FormControl(new Date(template.updateTimestamp)),
      });
    } else {
      formGroup = new FormGroup({
        id: new FormControl(template.id),
        mimeType: new FormControl(template.mimeType.value, [
          Validators.required,
        ]),
        description: new FormControl(template.description, [
          Validators.required,
        ]),
        mandatory: new FormControl(true),
        content: new FormControl(null),
        hasContent: new FormControl(template.hasContent),
        name: new FormControl(template.filename ? template.filename : ''),
        fileSize: new FormControl(template.filesize ? template.filesize : ''),
        lastUpdate: new FormControl(new Date(template.updateTimestamp)),
      });
    }
    formGroup.setValidators(this.mandatoryFileValidator());
    formGroup.get('mandatory')?.valueChanges.subscribe(() => {
      formGroup.updateValueAndValidity();
    });

    return formGroup;
  }

  mandatoryFileValidator(): ValidatorFn {
    return (group: AbstractControl): ValidationErrors | null => {
      const mandatory = group.get('mandatory')?.value;
      const hasContent = group.get('hasContent')?.value;

      if (mandatory && !hasContent) {
        return { requiredFileMissing: true };
      }
      return null;
    };
  }

  get fileFormArray(): FormArray {
    return this.onboardingFormGroup.get('documents') as FormArray;
  }

  getOnboardingStatus() {
    this.$loading.set(true);
    return this.onboardingStatusService
      .fetchOnboardingStatus()
      .pipe(finalize(() => this.$loading.set(false)));
  }

  onFileSelected(event: any, index: number): void {
    if (event.target.files && event.target.files.length > 0) {
      const fileSelectend = event.target.files[0] ?? null;
      const group = this.fileFormArray.at(index) as FormGroup;

      const reader = new FileReader();
      reader.onload = () => {
        const base64String = (reader.result as string).split(',')[1];
        group.patchValue({
          id: group.get('id')?.value,
          mimeType: group.get('mimeType')?.value,
          content: base64String,
          name: fileSelectend.name,
          fileSize: fileSelectend.size,
          hasContent: true,
          lastUpdate: new Date(),
        });
        const bodyToSend = {
          id: group.get('id')?.value,
          filename: fileSelectend.name,
          content: group.get('content')?.value,
        };
        // call http patch
        this._httpClient
          .patch(
            `${this.api_url}/onboarding-api/onboarding-request/${this.onboardingRequest?.id}/document`,
            bodyToSend
          )
          .pipe()
          .subscribe();
      };

      reader.onerror = (error) => {
        console.error('Errore durante la lettura del file:', error);
      };

      reader.readAsDataURL(fileSelectend);

      event.target.value = '';
    }
  }

  sendPostComment(event: string) {
    // call http patch
    const bodyComment = {
      author: this.user,
      content: event,
    };
    this._httpClient
      .patch(
        `${this.api_url}/onboarding-api/onboarding-request/${this.onboardingRequest?.id}/comment`,
        bodyComment
      )
      .pipe(
        tap((comment) => {
          this.onboardingRequest.comments = [
            bodyComment,
            ...this.onboardingRequest.comments,
          ];
        })
      )
      .subscribe({
        error: (error) => {
          this.alertBannerService.show({
            message: this.translocoService.translate(
              'requestOnboarding.errorSendComment'
            ),
            type: 'danger',
            autoDismissable: true,
          });
        },
      });
  }

  downloadFile(idFile: string) {
    this._httpClient
      .get(
        `${this.api_url}/onboarding-api/onboarding-request/${this.onboardingRequest?.id}/document/${idFile}`
      )
      .pipe(
        tap((data) => {
          this.download(data);
        })
      )
      .subscribe({
        error: (error) => {
          this.alertBannerService.show({
            message: this.translocoService.translate(
              'requestOnboarding.errorDownload'
            ),
            type: 'danger',
            autoDismissable: true,
          });
        },
      });
  }

  deleteFile(idFile: string, index: number) {
    this._httpClient
      .delete(
        `${this.api_url}/onboarding-api/onboarding-request/${this.onboardingRequest?.id}/document/${idFile}`
      )
      .pipe(
        tap((res) => {
          const controllToUpdate = (
            this.onboardingFormGroup.get('documents') as FormArray
          ).at(index);
          controllToUpdate.patchValue({
            content: null,
            name: null,
            hasContent: null,
            fileSize: null,
          });
        })
      )
      .subscribe({
        next: () => {
          this.alertBannerService.show({
            message: this.translocoService.translate(
              'requestOnboarding.okFileDeleted'
            ),
            type: 'success',
            autoDismissable: true,
          });
        },
        error: (error) => {
          this.alertBannerService.show({
            message: this.translocoService.translate(
              'requestOnboarding.errorFileDeleted'
            ),
            type: 'danger',
            autoDismissable: true,
          });
        },
      });
  }

  download(documentData: any) {
    const base64Content = documentData.content;
    const mimeType = documentData.documentTemplate
      ? documentData.documentTemplate.mimeType.value
      : documentData.mimeType.value;

    const byteCharacters = atob(base64Content);
    const byteNumbers = new Array(byteCharacters.length);
    for (let i = 0; i < byteCharacters.length; i++) {
      byteNumbers[i] = byteCharacters.charCodeAt(i);
    }
    const byteArray = new Uint8Array(byteNumbers);

    const blob = new Blob([byteArray], { type: mimeType });

    const blobUrl = URL.createObjectURL(blob);
    const a = document.createElement('a');
    a.href = blobUrl;
    const description = documentData.documentTemplate
      ? documentData.documentTemplate.description
      : documentData.description;
    a.download = `${description}.${mimeType.split('/')[1]}`;
    a.click();

    URL.revokeObjectURL(blobUrl);
  }

  onSubmit() {
    if (this.onboardingFormGroup.valid) {
      this.loading = true;
      const dialogRef = this.dialog.open(ConfirmDialogComponent, {
        data: {
          title: 'requestOnboarding.submitTitle',
          message: 'requestOnboarding.submitMessage',
          confirm: 'requestOnboarding.submitApprove',
          cancel: 'requestOnboarding.submitCancel',
        },
        width: '800px',
        hasBackdrop: false,
      });

      dialogRef.afterClosed().subscribe((result) => {
        if (result) {
          this.onboardingRequest.status.value = 'IN_PROGRESS';
          this._httpClient
            .post<OnboardingRequestDTO>(
              `${this.api_url}/onboarding-api/onboarding-request/${this.onboardingRequest?.id}/submit`,
              this.onboardingRequest
            )
            .pipe(
              tap((data) => {
                this.onboardingRequest = { ...data };
              })
            )
            .subscribe({
              next: () => {
                this.alertBannerService.show({
                  message: this.translocoService.translate(
                    'requestOnboarding.okSubmit'
                  ),
                  type: 'success',
                  autoDismissable: true,
                });
                this.loading = false;
              },
              error: (error) => {
                this.alertBannerService.show({
                  message: this.translocoService.translate(
                    'requestOnboarding.errorSubmit'
                  ),
                  type: 'danger',
                  autoDismissable: true,
                });
                this.loading = false;
              },
            });
        }
      });
    }
  }

  onCSRSelected(event: any) {
    const fileSelectend = event.target.files[0] ?? null;

    const body = new FormData();
    body.append('csr', fileSelectend);

    const dialogRef = this.dialog.open(ProcessingRequestDialogComponent, {
      data: {
        title: this.translocoService.translate('requestOnboarding.uploadCSR'),
        message: this.translocoService.translate(
          'requestOnboarding.messageCSR'
        ),
        file: body,
        requestId: this.onboardingRequest?.id,
        api_url: this.api_url,
      },
      width: '800px',
      hasBackdrop: false,
    });

    dialogRef.afterClosed().subscribe((result) => {});
  }

  downloadCredential() {
    const httpOptions = {
      responseType: 'blob' as 'blob',
    };
    const id = this.onboardingRequest.participantId;
    this._httpClient
      .get(`${this.api_url}/identity-api/certificate/${id}`, httpOptions)
      .subscribe((data) => {
        const blob = new Blob([data as BlobPart]);
        this.triggerFileDownload(blob as Blob);
      });
  }

  triggerFileDownload = (fileContent: Blob) => {
    const objectURL = URL.createObjectURL(fileContent);
    const fileLink = document.createElement('a');
    fileLink.href = objectURL;
    fileLink.download = 'certificate.pem';
    fileLink.click();

    URL.revokeObjectURL(objectURL);
  };
}
