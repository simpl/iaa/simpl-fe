import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { ProcessingRequestDialogComponent, ProcessingRequestDialogData } from './processing-request-dialog.component';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TranslocoTestingModule, TranslocoService } from '@jsverse/transloco';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatButtonModule } from '@angular/material/button';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { AppDomain, translocoTestConfiguration } from '@test-environment';

describe('ProcessingRequestDialogComponent', () => {
  let component: ProcessingRequestDialogComponent;
  let fixture: ComponentFixture<ProcessingRequestDialogComponent>;
  let httpMock: HttpTestingController;
  let mockDialogRef: jest.Mocked<MatDialogRef<ProcessingRequestDialogComponent>>;
  let translocoService: TranslocoService;

  const mockData: ProcessingRequestDialogData = {
    title: 'Processing Request',
    message: 'Processing your request...',
    requestId: '12345',
    file: { content: 'mock-file-content' },
    api_url: 'https://mock-api-url.com',
  };

  beforeEach(async () => {
    mockDialogRef = {
      close: jest.fn(),
    } as unknown as jest.Mocked<MatDialogRef<ProcessingRequestDialogComponent>>;

    await TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        TranslocoTestingModule.forRoot(translocoTestConfiguration(AppDomain.ONBOARDING)),
        MatProgressBarModule,
        MatButtonModule,
        NoopAnimationsModule,
        ProcessingRequestDialogComponent,
      ],
      providers: [
        { provide: MatDialogRef, useValue: mockDialogRef },
        { provide: MAT_DIALOG_DATA, useValue: mockData },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(ProcessingRequestDialogComponent);
    component = fixture.componentInstance;
    httpMock = TestBed.inject(HttpTestingController);
    translocoService = TestBed.inject(TranslocoService);
    fixture.detectChanges();
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it('should display the title and initial message', () => {
    const title = fixture.nativeElement.querySelector('h1[mat-dialog-title]');
    const message = fixture.nativeElement.querySelector('p');
    expect(title.textContent).toContain(mockData.title);
    expect(message.textContent).toContain(mockData.message);
  });

  it('should show progress bar initially', () => {
    const progressBar = fixture.nativeElement.querySelector('mat-progress-bar');
    expect(progressBar).not.toBeNull();
    expect(component.showProgress).toBeTruthy();
  });

  it('should make an HTTP POST request on init', () => {
    const req = httpMock.expectOne(`${mockData.api_url}/identity-api/csr/${mockData.requestId}`);
    expect(req.request.method).toBe('POST');
    expect(req.request.body).toEqual(mockData.file);
    req.flush({});
  });

  it('should handle successful HTTP response', fakeAsync(() => {
    const req = httpMock.expectOne(`${mockData.api_url}/identity-api/csr/${mockData.requestId}`);
    req.flush({});

    tick();
    fixture.detectChanges();

    expect(component.showProgress).toBeFalsy();
    expect(component.showButton).toBeTruthy();
    expect(component.data.message).toBe(
      translocoService.translate('requestOnboarding.messageCSROK')
    );

    const closeButton = fixture.nativeElement.querySelector('button');
    expect(closeButton).not.toBeNull();
  }));

  it('should handle failed HTTP response', fakeAsync(() => {
    const req = httpMock.expectOne(`${mockData.api_url}/identity-api/csr/${mockData.requestId}`);
    req.flush('Error', { status: 500, statusText: 'Server Error' });

    tick();
    fixture.detectChanges();

    expect(component.showProgress).toBeFalsy();
    expect(component.showButton).toBeTruthy();
    expect(component.data.message).toBe(
      translocoService.translate('requestOnboarding.messageCSRError')
    );
    const closeButton = fixture.nativeElement.querySelector('button');
    expect(closeButton).not.toBeNull();
  }));

  it('should close the dialog when the close button is clicked', () => {
    component.showButton = true;
    fixture.detectChanges();

    const closeButton = fixture.nativeElement.querySelector('button');
    closeButton.click();
    expect(mockDialogRef.close).toHaveBeenCalledWith(null);
  });
});
