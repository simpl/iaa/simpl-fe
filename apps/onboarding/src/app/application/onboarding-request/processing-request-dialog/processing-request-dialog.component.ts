import { Component, Inject, OnInit } from '@angular/core';
import {
  MAT_DIALOG_DATA,
  MatDialogActions,
  MatDialogContent,
  MatDialogRef,
  MatDialogTitle
} from '@angular/material/dialog';
import { MatButton } from '@angular/material/button';
import { TranslocoDirective, TranslocoService } from '@jsverse/transloco';
import { HttpClient } from '@angular/common/http';
import { MatProgressBar } from '@angular/material/progress-bar';
import { NgIf } from '@angular/common';

export interface ProcessingRequestDialogData {
  title: string;
  message: string;
  requestId: string;
  file: any;
  api_url: string;
}

@Component({
  selector: 'app-processing-request-dialog',
  template: `
    <ng-container *transloco="let t; prefix: 'requestOnboarding'">
      <h1 mat-dialog-title>{{ data.title }}</h1>
      <div mat-dialog-content>
        <p>{{ data.message }}</p>
        <mat-progress-bar *ngIf="showProgress" [mode]="modeProgress"></mat-progress-bar>
      </div>
      <div mat-dialog-actions>
        <button *ngIf="showButton" mat-button (click)="close()">{{ t('closeButton') }}</button>
      </div>
    </ng-container>
  `,
  imports: [
    MatDialogTitle,
    MatDialogContent,
    MatDialogActions,
    MatButton,
    TranslocoDirective,
    MatProgressBar,
    NgIf
  ],
  standalone: true
})
export class ProcessingRequestDialogComponent implements OnInit {

  modeProgress: 'determinate' | 'indeterminate' | 'buffer' | 'query' = 'indeterminate';
  message: string;
  showProgress = true;
  showButton = false;
  constructor(
    public dialogRef: MatDialogRef<ProcessingRequestDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ProcessingRequestDialogData,
    private _httpClient: HttpClient,
    private _translocoService: TranslocoService
  ) {
  }

  ngOnInit() {
    this._httpClient.post(`${this.data.api_url}/identity-api/csr/${this.data.requestId}`, this.data.file).pipe().subscribe({
      next: data => {
        this.showProgress = false;
        this.showButton = true;
        this.data.message = this._translocoService.translate(
          'requestOnboarding.messageCSROK'
        )
      },
      error: error => {
        this.showProgress = false;
        this.showButton = true;
        this.data.message = this._translocoService.translate(
          'requestOnboarding.messageCSRError'
        )
      }
    });
  }

  close(): void {
    this.dialogRef.close(null);
  }
}
