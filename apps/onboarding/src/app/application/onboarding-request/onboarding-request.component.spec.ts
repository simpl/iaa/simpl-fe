import { ComponentFixture, TestBed } from "@angular/core/testing";
import { OnboardingRequestComponent } from "./onboarding-request.component";
import { HttpClient, provideHttpClient } from "@angular/common/http";
import { TranslocoTestingModule } from "@jsverse/transloco";
import {
  AppDomain,
  provideApiUrl,
  translocoTestConfiguration,
} from "@test-environment";
import { NoopAnimationsModule } from "@angular/platform-browser/animations";
import { OnboardingStatusService } from "../onboarding-status/onboarding-status.service";
import { of } from "rxjs";
import { FormControl, FormGroup } from "@angular/forms";
import { By } from "@angular/platform-browser";
import { ProcessingRequestDialogComponent } from "./processing-request-dialog/processing-request-dialog.component";
import { KeycloakService } from 'keycloak-angular';
import { MatDialog } from '@angular/material/dialog';

describe("OnboardingRequestComponent", () => {
  let component: OnboardingRequestComponent;
  let fixture: ComponentFixture<OnboardingRequestComponent>;
  let onboardingStatusService: OnboardingStatusService;
  let httpClient: HttpClient;
  let fetchOnboardingStatusSpy: jest.SpyInstance;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        OnboardingRequestComponent,
        TranslocoTestingModule.forRoot(
          translocoTestConfiguration(AppDomain.ONBOARDING)
        ),
        NoopAnimationsModule,
      ],
      providers: [
        {
          provide: MatDialog,
          useValue: {
            open: jest.fn().mockReturnValue({
              afterClosed: () => of(true),
            }),
          },
        },
        {
          provide: KeycloakService,
          useValue: {
            getUserRoles: jest.fn().mockReturnValue(['USER', 'ADMIN']),
            getKeycloakInstance: jest.fn().mockReturnValue({
              tokenParsed: {
                name: 'Test User',
                resourceAccess: {
                  yourClientId: { roles: ['USER', 'ADMIN'] },
                  name: 'Test User'
                },
              },
            }),
          },
        },
        provideHttpClient(),
        provideApiUrl
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(OnboardingRequestComponent);
    component = fixture.componentInstance;
    onboardingStatusService = TestBed.inject(OnboardingStatusService);
    httpClient = TestBed.inject(HttpClient);
    fetchOnboardingStatusSpy = jest
      .spyOn(onboardingStatusService, "fetchOnboardingStatus")
      .mockReturnValue(
        of({
          content: [
            {
              id: "1",
              participantId: "1",
              participantType: {
                id: 1,
                value: "CONSUMER",
                label: "Consumer",
              },
              status: {
                id: 1,
                value: "IN_PROGRESS",
                label: "IN PROGRESS",
              },
              applicant: {
                username: "usertest",
                firstName: "User",
                lastName: "Test",
                email: "user@mail.com",
                password: "Password123!",
              },
              organization: "organization",
              documents: [],
              comments: [],
              rejectionCause: "documents missing",
              expirationTimeframe: 3000,
              creationTimestamp: new Date().toISOString(),
              updateTimestamp: new Date().toISOString(),
              lastStatusUpdateTimestamp: new Date().toISOString(),
            },
          ],
          page: {
            size: 1,
            number: 1,
            totalElements: 1,
            totalPages: 1,
          },
          empty: false,
        })
      );

      jest.spyOn(window, 'FileReader').mockImplementation(() => {
        const fileReaderMock: Partial<FileReader> = {
          onload: null,
          readAsDataURL: function(this: FileReader, _file: File) {
            Object.defineProperty(this, 'result', {
              value: "data:application/pdf;base64," + btoa("fakecontent"),
              writable: true,
            });
            if (this.onload) {
              this.onload(new ProgressEvent('load') as ProgressEvent<FileReader>);
            }
          }
        };
        return fileReaderMock as FileReader;
      });
      fixture.detectChanges();
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should handle file input and upload file logic correctly", () => {
    fixture.detectChanges();

    component.fileFormArray.push(
      component.createOnboardingFormGroup({
        id: "doc1",
        mimeType: { id: 1, value: "application/pdf", description: "application/pdf" },
        description: "Test Document",
        content: "base64",
        hasContent: false,
        filename: "",
        filesize: 0,
        updateTimestamp: new Date().toISOString(),
        documentTemplate: {
          id: "doc1",
          description: "Test Document",
          mandatory: true,
          mimeType: { id: 1, value: "application/pdf", description: "application/pdf" },
          creationTimestamp: new Date().toISOString(),
          updateTimestamp: new Date().toISOString(),
        },
        creationTimestamp: new Date().toISOString(),
      })
    );

    fixture.detectChanges();

    const fileInputDebugElements = fixture.debugElement.queryAll(By.css('input[type="file"]'));
    expect(fileInputDebugElements.length).toBeGreaterThan(0);

    const fileInputDebug = fileInputDebugElements[0];
    const fileInput = fileInputDebug.nativeElement as HTMLInputElement;
    const file = new File([""], "test-file.pdf");

    const dataTransfer = {
      items: {
        fileList: [] as File[],
        add(file: File) {
          this.fileList.push(file);
        }
      },
      get files() {
        return this.items.fileList;
      }
    };

    dataTransfer.items.add(file);

    Object.defineProperty(fileInput, 'files', {
      value: dataTransfer.files,
      writable: false,
      configurable: true
    });

    const onFileSelectedSpy = jest.spyOn(component, "onFileSelected");
    fileInput.dispatchEvent(new Event('change'));
    fixture.detectChanges();

    expect(onFileSelectedSpy).toHaveBeenCalledWith(expect.anything(), 0);
    const formGroup = component.fileFormArray.at(0) as FormGroup;
    expect(formGroup.get("name")?.value).toBe("test-file.pdf");
    expect(formGroup.get("hasContent")?.value).toBe(true);
  });

  it("should submit the form successfully", () => {
    fixture.detectChanges();
    jest.spyOn(httpClient, "post").mockReturnValue(of({}));

    component.onboardingRequest = {
      id: '1',
      participantId: '2',
      participantType: {
        id: 1,
        value: 'CONSUMER',
        label: 'Consumer',
      },
      status: {
        id: 1,
        value: 'IN_PROGRESS',
        label: 'IN PROGRESS',
      },
      organization: "organization",
      applicant: {
        username: 'testuser',
        firstName: 'Test',
        lastName: 'User',
        email: 'user@email.com',
        password: 'Password123!'
      },
      rejectionCause: 'documents missing',
      creationTimestamp: new Date().toISOString(),
      updateTimestamp: new Date().toISOString(),
      lastStatusUpdateTimestamp: new Date().toISOString(),
      expirationTimeframe: 3000,
      documents: [],
      comments: []
    };

    component.fileFormArray.push(
      component.createOnboardingFormGroup({
        id: "doc1",
        mimeType: { id: 1, value: "application/pdf", description: "application/pdf" },
        description: "Test Document",
        documentTemplate: {
          id: "doc1",
          description: "Test Document",
          mandatory: true,
          mimeType: { id: 1, value: "application/pdf", description: "application/pdf" },
          creationTimestamp: new Date().toISOString(),
          updateTimestamp: new Date().toISOString(),
        },
        creationTimestamp: new Date().toISOString(),
        content: "base64",
        hasContent: true,
        filename: "test-file.pdf",
        filesize: 1024,
        updateTimestamp: new Date().toISOString(),
      })
    );

    fixture.detectChanges();
    const onSubmitSpy = jest.spyOn(component, "onSubmit");

    const submitButton: HTMLButtonElement =
      fixture.debugElement.nativeElement.querySelector("#submit");
    submitButton.click();

    expect(onSubmitSpy).toHaveBeenCalled();
  });

  it('should send a comment and update the comments list', () => {
    const mockComment = { author: 'Test User', content: 'Test comment' };
    component.onboardingRequest = { id: '1', comments: [] } as any;
    jest.spyOn(httpClient, 'patch').mockReturnValue(of(mockComment));
    jest.spyOn(component['alertBannerService'], 'show').mockImplementation();

    component.sendPostComment('Test comment');

    expect(httpClient.patch).toHaveBeenCalledWith(
      `${component['api_url']}/onboarding-api/onboarding-request/1/comment`,
      { author: 'Test User', content: 'Test comment' }
    );
    expect(component.onboardingRequest.comments).toContainEqual(mockComment);
    expect(component['alertBannerService'].show).not.toHaveBeenCalledWith(
      expect.objectContaining({ type: 'danger' })
    );
  });

  it('should download a file', () => {
    const mockData = { content: 'mock-content', mimeType: { value: 'application/pdf' } };
    component.onboardingRequest = { id: '1' } as any;
    jest.spyOn(httpClient, 'get').mockReturnValue(of(mockData));
    const downloadSpy = jest.spyOn(component, 'download').mockImplementation();

    component.downloadFile('file-id');

    expect(httpClient.get).toHaveBeenCalledWith(
      `${component['api_url']}/onboarding-api/onboarding-request/1/document/file-id`
    );
    expect(downloadSpy).toHaveBeenCalledWith(mockData);
  });

  it('should delete a file and update the form array', () => {
    component.onboardingRequest = { id: '1' } as any;
    component.fileFormArray.push(
      new FormGroup({
        content: new FormControl('mock-content'),
        name: new FormControl('mock-name'),
        hasContent: new FormControl(true),
        fileSize: new FormControl(1024),
      })
    );
    jest.spyOn(httpClient, 'delete').mockReturnValue(of({}));
    jest.spyOn(component['alertBannerService'], 'show').mockImplementation();

    component.deleteFile('file-id', 0);

    expect(httpClient.delete).toHaveBeenCalledWith(
      `${component['api_url']}/onboarding-api/onboarding-request/1/document/file-id`
    );
    expect(component.fileFormArray.at(0).value).toEqual({
      content: null,
      name: null,
      hasContent: null,
      fileSize: null,
    });
    expect(component['alertBannerService'].show).toHaveBeenCalledWith(
      expect.objectContaining({ type: 'success' })
    );
  });

  it('should open CSR upload dialog on file selection', () => {
    const dialogSpy = jest.spyOn(component['dialog'], 'open');
    component.onboardingRequest = { id: '1' } as any;
    const mockEvent = {
      target: { files: [new File([''], 'mock.csr')] },
    };

    component.onCSRSelected(mockEvent);

    expect(dialogSpy).toHaveBeenCalledWith(ProcessingRequestDialogComponent, expect.any(Object));
  });

  it('should download credentials', () => {
    component.onboardingRequest = { participantId: '1' } as any;
    const blob = new Blob(['mock'], { type: 'application/x-pem-file' });
    jest.spyOn(httpClient, 'get').mockReturnValue(of(blob));
    const triggerDownloadSpy = jest.spyOn(component, 'triggerFileDownload').mockImplementation();

    component.downloadCredential();

    expect(httpClient.get).toHaveBeenCalledWith(
      `${component['api_url']}/identity-api/certificate/1`,
      expect.any(Object)
    );
    expect(triggerDownloadSpy).toHaveBeenCalledWith(blob);
  });

  it('should validate mandatory files', () => {
    const formGroup = new FormGroup({
      mandatory: new FormControl(true),
      hasContent: new FormControl(false),
    });

    const result = component.mandatoryFileValidator()(formGroup);

    expect(result).toEqual({ requiredFileMissing: true });

    formGroup.patchValue({ hasContent: true });
    const validResult = component.mandatoryFileValidator()(formGroup);

    expect(validResult).toBeNull();
  });

  it('should open dialog and submit request on confirmation', () => {
    component.onboardingRequest = {
      id: '1',
      status: { value: 'PENDING' },
    } as any;
    const dialogSpy = jest.spyOn(component['dialog'], 'open').mockReturnValue({
      afterClosed: () => of(true),
    } as any);
    jest.spyOn(httpClient, 'post').mockReturnValue(of({ id: '1', status: { value: 'IN_PROGRESS' } }));
    jest.spyOn(component['alertBannerService'], 'show');

    component.onSubmit();

    expect(dialogSpy).toHaveBeenCalled();
    expect(httpClient.post).toHaveBeenCalledWith(
      `${component['api_url']}/onboarding-api/onboarding-request/1/submit`,
      expect.any(Object)
    );
    expect(component.onboardingRequest.status.value).toBe('IN_PROGRESS');
    expect(component['alertBannerService'].show).toHaveBeenCalledWith(
      expect.objectContaining({ type: 'success' })
    );
  });

  it('should create a valid onboarding form group', () => {
    const mock = {
      id: 'doc1',
      mimeType: { id: 1, value: 'application/pdf' },
      description: 'Test',
      mandatory: true,
      content: 'mock-content',
      hasContent: true,
      filename: 'test-file.pdf',
      filesize: 1024,
      updateTimestamp: new Date().toISOString(),
    };

    const formGroup = component.createOnboardingFormGroup(mock as any);

    expect(formGroup.get('id')?.value).toBe('doc1');
    expect(formGroup.get('mimeType')?.value).toBe('application/pdf');
    expect(formGroup.get('description')?.value).toBe('Test');
    expect(formGroup.get('mandatory')?.value).toBe(true);
    expect(formGroup.valid).toBe(true);
  });

  it('should initialize onboarding request and forms on ngOnInit', () => {
    component.ngOnInit();
    expect(fetchOnboardingStatusSpy).toHaveBeenCalled();
    expect(component.fileFormArray.length).toBeGreaterThanOrEqual(0);
    expect(component.user).toBe('Test User');
    expect(component.roles).toContain('USER');
    expect(component.roles).toContain('ADMIN');
  });

  it('should update onboarding request status correctly', () => {
    component.onboardingRequest = { status: { value: 'PENDING' } } as any;

    component.onboardingRequest.status.value = 'IN_PROGRESS';

    expect(component.onboardingRequest.status.value).toBe('IN_PROGRESS');
  });

});
