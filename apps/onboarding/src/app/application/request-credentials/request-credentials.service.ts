import { Injectable } from "@angular/core";
import { ApiService } from "@fe-simpl/api";
import { Observable } from 'rxjs';
import {
  ApplicationRequestProperties,
  NewUserResponse, ParticipantType
} from '@fe-simpl/api-types';

@Injectable({
  providedIn: "root",
})
export class RequestCredentialsService {
  constructor(private readonly apiService: ApiService) {}

  applicationRequest(
    data: ApplicationRequestProperties
  ): Observable<NewUserResponse> {

    return this.apiService.post<NewUserResponse, ApplicationRequestProperties>(
      "/onboarding-api/onboarding-request",
      data
    )
  }

  getParticipantTypeList(): Observable<Array<ParticipantType>> {
    return this.apiService.get<Array<ParticipantType>>(
      `/onboarding-api/participant-type`
    );
  }

}
