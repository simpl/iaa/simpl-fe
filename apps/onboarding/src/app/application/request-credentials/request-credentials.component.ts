import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  AbstractControl,
  FormControl,
  FormGroup,
  ReactiveFormsModule,
  ValidationErrors,
  ValidatorFn,
  Validators
} from '@angular/forms';
import {
  MatButton,
  MatButtonModule,
  MatIconButton
} from '@angular/material/button';
import {
  MatFormField,
  MatFormFieldModule,
  MatHint,
  MatLabel
} from '@angular/material/form-field';
import { MatInput, MatError, MatInputModule } from '@angular/material/input';
import { MatOption } from '@angular/material/autocomplete';
import { MatSelect, MatSelectModule } from '@angular/material/select';
import { TranslocoDirective, TranslocoService } from '@jsverse/transloco';
import { ParticipantType } from '@fe-simpl/api-types';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { ActivatedRoute, Router } from '@angular/router';
import { MatIcon, MatIconModule } from '@angular/material/icon';
import { RequestCredentialsService } from './request-credentials.service';
import { catchError, finalize, tap, throwError } from 'rxjs';
import { AlertBannerService } from '@fe-simpl/alert-banner';

@Component({
  selector: 'app-request-credentials',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule,
    TranslocoDirective,
    MatProgressSpinnerModule,
    MatFormFieldModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatSelectModule,
    MatButton,
    MatError,
    MatInput,
    MatFormField,
    MatHint,
    MatOption,
    MatSelect,
    MatIcon,
    MatIconButton,
    MatLabel
  ],
  templateUrl: './request-credentials.component.html',
  styleUrl: './request-credentials.component.css'
})
export class RequestCredentialsComponent implements OnInit {
  applicationForm: FormGroup;
  participants: ParticipantType[] = [];
  passwordVisible = false;
  passwordConfirmVisible = false;
  loading = false;
  credentials: { username?: string, password?: string } = { username: '', password: '' };

  constructor(
    private readonly router: Router,
    private readonly _requestCredentialsService: RequestCredentialsService,
    private readonly  _alertBannerService: AlertBannerService,
    private readonly _translocoService: TranslocoService,
    private readonly route: ActivatedRoute
  ) {
  }

  ngOnInit(): void {
    this.applicationForm = new FormGroup(
      {
        applicant: new FormGroup({
          username: new FormControl('', [
            Validators.required,
            Validators.pattern(/^[0-9A-Za-zàèéìòù@.-_]+$/)
          ]),
          firstName: new FormControl('', [
            Validators.required,
            Validators.pattern(/^[A-Za-zàèéìòù]+(\s[A-Za-zàèéìòù]+)*$/)
          ]),
          lastName: new FormControl('', [
            Validators.required,
            Validators.pattern(/^[A-Za-zàèéìòù]+(['-\s]([A-Za-zàèéìòù]+))*$/)
          ]),
          email: new FormControl('', [
            Validators.required,
            Validators.email,
            Validators.pattern(/^[a-zA-Z0-9_+&*-]+(?:\.[a-zA-Z0-9_+&*-]+){0,255}@(?:[a-zA-Z0-9-]+\.){1,255}[a-zA-Z]{2,7}$/)
          ]),
          password: new FormControl('', [
            Validators.required,
            Validators.minLength(10),
            Validators.maxLength(64),
            detailedPasswordValidation
          ]),
          confirmPassword: new FormControl('', [Validators.required])
        }, { validators: confirmPasswordValidator }),
        participantType: new FormGroup({
          id: new FormControl('', [Validators.required])
        }),
        organization: new FormControl('', [
          Validators.required,
          Validators.pattern(/^[0-9A-Za-zàèéìòù]+((['-\s]|(\s&\s))([0-9A-Za-zàèéìòù]+))*$/)
        ])
      }
    );


    this._requestCredentialsService.getParticipantTypeList().pipe(
      tap(participantType => {
        this.participants = participantType;
      })
    ).subscribe();

  }

  onSubmit() {
    if (this.applicationForm.valid) {
      this.loading = true;
      this._requestCredentialsService.applicationRequest(this.applicationForm.getRawValue())
        .pipe(
          tap(res => {
            this.credentials.username = this.applicationForm.get('applicant.username')?.value;
            this.credentials.password = this.applicationForm.get('applicant.password')?.value;
          }),
          catchError((error) => {
            const code = error as { status: number };
            if (code.status === 409) {
              this._alertBannerService.show({
                message: this._translocoService.translate('requestApplication.errorUnique'),
                type: 'danger',
                strong: 'Attention!',
                autoDismissable: true,
                timeToDismissed: 6000
              });
            }
            return throwError(() => error);
          }),
          finalize(() => {
            this.loading = false;
          })
        )
        .subscribe();
    }

  }

  navigateToOnboarding() {
    this.router.navigate(['/application/additional-request'], { relativeTo: this.route }).then();
  }
}

export const confirmPasswordValidator: ValidatorFn = (
  control: AbstractControl
): ValidationErrors | null => {
  const passwordControl = control.get('password');
  const confirmPasswordControl = control.get('confirmPassword');

  if (!passwordControl || !confirmPasswordControl) {
    return null;
  }

  const match = passwordControl.value === confirmPasswordControl.value;

  if (!match) {
    confirmPasswordControl.setErrors({
      ...confirmPasswordControl.errors,
      noMatch: true
    });
  } else {
    confirmPasswordControl.setErrors(
      confirmPasswordControl.errors
        ? { ...confirmPasswordControl.errors }
        : null
    );
  }

  return null;
};

export const detailedPasswordValidation: ValidatorFn = (
  control: AbstractControl
): ValidationErrors | null => {
  const value = control.value;
  const errors: any = {};

  if (!value || value.length < 10) {
    errors.minLength = true;
  }

  if (value.length > 64) {
    errors.maxLength = true;
  }

  if (!/[A-Z]/.test(value)) {
    errors.uppercase = true;
  }

  if (!/[a-z]/.test(value)) {
    errors.lowercase = true;
  }

  if (!/\d/.test(value)) {
    errors.numeric = true;
  }

  if (!/[!"#$%&'()+,-./:;<=>?@[\]^_`{|}~]/.test(value)) {
    errors.specialChar = true;
  }

  const forbiddenValues = [
    control.root.get('applicant.email')?.value,
    control.root.get('organization')?.value,
    control.root.get('applicant.lastName')?.value,
    control.root.get('applicant.firstName')?.value,
    control.root.get('applicant.username')?.value
  ];

  forbiddenValues.forEach((valueToCheck) => {
    if (valueToCheck) {
      for (let i = 0; i < valueToCheck.length; i++) {
        for (let j = i + 5; j <= valueToCheck.length; j++) {
          const portion = valueToCheck.substring(i, j).toLowerCase();
          if (value.toLowerCase().includes(portion)) {
            errors.forbidden = true;
            break;
          }
        }
      }
    }
  });

  return Object.keys(errors).length > 0 ? errors : null;
};
