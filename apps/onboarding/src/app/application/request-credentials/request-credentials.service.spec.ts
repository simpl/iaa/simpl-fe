import { TestBed } from "@angular/core/testing";

import { RequestCredentialsService } from "./request-credentials.service";
import { provideHttpClient } from "@angular/common/http";
import { provideApiUrl } from "@test-environment";
import { ApplicationRequestProperties } from "@fe-simpl/api-types";
import { of } from "rxjs";

describe("RequestApplicationService", () => {
  let service: RequestCredentialsService;
  const applicationRequestMock = {
    username: "username",
    password: "password",
    loginLink: "loginLin",
  };
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideHttpClient(), provideApiUrl],
    });
    service = TestBed.inject(RequestCredentialsService);
  });

  it("should create", () => {
    expect(service).toBeTruthy();
  });

  it("should applicationRequest", () => {
    jest
      .spyOn(service, "applicationRequest")
      .mockReturnValue(of(applicationRequestMock));
    service
      .applicationRequest({} as ApplicationRequestProperties)
      .subscribe((participantDto) => {
        expect(participantDto).toEqual(applicationRequestMock);
      });
  });
});
