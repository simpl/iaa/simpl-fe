describe('template spec', () => {
  it('passes', () => {
    cy.visit('/application/request')
    cy.get('form').get('mat-form-field').eq(0).type('test@alten.it')
    cy.get('form').get('mat-form-field').eq(1).type('alten')
    cy.get('form').get('mat-select').click().get('mat-option').contains('Consumer').click();
    cy.get('form').get('mat-form-field').eq(3).type('morewski')
    cy.get('form').get('mat-form-field').eq(4).type('Luca')
    cy.get('form').get('mat-form-field').eq(5).type('Moro')
    cy.get('form').get('mat-form-field').eq(6).type('Luca1989!')
    cy.get('form').get('mat-form-field').eq(7).type('Luca1989!')
    return cy.get('form').get('button').click()

    /*cy.on('uncaught:exception', (err, runnable) => {
      return false
    })*/
  })
})
