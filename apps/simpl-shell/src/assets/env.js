(function(window) {
  window.env = window.env || {};

  // Environment variables
  window["env"]["api_Url"] = "https://t1.authority.dev.aruba-simpl.cloud";
  window["env"]["keycloakConfig_url"] =
    "https://t1.authority.dev.aruba-simpl.cloud/auth";
  window["env"]["keycloakConfig_realm"] = "authority";
  window["env"]["keycloakConfig_clientId"] ="frontend-cli";

})(this);
