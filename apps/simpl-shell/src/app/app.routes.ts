import { NxWelcomeComponent } from './nx-welcome.component';
import { Route } from '@angular/router';
import { NoAuthGuard } from "@fe-simpl/auth";

export const appRoutes: Route[] = [
  {
    path: 'users-roles',
    loadChildren: () =>
      import('usersandroles/Routes').then((m) => m.remoteRoutes),
  },
  {
    path: 'sap',
    loadChildren: () => import('sap/Routes').then((m) => m.remoteRoutes),
  },
  {
    path: 'participant-utility',
    loadChildren: () =>
      import('participant-utility/Routes').then((m) => m.remoteRoutes),
  },
  {
    path: 'onboarding',
    loadChildren: () => import('onboarding/Routes').then((m) => m.remoteRoutes),
  },
  {
    path: '',
    canActivate: [NoAuthGuard],
    component: NxWelcomeComponent,
  },
];
