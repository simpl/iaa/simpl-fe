import { APP_INITIALIZER, ApplicationConfig, isDevMode } from '@angular/core';
import { provideRouter } from '@angular/router';
import { appRoutes } from './app.routes';
import { provideTransloco } from '@jsverse/transloco';
import { I18nService } from '@fe-simpl/i18n';
import { HTTP_INTERCEPTORS, provideHttpClient, withInterceptors, withInterceptorsFromDi } from '@angular/common/http';
import { errorHandlingInterceptor } from '@fe-simpl/error-handler';
import { API_URL } from '@fe-simpl/api';
import { provideAnimationsAsync } from '@angular/platform-browser/animations/async';
import { KeycloakBearerInterceptor, KeycloakService } from 'keycloak-angular';
import { environment } from '../environments/environment';

export const initializeKeycloak = (keycloak: KeycloakService) => {
  return () =>
    keycloak.init({
      config: {
        url: environment.keycloakConfig_url,
        clientId: environment.keycloakConfig_clientId,
        realm: environment.keycloakConfig_realm,
      },
      initOptions: {
        onLoad: 'check-sso',
        silentCheckSsoRedirectUri: window.location.origin + '/assets/silent-check-sso.html'
      },
      bearerExcludedUrls: ['/public'],
    }).then(r => {
      console.log(r);
    });
};

export const appConfig: ApplicationConfig = {
  providers: [
    KeycloakService,
    provideRouter(appRoutes),
    provideAnimationsAsync(),
    provideHttpClient(withInterceptorsFromDi(), withInterceptors([errorHandlingInterceptor])),
    provideTransloco({
      config: {
        availableLangs: ['en', 'es'],
        defaultLang: 'en',
        prodMode: !isDevMode(),
      },
      loader: I18nService,
    }),
    { provide: API_URL, useValue: environment.api_Url },
    {
      provide: APP_INITIALIZER,
      useFactory: initializeKeycloak,
      multi: true,
      deps: [KeycloakService],
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: KeycloakBearerInterceptor,
      multi: true,
    }
  ],
};
