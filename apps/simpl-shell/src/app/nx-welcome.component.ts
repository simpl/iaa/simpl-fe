import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButton } from '@angular/material/button';
import { TranslocoDirective } from '@jsverse/transloco';
import { RouterLink } from '@angular/router';

@Component({
  selector: 'app-nx-welcome',
  standalone: true,
  imports: [CommonModule, MatButton, TranslocoDirective, RouterLink],
  template: `
    <div class="d-flex justify-content-center align-items-center px-2 min-vh-100">
      <div class="col-12 col-lg-4">
        <ng-container *transloco="let t; prefix: 'info'">
          <h1 class="text-primary pb-3">{{ t('title') }}</h1>
          <div class="d-flex justify-content-center flex-column gap-3">
            <h5>First ask for credentials to access the dataspace, then you will be redirected to the form to start
              registration.</h5>
            <div>
              <button mat-flat-button color="accent" [routerLink]="['/application/request']">Register here
              </button>
            </div>
          </div>
        </ng-container>
      </div>
    </div>

  `,
  styles: [],
})
export class NxWelcomeComponent {
}
