import { CommonModule } from '@angular/common';
import { Component, inject, OnInit, ViewChild } from '@angular/core';
import { TranslocoDirective, TranslocoService } from '@jsverse/transloco';
import { AgentConfigurationService } from '../agent-configuration.service';
import { EuiButtonModule } from '@eui/components/eui-button';
import { EuiLabelModule } from '@eui/components/eui-label';
import { EuiIconModule } from '@eui/components/eui-icon';
import { EuiDialogComponent, EuiDialogModule } from '@eui/components/eui-dialog';
import { EuiProgressBarModule } from '@eui/components/eui-progress-bar';
import { FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { EuiFileUploadModule } from '@eui/components/eui-file-upload';

@Component({
  selector: 'app-credentials-import',
  standalone: true,
  imports: [
    CommonModule,
    TranslocoDirective,
    EuiButtonModule,
    EuiLabelModule,
    EuiIconModule,
    EuiDialogModule,
    EuiProgressBarModule,
    ReactiveFormsModule,
    EuiFileUploadModule,
  ],
  templateUrl: './credentials-import.component.html',
})
export class CredentialsImportComponent implements OnInit {
  public form: FormGroup;
  public progress = 0;
  status: 'loading' | 'prompt' | 'success' | 'error' = 'prompt';
  @ViewChild('dialog') dialog: EuiDialogComponent;
  private readonly agentConfigurationService = inject(
    AgentConfigurationService
  );
  private readonly translocoService = inject(TranslocoService);

  ngOnInit() {
    this.form = new FormGroup({
      file: new FormControl(null, Validators.required),
    });

    this.form.statusChanges.subscribe((status) => {
      if (status === 'VALID') {
        this.dialog?.enableAcceptButton();
      } else {
        this.dialog?.disableAcceptButton();
      }
    });
  }

  uploadCredentials() {
      this.status = 'loading';
      console.log(this.form.get('file')?.value[0])
      this.agentConfigurationService.uploadCredentials(this.form.get('file')?.value[0]).subscribe({
        next: () => {
          this.status = 'success';
          this.dialog.acceptLabel = this.translocoService.translate('agentConfiguration.credentialsImport.ok')
          this.form.reset()
          this.dialog.openDialog();
        },
        error: () => {
          this.status = 'error';
          this.dialog.acceptLabel = this.translocoService.translate('agentConfiguration.credentialsImport.ok')
          this.form?.reset()
          this.dialog.openDialog();
        },
      });
  }

  openDialog() {
    this.dialog.openDialog();
    this.dialog.disableAcceptButton();
  }

  onAccept() {

    if (this.status !== 'success' && this.status !== 'error') {
      this.uploadCredentials()
    }

    if (this.status === 'success' || this.status === 'error') {
      this.status = 'prompt';
      this.dialog.hasDismissButton = true;
      this.dialog.acceptLabel = this.translocoService.translate(
        'agentConfiguration.csrGeneration.ok'
      );
    }
  }

  onDismiss() {
    this.status = 'prompt';
    this.form?.reset();
    this.dialog.acceptLabel = this.translocoService.translate(
      'agentConfiguration.credentialsImport.confirm'
    );
  }
}
