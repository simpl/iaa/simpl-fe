import { HttpClient, HttpResponse } from '@angular/common/http';
import { inject, Injectable } from '@angular/core';
import { API_URL, ApiService } from '@fe-simpl/api';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface KeyPair {
  privateKey: string;
  publicKey: string;
}

export interface CsrDetails {
  commonName: string;
  organization: string;
  organizationalUnit: string;
  country: string
}

@Injectable({ providedIn: 'root' })
export class AgentConfigurationService {
  private readonly apiService = inject(ApiService);
  private readonly http = inject(HttpClient);
  private readonly api_url = inject(API_URL);

  hasKeypair() {
    return this.http.head(`${this.api_url}/auth-api/keypair`, {
      observe: 'response'
    }).pipe(
      map((res: HttpResponse<any>) => res.status !== 204)
    );
  }

  generateKeypair() {
    return this.apiService.post(`/auth-api/keypair/generate`);
  }

  importKeyPair(keypair: KeyPair){
    return this.apiService.post(
      `/auth-api/keypair`, keypair
    );
  }

  generateCsr(csrDetails: CsrDetails){
    const url = `/auth-api/csr/generate`;
    return this.http.post(
      `${this.api_url}${url}`, csrDetails, {
        responseType: 'blob',
        observe: 'response',
      }
    ).pipe(
      map((response) => {
        const contentDisposition = response.headers.get('Content-Disposition');
        const fileName = this.extractFileName(contentDisposition);
        return {
          file: response.body!,
          fileName,
        };
      })
    );
  }

  uploadCredentials(file: File): Observable<void> {
    const body = new FormData();
    body.append("file", file);
    return this.apiService.uploadFile("/user-api/credential", body);
  }

  private extractFileName(contentDisposition: string | null): string {
    if (!contentDisposition) {
      return 'csr.pem';
    }
    const matches = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/.exec(contentDisposition);
    return matches && matches[1] ? matches[1].replace(/['"]/g, '') : 'csr.pem';
  }
}
