import { CommonModule } from '@angular/common';
import { Component, inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { TranslocoDirective, TranslocoService } from '@jsverse/transloco';
import { AgentConfigurationService, KeyPair } from '../agent-configuration.service';
import { AlertBannerService } from '@fe-simpl/alert-banner';
import { EuiTextAreaModule } from '@eui/components/eui-textarea';
import { EuiButtonModule } from '@eui/components/eui-button';
import { EuiIconModule } from '@eui/components/eui-icon';
import { EuiLabelModule } from '@eui/components/eui-label';

@Component({
  selector: 'app-keypair-import',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule,
    TranslocoDirective,
    EuiTextAreaModule,
    EuiButtonModule,
    EuiIconModule,
    EuiLabelModule,
  ],
  templateUrl: './keypair-import.component.html'
})
export class KeypairImportComponent implements OnInit {
  keypairImportForm: FormGroup;
  loading = false;
  private readonly service = inject(AgentConfigurationService);
  private readonly alertBannerService = inject(AlertBannerService);
  private readonly translocoService = inject(TranslocoService);

  privateKeyUploaded = false;
  publicKeyUploaded = false;
  privateFileInfo: { name: string; size: number } | null = null;
  publicFileInfo: { name: string; size: number } | null = null;

  ngOnInit(): void {
    this.keypairImportForm = new FormGroup({
      privateKey: new FormControl('', [Validators.required]),
      publicKey: new FormControl('', [Validators.required]),
    });
  }

  onFileUploaded(event: any, key: 'private' | 'public'): void {
    const file = event.target.files[0] ?? null;
    if (file) {
      const reader = new FileReader();
      reader.onload = () => {
        const fileContent = reader.result as string;
        this.keypairImportForm.patchValue({
          [key + 'Key']: fileContent,
        });
        this.keypairImportForm.get(key + 'Key')?.disable();

        if (key === 'private') {
          this.privateKeyUploaded = true;
          this.privateFileInfo = { name: file.name, size: file.size };
        } else {
          this.publicKeyUploaded = true;
          this.publicFileInfo = { name: file.name, size: file.size };
        }
      };
      reader.readAsText(file);
    }
  }

  fieldsValid() {
    const privateValid = this.keypairImportForm.get('privateKey')?.value !== '';
    const publicValid = this.keypairImportForm.get('publicKey')?.value !== '';
    return privateValid && publicValid;
  }

  onFileDeleted(key: 'private' | 'public') {
    this.keypairImportForm.patchValue({
      [key + 'Key']: '',
    });
    this.keypairImportForm.get(key + 'Key')?.enable();

    if (key === 'private') {
      this.privateKeyUploaded = false;
      this.privateFileInfo = null;
    } else {
      this.publicKeyUploaded = false;
      this.publicFileInfo = null;
    }
  }

  onSubmit() {
    if (this.fieldsValid()) {
      this.loading = true;

      const keypair: KeyPair = {
        privateKey: this.keypairImportForm.get('privateKey')?.value ?? '',
        publicKey: this.keypairImportForm.get('publicKey')?.value ?? '',
      };

      this.service.importKeyPair(keypair).subscribe({
        next: () => {
          this.loading = false;
          this.alertBannerService.show({
            message: this.translocoService.translate(
              'agentConfiguration.importKeypair.success'
            ),
            type: 'info',
          });
          this.keypairImportForm.reset();
        },
      });
    }
  }

  get privateKey() {
    return this.keypairImportForm.get('privateKey')?.value;
  }

  get publicKey() {
    return this.keypairImportForm.get('publicKey')?.value;
  }
}
