import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KeypairImportComponent } from './keypair-import.component';
import { provideHttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { TranslocoTestingModule } from '@jsverse/transloco';
import { translocoTestConfiguration, AppDomain } from '@test-environment';
import { of } from 'rxjs';
import { AgentConfigurationService } from '../agent-configuration.service';


describe('KeypairImportComponent', () => {
  let component: KeypairImportComponent;
  let fixture: ComponentFixture<KeypairImportComponent>;

  const mockAgentConfigurationService = {
    importKeyPair: jest.fn().mockReturnValue(of({}))
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        KeypairImportComponent,
        HttpClientTestingModule,
        TranslocoTestingModule.forRoot(
          translocoTestConfiguration(AppDomain.PARTICIPANT_UTILITY)
        ),
        NoopAnimationsModule
      ],
      providers: [
        provideHttpClient(),
        { provide: AgentConfigurationService, useValue: mockAgentConfigurationService }
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(KeypairImportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize the form with empty fields', () => {
    expect(component.keypairImportForm.value).toEqual({
      privateKey: '',
      publicKey: '',
    });
  });

  it('should handle file upload for privateKey', () => {
    const file = new File(['private-key-content'], 'private-key.pem', { type: 'text/plain' });
    const event = { target: { files: [file] } } as unknown as Event;

    const fileReaderMock = {
      readAsText: jest.fn(),
      onload: jest.fn(),
    };

    jest.spyOn(window, 'FileReader').mockImplementation(() => fileReaderMock as unknown as FileReader);

    component.onFileUploaded(event, 'private');
    expect(fileReaderMock.readAsText).toHaveBeenCalledWith(file);

    Object.defineProperty(fileReaderMock, 'result', {
      value: 'private-key-content',
      writable: true,
    });
    fileReaderMock.onload!({} as ProgressEvent);

    expect(component.keypairImportForm.get('privateKey')?.value).toBe('private-key-content');
    expect(component.privateKeyUploaded).toBe(true);
    expect(component.privateFileInfo).toEqual({ name: 'private-key.pem', size: file.size });
  });

  it('should handle file deletion for publicKey', () => {
    component.keypairImportForm.patchValue({ publicKey: 'public-key-content' });
    component.publicKeyUploaded = true;
    component.publicFileInfo = { name: 'public-key.pem', size: 1024 };

    component.onFileDeleted('public');

    expect(component.keypairImportForm.get('publicKey')?.value).toBe('');
    expect(component.keypairImportForm.get('publicKey')?.enabled).toBe(true);
    expect(component.publicKeyUploaded).toBe(false);
    expect(component.publicFileInfo).toBeNull();
  });

  it('should validate fields correctly', () => {
    expect(component.fieldsValid()).toBe(false);

    component.keypairImportForm.patchValue({
      privateKey: 'mock-private-key',
      publicKey: 'mock-public-key',
    });

    expect(component.fieldsValid()).toBe(true);
  });

  it('should submit the form successfully', () => {
    component.keypairImportForm.patchValue({
      privateKey: 'mock-private-key',
      publicKey: 'mock-public-key',
    });

    mockAgentConfigurationService.importKeyPair.mockReturnValue(of(null));

    component.onSubmit();

    expect(mockAgentConfigurationService.importKeyPair).toHaveBeenCalled();
    expect(component.loading).toBe(false);
    expect(component.keypairImportForm.value).toEqual({
      privateKey: null,
      publicKey: null,
    });
  });
});
