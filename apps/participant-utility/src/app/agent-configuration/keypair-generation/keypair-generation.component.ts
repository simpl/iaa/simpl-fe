import { Component, inject, ViewChild } from '@angular/core';
import { TranslocoDirective, TranslocoService } from '@jsverse/transloco';
import { CommonModule } from '@angular/common';
import { EuiButtonModule } from '@eui/components/eui-button';
import { EuiIconModule } from '@eui/components/eui-icon';
import { EuiLabelModule } from '@eui/components/eui-label';
import { EuiDialogComponent, EuiDialogModule } from '@eui/components/eui-dialog';
import { AgentConfigurationService } from '../agent-configuration.service';

@Component({
  selector: 'app-keypair-generation',
  standalone: true,
  imports: [
    CommonModule,
    TranslocoDirective,
    EuiButtonModule,
    EuiIconModule,
    EuiLabelModule,
    EuiDialogModule
  ],
  templateUrl: './keypair-generation.component.html',
})
export class KeypairGenerationComponent {

  status: "loading" | "prompt" | "success" | "error" = "prompt"
  @ViewChild('dialog') dialog: EuiDialogComponent;
  private readonly agentConfigurationService = inject(AgentConfigurationService);
  private readonly translocoService = inject(TranslocoService);

  public openDialog(): void {
    this.dialog.openDialog();
  }

  public onAccept(): void {
    if (this.status !== "success") {
      this.generateKeyPair();
    }

    if (this.status === "success") {
      this.status = 'prompt'
      this.dialog.hasDismissButton = true;
      this.dialog.acceptLabel = this.translocoService.translate('agentConfiguration.keypairGeneration.confirm')
    }
  }

  generateKeyPair(){
    this.status = "loading";
    this.agentConfigurationService.generateKeypair().subscribe({
      next: () => {
        this.status = "success";
        this.dialog.acceptLabel = this.translocoService.translate('agentConfiguration.keypairGeneration.ok')
        this.dialog.hasDismissButton = false;
        this.dialog.openDialog();
      },
      error: () => {
        this.status = "error";
        this.dialog.openDialog();
      }
    })
  }
}
