import { inject, Injectable } from '@angular/core';

import { map, Observable, of, switchMap, throwError } from 'rxjs';
import { EchoService, EchoTResponseDTO } from '../echo/echo.service';
import { AlertBannerService } from '@fe-simpl/alert-banner';
import { TranslocoService } from '@jsverse/transloco';

@Injectable({
  providedIn: 'root'
})
export class EchoResolver {
  private readonly api = inject(EchoService);
  private readonly alert = inject(AlertBannerService);
  private readonly transloco = inject(TranslocoService);

  resolve(): Observable<EchoTResponseDTO | boolean> {
    return this.api.hasCredential().pipe(
      switchMap((response) => (response) ? this.api.echo() : throwError('err')),
      map(result => {
        if (typeof result === "object" && result.connectionStatus === "NOT_CONNECTED") {
          this.alert.show({
            type: "danger",
            message: this.transloco.translate("echo.notConnectedAlert"),
          });
        }
        return result;
      })
    );
  }
}
