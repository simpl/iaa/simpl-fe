import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslocoDirective, TranslocoService } from '@jsverse/transloco';
import { PingService } from './ping.service';
import { catchError, tap, throwError } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import { FormControl, ReactiveFormsModule, Validators } from '@angular/forms';
import { AlertBannerService } from '@fe-simpl/alert-banner';
import { EuiAllModule } from '@eui/components';

@Component({
  selector: 'app-ping',
  standalone: true,
  imports: [
    CommonModule,
    TranslocoDirective,
    ReactiveFormsModule,
    EuiAllModule,
  ],
  templateUrl: './ping.component.html',
  styleUrl: './ping.component.css',
})
export class PingComponent {
  fqdnControl = new FormControl('', [Validators.required]);
  loading = false;

  constructor(
    private readonly pingService: PingService,
    private readonly _alertService: AlertBannerService,
    private readonly _translocoService: TranslocoService
  ) {}

  onPing() {
    if (this.fqdnControl.valid) {
      const value = this.fqdnControl.value;
      const params = new HttpParams().append('fqdn', value as string);
      this.loading = true;
      this.pingService
        .ping(params)
        .pipe(
          tap((res) => {
            this._alertService.show({
              type: 'info',
              message: this._translocoService.translate(
                'ping.connectionSuccess'
              ),
              autoDismissable: true,
              timeToDismissed: 4000,
            });
            this.loading = false;
          }),
          catchError((err) => {
            this._alertService.show({
              type: 'danger',
              message: this._translocoService.translate(
                'ping.connectionNotSuccess'
              ),
              autoDismissable: true,
              timeToDismissed: 4000,
            });
            this.loading = false;
            return throwError(() => err);
          })
        )
        .subscribe();
    }
  }
}
