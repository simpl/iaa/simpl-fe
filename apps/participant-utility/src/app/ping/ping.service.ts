import { Injectable, inject } from "@angular/core";
import { ApiService } from "@fe-simpl/api";
import { HttpParams } from '@angular/common/http';

@Injectable({ providedIn: "root" })
export class PingService {
  private api = inject(ApiService);

  ping(params: HttpParams) {
    return this.api.get<never>("/user-api/agent/ping", params);
  }
}
