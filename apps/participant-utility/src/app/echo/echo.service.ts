import { Injectable, inject } from "@angular/core";
import { ApiService } from "@fe-simpl/api";
import { Observable } from "rxjs";

export interface IdentityAttributeDTO {
  code: string,
  name: string,
  description: string,
  assignableToRoles: boolean,
  enabled: boolean,
  participantTypes: string[],
  used: boolean
}

export interface EchoTResponseDTO {
    id?: string,
    username?: string,
    userRole?: string[],
    commonName?: string,
    connectionStatus?: 'CONNECTED' | 'NOT_CONNECTED',
    mtlsStatus?: 'SECURED' | 'NOT_SECURED',
    email?: string,
    userEmail?: string,
    participantType?: string,
    organization?: string,
    status?: string,
    outcomeUserEmail?: string,
    certificateId?: string,
    identityAttributes?: IdentityAttributeDTO[]
    userIdentityAttributes?: string[],
}

@Injectable({ providedIn: "root" })
export class EchoService {
  private api = inject(ApiService);

    tier1() {
        return this.api.get<EchoTResponseDTO>("/user-api/mtls/echo-t1");
    }

    echo() {
        return this.api.get<EchoTResponseDTO>("/user-api/agent/echo");
    }

    hasCredential() {
        return this.api.get<boolean>("/user-api/credential");
    }

    loadCredential(file: File): Observable<void> {
        const body = new FormData();
        body.append("file", file);
        return this.api.uploadFile("/user-api/credential", body);
    }

    triggerDownloadIdentityAttributeInParticipantCluster(): Observable<void> {
      return this.api.get("/user-api/agent/identity-attributes");
    }
}
