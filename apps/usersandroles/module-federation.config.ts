import { ModuleFederationConfig } from '@nx/module-federation';

const coreLibraries = new Set([
  '@jsverse/transloco',
]);

const config: ModuleFederationConfig = {
  name: 'usersandroles',
  exposes: {
    './Routes': 'apps/usersandroles/src/app/remote-entry/entry.routes.ts',
  },
  shared: (libraryName, defaultConfig) => {
    if (coreLibraries.has(libraryName)) {
      return defaultConfig;
    }

    // Returning false means the library is not shared.
    return false;
  },
};

export default config;
