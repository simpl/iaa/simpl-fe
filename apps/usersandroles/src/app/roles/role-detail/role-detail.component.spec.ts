import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RoleDetailComponent } from './role-detail.component';
import { RolesService } from '../roles.service';
import { ActivatedRoute } from '@angular/router';
import { of, BehaviorSubject } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { AlertBannerService } from '@fe-simpl/alert-banner';
import { TranslocoService, TranslocoTestingModule } from '@jsverse/transloco';
import { CommonModule } from '@angular/common';
import { MatButton } from '@angular/material/button';
import { AppDomain, translocoTestConfiguration } from '@test-environment';

const matDialogMock = {
  open: jest.fn().mockReturnValue({
    afterClosed: jest.fn().mockReturnValue(of(true)),
  }),
};

describe('RoleDetailComponent', () => {
  let component: RoleDetailComponent;
  let fixture: ComponentFixture<RoleDetailComponent>;
  let rolesService: RolesService;
  let activatedRoute: ActivatedRoute;
  let matDialog: MatDialog;
  let alertBannerService: AlertBannerService;
  let translocoService: TranslocoService;

  const mockRoleDetail = { id: '123', name: 'Test Role', description: 'Test Description', assignedIdentityAttributes: [] };
  const mockAttributes = [ {
    code: 'string',
    name: 'string',
    description: 'string',
    assignableToRoles: true,
    enabled: true,
    participantTypes: ['test', 'test1'],
    used: true }
  ];

  let mockRoleDetailSubject: BehaviorSubject<any>;

  beforeEach(async () => {
    mockRoleDetailSubject = new BehaviorSubject(mockRoleDetail);

    await TestBed.configureTestingModule({
      imports: [CommonModule, MatButton, RoleDetailComponent,
        TranslocoTestingModule.forRoot(
          translocoTestConfiguration(AppDomain.USERS_AND_ROLES)
        ),],
      providers: [
        {
          provide: RolesService,
          useValue: {
            getRoleDetail: jest.fn().mockReturnValue(of(mockRoleDetail)),
            roleDetail$: mockRoleDetailSubject.asObservable(),
            assignIdentityAttributesToARole: jest.fn()
          }
        },
        { provide: MatDialog, useValue: matDialogMock },
        { provide: ActivatedRoute, useValue: { snapshot: { params: { id: '123' } } } },
        { provide: AlertBannerService, useValue: { show: jest.fn() } },
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(RoleDetailComponent);
    component = fixture.componentInstance;

    rolesService = TestBed.inject(RolesService);
    activatedRoute = TestBed.inject(ActivatedRoute);
    matDialog = TestBed.inject(MatDialog);
    alertBannerService = TestBed.inject(AlertBannerService);
    translocoService = TestBed.inject(TranslocoService);

    fixture.detectChanges();
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it('should load role details on init', () => {
    component.ngOnInit();
    expect(rolesService.getRoleDetail).toHaveBeenCalledWith('123');
    component.roleDetail = mockRoleDetail;
    expect(component.roleDetail).toEqual(mockRoleDetail);
  });

  it('should update role details when roleDetail$ emits', () => {
    component.ngOnInit();
    mockRoleDetailSubject.next(mockRoleDetail);
    expect(component.roleDetail).toEqual(mockRoleDetail);
  });

  it('should call onSaveAttributes and show success message when attributes are saved', () => {
    component.confirmDialog = { openDialog: jest.fn(), closeDialog: jest.fn() } as any;

    component.onSelectedAttributes(mockAttributes);
    component.onSaveAttributes();

    expect(component.confirmDialog.openDialog).toHaveBeenCalled();
  });
});
