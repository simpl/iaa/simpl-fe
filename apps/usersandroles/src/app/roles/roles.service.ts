import { inject, Injectable } from '@angular/core';
import { BehaviorSubject, Observable, tap } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { API_URL, ApiService } from '@fe-simpl/api';
import { SearchDataSource } from '@fe-simpl/search-page';

export interface IdentityAttributeDTO {
  code: string,
  name: string,
  description: string,
  assignableToRoles: boolean,
  enabled: boolean,
  participantTypes: string[],
  used: boolean
}

@Injectable({
  providedIn: 'root'
})
export class RolesService {

  private readonly _roleDetail: BehaviorSubject<any> = new BehaviorSubject(null);
  private readonly api_url = inject(API_URL);
  private readonly apiService = inject(ApiService);

  constructor(private readonly _httpClient: HttpClient){}

  getRoleList(params: HttpParams){
    return this.apiService.get<SearchDataSource>(
      `/user-api/role/search`, params
    );
  }

  getIdentityAttributeList(params: HttpParams){
    return this.apiService.get<SearchDataSource>(
      `/user-api/identity-attribute/search`, params
    );
  }

  get roleDetail$(): Observable<any>
  {
    return this._roleDetail.asObservable();
  }

  getRoleDetail(roleId: string): Observable<any> {
    return this._httpClient.get(this.api_url + `/user-api/role/${roleId}`).pipe(
      tap((res: any) => {
        this._roleDetail.next(res);
      })
    );
  }

  assignIdentityAttributesToARole(id: string, attributes: string[]): Observable<any> {
    return this._httpClient.put(this.api_url + `/user-api/role/${id}/identity-attributes`, attributes)
  }
}
