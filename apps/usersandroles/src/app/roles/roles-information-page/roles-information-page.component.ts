import { CommonModule } from '@angular/common';
import { Component, inject, OnInit, signal } from '@angular/core';
import { SearchDataSource } from '@fe-simpl/search-page';
import { TranslocoDirective } from '@jsverse/transloco';
import { ActivatedRoute, Router } from '@angular/router';
import { EuiButtonModule } from '@eui/components/eui-button';
import { EuiIconModule } from '@eui/components/eui-icon';
import { EuiLabelModule } from '@eui/components/eui-label';
import { EuiTableV2Module, Sort } from '@eui/components/eui-table-v2';
import { EuiPaginationEvent, EuiPaginatorModule } from '@eui/components/eui-paginator';
import { EuiProgressBarModule } from '@eui/components/eui-progress-bar';
import { EuiButtonGroupModule } from '@eui/components/eui-button-group';
import { EuiInputGroupModule } from '@eui/components/eui-input-group';
import { EuiInputTextModule } from '@eui/components/eui-input-text';
import { EuiPageModule } from '@eui/components/eui-page';
import { EuiResizableDirectiveModule } from '@eui/components/directives';
import { EuiChipListModule } from '@eui/components/eui-chip-list';
import { EuiChipModule } from '@eui/components/eui-chip';
import { RolesService } from '../roles.service';
import { HttpParams } from '@angular/common/http';
import { Role } from '../../users/users.service';
import { finalize } from 'rxjs';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'app-roles-information-page',
  standalone: true,
  imports: [
    CommonModule,
    TranslocoDirective,
    EuiPageModule,
    EuiButtonModule,
    EuiButtonGroupModule,
    EuiIconModule,
    EuiLabelModule,
    EuiTableV2Module,
    EuiPaginatorModule,
    EuiProgressBarModule,
    EuiInputGroupModule,
    EuiInputTextModule,
    EuiChipListModule,
    EuiChipModule,
    EuiResizableDirectiveModule,
    ReactiveFormsModule
  ],
  templateUrl: './roles-information-page.component.html',
  styleUrl: './roles-information-page.component.css'
})
export class RolesInformationPageComponent implements OnInit {
  private readonly service = inject(RolesService);
  private readonly router = inject(Router);
  private readonly route = inject(ActivatedRoute);
  public pagination: EuiPaginationEvent;
  data: Array<Role> = [];
  totalElements: number = 0;
  loading = signal(false);

  sortingCriteria: Array<string> = [];
  filtersForm: FormGroup;
  chips: Array<{field: string, value: string}> = [];

  constructor() {}

  ngOnInit(): void {
    this.filtersForm = new FormGroup({
      name: new FormControl(''),
      description: new FormControl(''),
    });

    this.pagination = {
      page: 0,
      pageSize: 5,
      nbPage: 5
    }
    this.getRoleList()
  }

  getRoleList(){
    this.loading.set(true);

    let searchFilters: HttpParams = new HttpParams()
    .set('page', this.pagination.page)
    .set('size', this.pagination.pageSize);

    if (this.filtersForm.value) {
      for (const [key, value] of Object.entries(
        this.filtersForm.getRawValue()
      )) {
        if (value) {
          searchFilters = searchFilters.append(key, value as string);
        }
      }
    }

    if(this.sortingCriteria){
      this.sortingCriteria.forEach(criteria => {
        searchFilters = searchFilters.append('sort', criteria)
      })
    }

    this.updateChips();

    this.service.getRoleList(searchFilters)
      .pipe(finalize(() => this.loading.set(false)))
      .subscribe({
        next: (response: SearchDataSource) => {
          this.totalElements = response.page.totalElements;
          this.data = response.content;
        },
        error: (err) => {
          console.error(err);
        },
      })
  }

  updateChips(){
    this.chips = Object.keys(this.filtersForm.controls)
      .filter(field => !!this.filtersForm.get(field)?.value)
      .map(field => ({ field, value: this.filtersForm.get(field)?.value }));
  }

  removeChip(event: any) {
    this.filtersForm.get(event.removed.id)?.reset();
    this.filtersForm.updateValueAndValidity();
    this.getRoleList();
  }

  onPageChange(e: EuiPaginationEvent): void {
    this.pagination = e;
    this.getRoleList();
  }

  onSortChange(newSortCriteria: Array<Sort>){
    this.sortingCriteria = newSortCriteria.map(sort => {
      return sort.sort + ',' + sort.order
    });

    this.getRoleList();
  }

  resetFilters(){
    this.filtersForm.reset();
    this.filtersForm.updateValueAndValidity();
    this.getRoleList();
  }

  getRoleDetailById(event: any) {
    this.router.navigate([`/role`, event.id], { relativeTo: this.route })
  }
}
