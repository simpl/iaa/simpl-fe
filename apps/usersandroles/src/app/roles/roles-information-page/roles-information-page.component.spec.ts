import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { RolesInformationPageComponent } from './roles-information-page.component';
import { RoleDto } from '@fe-simpl/api-types';
import { of } from 'rxjs';
import { TranslocoTestingModule } from '@jsverse/transloco';
import { AppDomain, translocoTestConfiguration } from '@test-environment';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { KeycloakService } from 'keycloak-angular';
import { API_URL, ApiService } from '@fe-simpl/api';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import enTranslations from "../../../assets/i18n/en.json";
import { RolesService } from '../roles.service';
import { ActivatedRoute } from '@angular/router';

describe('RolesInformationPageComponent', () => {
  let component: RolesInformationPageComponent;
  let fixture: ComponentFixture<RolesInformationPageComponent>;
  let httpTestingController: HttpTestingController;

  const mockList: Array<RoleDto> = [{
    id: "123",
    name: "TEST_NAME",
    description: "lorem ipsum description lorem ipsum description.",
  }];
  const mockApiUrl = 'http://mock-api-url';
  const mockRequestListService = {
    get:  jest.fn().mockReturnValue(of([])),
    search: jest.fn().mockReturnValue(of({
      content: mockList,
      page: {
        number: 0,
        totalPages: 1,
        totalElements: mockList.length,
        size: 5
      }
    }))
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RolesInformationPageComponent,
        HttpClientTestingModule,
        TranslocoTestingModule.forRoot(
          translocoTestConfiguration(AppDomain.USERS_AND_ROLES)
        ),
        NoopAnimationsModule
      ],
      providers: [
        KeycloakService,
        RolesService,
        { provide: ApiService, useValue: mockRequestListService },
        { provide: API_URL, useValue: mockApiUrl },
        { provide: ActivatedRoute, useValue: { snapshot: {} } },
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RolesInformationPageComponent);
    component = fixture.componentInstance;
    httpTestingController = TestBed.inject(HttpTestingController);
    fixture.detectChanges();
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
