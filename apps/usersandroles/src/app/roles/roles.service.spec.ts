import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { RolesService } from './roles.service';
import { API_URL } from '@fe-simpl/api';

const MOCK_API_URL = 'https://mock-api.com';

describe('RolesService', () => {
  let service: RolesService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        RolesService,
        { provide: API_URL, useValue: MOCK_API_URL },
      ],
    });

    service = TestBed.inject(RolesService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should fetch role details and update the BehaviorSubject', () => {
    const mockRoleDetail = { id: '1', name: 'Admin' };
    const roleId = '1';

    service.getRoleDetail(roleId).subscribe((res) => {
      expect(res).toEqual(mockRoleDetail);
    });

    const req = httpMock.expectOne(`${MOCK_API_URL}/user-api/role/${roleId}`);
    expect(req.request.method).toBe('GET');
    req.flush(mockRoleDetail);

    service.roleDetail$.subscribe((roleDetail) => {
      expect(roleDetail).toEqual(mockRoleDetail);
    });
  });

  it('should assign identity attributes to a role', () => {
    const mockResponse = { success: true };
    const roleId = '1';
    const attributes = ['attr1', 'attr2'];

    service.assignIdentityAttributesToARole(roleId, attributes).subscribe((res) => {
      expect(res).toEqual(mockResponse);
    });

    const req = httpMock.expectOne(`${MOCK_API_URL}/user-api/role/${roleId}/identity-attributes`);
    expect(req.request.method).toBe('PUT');
    expect(req.request.body).toEqual(attributes);
    req.flush(mockResponse);
  });

  it('should expose roleDetail$ as an observable', () => {
    const mockRoleDetail = { id: '1', name: 'Admin' };

    service.roleDetail$.subscribe((roleDetail) => {
      expect(roleDetail).toBeNull();
    });

    service.getRoleDetail('1').subscribe();
    const req = httpMock.expectOne(`${MOCK_API_URL}/user-api/role/1`);
    req.flush(mockRoleDetail);

    service.roleDetail$.subscribe((roleDetail) => {
      expect(roleDetail).toEqual(mockRoleDetail);
    });
  });
});
