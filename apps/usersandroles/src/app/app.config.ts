import {
  ApplicationConfig,
  importProvidersFrom, inject,
  isDevMode,
  provideAppInitializer
} from '@angular/core';
import { provideRouter, withRouterConfig } from '@angular/router';
import { appRoutes } from "./app.routes";
import { provideAnimationsAsync } from "@angular/platform-browser/animations/async";
import { provideTransloco } from '@jsverse/transloco';
import {
  provideHttpClient,
  withInterceptors,
  withInterceptorsFromDi,
} from "@angular/common/http";
import { environment } from "../environments/environment";
import { I18nService } from "@fe-simpl/i18n";
import { API_URL } from "@fe-simpl/api";
import { provideStore} from "@ngrx/store";
import { errorHandlingInterceptor } from "@fe-simpl/error-handler";
import {
  createInterceptorCondition,
  INCLUDE_BEARER_TOKEN_INTERCEPTOR_CONFIG,
  IncludeBearerTokenCondition, includeBearerTokenInterceptor,
  provideKeycloak
} from 'keycloak-angular';
import { appConfig as euiAppConfig} from '../config';
import {
  EUI_CONFIG_TOKEN, EuiServiceStatus,
  CoreModule as EuiCoreModule,
  provideEuiInitializer,
  translateConfig
} from '@eui/core';
import { TranslateModule } from '@ngx-translate/core';
import { provideAnimations } from '@angular/platform-browser/animations';
import { AppStarterService } from './app-starter.service';
import { Observable } from 'rxjs';

const init = (): Observable<EuiServiceStatus> => {
  const appStarter = inject(AppStarterService);
  return appStarter.start();
};

const urlCondition = createInterceptorCondition<IncludeBearerTokenCondition>({
  urlPattern: /^https?:\/\/[^/]+(\/.*)?$/i,
  bearerPrefix: 'Bearer'
});

export const appConfig: ApplicationConfig = {
  providers: [
    importProvidersFrom(
      EuiCoreModule.forRoot(),
      TranslateModule.forRoot(translateConfig)
    ),
    provideKeycloak({
      config: {
        clientId: environment.keycloakConfig_clientId,
        realm: environment.keycloakConfig_realm,
        url: environment.keycloakConfig_url,
      },
      initOptions: {
        onLoad: 'check-sso',
        silentCheckSsoRedirectUri: document.baseURI + 'assets/silent-check-sso.html'
      },
    }),
    {
      provide: INCLUDE_BEARER_TOKEN_INTERCEPTOR_CONFIG,
      useValue: [urlCondition]
    },
    provideStore(),
    provideRouter(appRoutes, withRouterConfig({
      onSameUrlNavigation: 'reload'
    })),
    provideAnimationsAsync(),
    provideHttpClient(
      withInterceptorsFromDi(),
      withInterceptors([errorHandlingInterceptor])
    ),
    provideTransloco({
      config: {
        availableLangs: ['en', 'es'],
        defaultLang: 'en',
        prodMode: !isDevMode(),
      },
      loader: I18nService,
    }),
    { provide: API_URL, useValue: environment.api_Url },
    {
      provide: EUI_CONFIG_TOKEN,
      useValue: { appConfig: euiAppConfig, environment }
    },
    provideEuiInitializer(),
    provideAppInitializer(init),
    provideHttpClient(withInterceptorsFromDi(),withInterceptors([includeBearerTokenInterceptor])),
    AppStarterService,
    provideAnimations()
  ],
};
