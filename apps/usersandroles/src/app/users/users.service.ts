import { HttpClient, HttpParams } from "@angular/common/http";
import { inject, Injectable } from "@angular/core";
import { API_URL, ApiService } from "@fe-simpl/api";
import { SearchDataSource } from "@fe-simpl/search-page";
import { map, Observable } from "rxjs";

export interface UserRole {
  name: string;
  description: string;
}

export interface User {
  username: string;
  firstName: string;
  lastName: string;
  email: string;
  password: string;
}

export interface Role {
  id: string,
  name: string,
  description: string,
  assignedIdentityAttributes: Array<string>
}

@Injectable({ providedIn: "root" })
export class UsersService {
  private readonly http = inject(HttpClient);
  private readonly api_url = inject(API_URL);
  private readonly apiService = inject(ApiService);

  getUserList(filters: HttpParams){
    return this.apiService.get<Array<User>>(
      `/user-api/user/search`, filters
    );
  }

  getUserRoles(id: string): Observable<Array<UserRole>> {
    const encId = encodeURI(id);
    return this.apiService.get<Array<UserRole>>(
      `/user-api/user/${encId}/roles`
    );
  }

  createUser(user: User): Observable<string>{
    return this.http.post(
      `${this.api_url}/user-api/user/as-t1user`,
      user,
      {responseType: 'text'}
    )
  }

  getRoleList(){
    return this.apiService.get<SearchDataSource>(
      `/user-api/role/search`
    ).pipe(
      map(res => res.content as Array<Role>)
    );
  }

  updateUserRoles(id: string, roles: Array<string>){
    const encId = encodeURI(id);
    return this.apiService.put(
      `/user-api/user/${encId}/roles`, roles
    )
  }
}
