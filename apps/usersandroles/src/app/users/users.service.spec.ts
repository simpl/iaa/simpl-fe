import { TestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { UsersService, User, UserRole, Role } from './users.service';
import { API_URL } from '@fe-simpl/api';
import { TranslocoTestingModule } from '@jsverse/transloco';
import { AppDomain, translocoTestConfiguration } from '@test-environment';

describe('UsersService', () => {
  let service: UsersService;
  let httpMock: HttpTestingController;
  const mockApiUrl = 'https://mock-api-url.com';

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        TranslocoTestingModule.forRoot(
          translocoTestConfiguration(AppDomain.USERS_AND_ROLES)
        ),
      ],
      providers: [UsersService, { provide: API_URL, useValue: mockApiUrl }],
    });

    service = TestBed.inject(UsersService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should call the endpoint and return user roles', () => {
    const mockRoles: Array<UserRole> = [
      { name: 'Admin', description: 'Administrator role' },
      { name: 'User', description: 'User role' },
    ];
    const userId = '123';

    service.getUserRoles(userId).subscribe((roles) => {
      expect(roles).toEqual(mockRoles);
    });

    const req = httpMock.expectOne(
      `${mockApiUrl}/user-api/user/${encodeURI(userId)}/roles`
    );
    expect(req.request.method).toBe('GET');
    req.flush(mockRoles);
  });

  it('should call the endpoint to create a user', () => {
    const mockUser: User = {
      username: 'username',
      firstName: 'user',
      lastName: 'surname',
      email: 'user@mail.com',
      password: 'password123',
    };
    const mockResponse = 'User created successfully';

    service.createUser(mockUser).subscribe((response) => {
      expect(response).toBe(mockResponse);
    });

    const req = httpMock.expectOne(`${mockApiUrl}/user-api/user/as-t1user`);
    expect(req.request.method).toBe('POST');
    expect(req.request.body).toEqual(mockUser);
    req.flush(mockResponse);
  });

  it('should call the endpoint and return the roles list', () => {
    const mockRoles: Array<Role> = [
      {
        id: '1',
        name: 'Admin',
        description: 'Administrator',
        assignedIdentityAttributes: [],
      },
      {
        id: '2',
        name: 'User',
        description: 'User',
        assignedIdentityAttributes: [],
      },
    ];
    const mockResponse = { content: mockRoles };

    service.getRoleList().subscribe((roles) => {
      expect(roles).toEqual(mockRoles);
    });

    const req = httpMock.expectOne(`${mockApiUrl}/user-api/role/search`);
    expect(req.request.method).toBe('GET');
    req.flush(mockResponse);
  });

  it('should call the endpoint to update user roles', () => {
    const userId = '123';
    const roles = ['Admin', 'User'];
    const mockResponse = { success: true };

    service.updateUserRoles(userId, roles).subscribe((response) => {
      expect(response).toEqual(mockResponse);
    });

    const req = httpMock.expectOne(
      `${mockApiUrl}/user-api/user/${encodeURI(userId)}/roles`
    );
    expect(req.request.method).toBe('PUT');
    req.flush(mockResponse);
  });
});
