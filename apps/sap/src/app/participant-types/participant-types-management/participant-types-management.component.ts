import { Component, Inject } from "@angular/core";
import { CommonModule } from "@angular/common";
import {
  SearchPageComponent,
  SearchPageInfo,
  TextFilter,
} from "@fe-simpl/search-page";
import { TranslocoDirective } from "@jsverse/transloco";
import {
  MAT_DIALOG_DATA,
  MatDialogModule,
  MatDialogRef,
} from "@angular/material/dialog";
import { MatButtonModule } from "@angular/material/button";
import { ParticipantTypeEnum } from "../../identity-attributes-management-page/identity-attributes-service";

@Component({
  selector: "app-participant-types-management",
  standalone: true,
  imports: [
    CommonModule,
    SearchPageComponent,
    TranslocoDirective,
    MatDialogModule,
    MatButtonModule,
  ],
  templateUrl: "./participant-types-management.component.html",
  styleUrl: "./participant-types-management.component.css",
})
export class ParticipantTypesManagementComponent {
  idAttrIds: string[] = [];
  pageInfo: SearchPageInfo;
  filter: { name: string; value: string };

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { participantType: ParticipantTypeEnum },
    public dialogRef: MatDialogRef<ParticipantTypesManagementComponent>
  ) {
    this.filter = { name: "participantTypeNotIn", value: data.participantType };
    this.initializeTableInfo();
  }

  initializeTableInfo() {
    this.pageInfo = {
      endpoint: `/sap-api/identity-attribute/search`,
      columns: {
        checkbox: {
          header: "checkbox",
          isCheckbox: true,
        },
        id: {
          header: "participantType.id",
        },
        code: {
          header: "participantType.code",
        },
        name: {
          header: "participantType.name",
        },
        assignableToRoles: {
          header: "participantType.assignableToRoles",
        },
        enabled: {
          header: "participantType.enabled",
        },
        outcomeUserEmail: {
          header: "participantType.outcomeUserEmail",
        },
        creationTimestamp: {
          header: "participantType.creationTimestamp",
          mapper: (v) => (v ? new Date(v) : ""),
        },
        updateTimestamp: {
          header: "participantType.updateTimestamp",
          mapper: (v) => (v ? new Date(v) : ""),
        },
      },
      filters: {
        name: new TextFilter("name", "filters.name"),
        code: new TextFilter("code", "filters.code"),
      },
    };
  }
}
