import { Component, inject, signal } from "@angular/core";
import { CommonModule } from "@angular/common";
import {
  BooleanDetail,
  DetailsEntity,
  DetailsPageComponent,
  TextDetail,
} from "@fe-simpl/details-page";
import { TranslocoDirective, TranslocoService } from "@jsverse/transloco";
import {
  DtoIdentityAttributeCreate,
  IdentityAttributesService,
  ParticipantTypeEnum,
} from "../identity-attributes-service";
import { LoaderComponent } from "@fe-simpl/loader";
import { finalize } from "rxjs";
import { AlertBannerService } from "@fe-simpl/alert-banner";
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: "app-identity-attributes-create",
  standalone: true,
  imports: [
    CommonModule,
    DetailsPageComponent,
    TranslocoDirective,
    LoaderComponent,
  ],
  templateUrl: "./identity-attributes-create.component.html",
  styleUrl: "./identity-attributes-create.component.css",
})
export class IdentityAttributesCreateComponent {
  identityAttributesService = inject(IdentityAttributesService);
  banner = inject(AlertBannerService);
  router = inject(Router);
  transloco = inject(TranslocoService);
  private readonly route =  inject(ActivatedRoute)

  loading = signal(false);

  editForm = signal<DetailsEntity>({
    name: new TextDetail("identityAttributesDetail.name", "", {
      editable: true,
      required: true,
    }),
    code: new TextDetail("identityAttributesDetail.code", "", {
      editable: true,
      required: true,
    }),
    assignable: new BooleanDetail(
      "identityAttributesDetail.assignable",
      false,
      { editable: true, required: true }
    ),
  });

  saveAction(data: any) {
    this.loading.set(true);
    const body: DtoIdentityAttributeCreate = {
      code: data.code,
      name: data.name,
      description: "",
      assignableToRoles: data.assignable,
      enabled: true,
      participantTypes: [
        ParticipantTypeEnum.APPLICATION_PROVIDER,
        ParticipantTypeEnum.CONSUMER,
        ParticipantTypeEnum.DATA_PROVIDER,
        ParticipantTypeEnum.INFRASTRUCTURE_PROVIDER,
      ], // TODO edit
    };
    this.identityAttributesService
      .create(body)
      .pipe(finalize(() => this.loading.set(false)))
      .subscribe({
        next: (ia) => {
          this.banner.show({
            type: "info",
            message: this.transloco.translate(
              "identityAttributesCreate.createSuccess"
            ),
          });
          this.router.navigate(['/identity-attributes', ia.id], { relativeTo: this.route });
        },
      });
  }
}
