import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { IdentityAttributesCreateComponent } from './identity-attributes-create.component';
import { IdentityAttributesService } from '../identity-attributes-service';
import { AlertBannerService } from '@fe-simpl/alert-banner';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslocoService, TranslocoTestingModule } from '@jsverse/transloco';
import { of } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { By } from '@angular/platform-browser';
import { LoaderComponent } from '@fe-simpl/loader';
import { DetailsPageComponent } from '@fe-simpl/details-page';
import { AppDomain, translocoTestConfiguration } from '@test-environment';

// Mock services
const mockIdentityAttributesService = {
  create: jest.fn()
};

const mockAlertBannerService = {
  show: jest.fn()
};

const mockRouter = {
  navigate: jest.fn()
};

const mockActivatedRoute = {
  snapshot: jest.fn(),
};

describe('IdentityAttributesCreateComponent', () => {
  let component: IdentityAttributesCreateComponent;
  let fixture: ComponentFixture<IdentityAttributesCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        IdentityAttributesCreateComponent,
        LoaderComponent,
        DetailsPageComponent,
        TranslocoTestingModule.forRoot(
          translocoTestConfiguration(AppDomain.SAP))
      ],
      providers: [
        { provide: IdentityAttributesService, useValue: mockIdentityAttributesService },
        { provide: AlertBannerService, useValue: mockAlertBannerService },
        { provide: Router, useValue: mockRouter },
        { provide: ActivatedRoute, useValue: mockActivatedRoute },
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IdentityAttributesCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show the loader when loading is true', () => {
    component.loading.set(true);
    fixture.detectChanges();

    const loaderElement = fixture.debugElement.query(By.css('lib-loader'));
    expect(loaderElement).toBeTruthy();
  });


  it('should show the form when loading is false', fakeAsync(() => {

    component.loading.set(false);
    fixture.detectChanges();
    tick();


    const formElement = fixture.debugElement.query(By.css('lib-details-page'));
    expect(formElement).toBeTruthy();


    const formComponentInstance = formElement.componentInstance;
    expect(formComponentInstance.editMode()).toBe(true);
    expect(formComponentInstance.hideCancelButton()).toBe(true);
  }));


  it('should call saveAction and show banner on success', fakeAsync(() => {
    const mockData = {
      code: 'test-code',
      name: 'test-name',
      assignable: true
    };

    const mockResponse = { id: '123' };
    mockIdentityAttributesService.create.mockReturnValue(of(mockResponse));
    component.saveAction(mockData);
    fixture.detectChanges();
    tick();

    expect(component.loading()).toBe(false);

    expect(mockIdentityAttributesService.create).toHaveBeenCalledWith({
      code: 'test-code',
      name: 'test-name',
      description: '',
      assignableToRoles: true,
      enabled: true,
      participantTypes: [
        'APPLICATION_PROVIDER',
        'CONSUMER',
        'DATA_PROVIDER',
        'INFRASTRUCTURE_PROVIDER'
      ]
    });

    expect(mockAlertBannerService.show).toHaveBeenCalledWith({
      type: 'info',
      message: 'You successfully created an attribute'
    });

    expect(mockRouter.navigate).toHaveBeenCalledWith(['/identity-attributes', '123'], { relativeTo: mockActivatedRoute });
  }));


  it('should handle finalizing after saveAction', () => {
    const mockData = {
      code: 'test-code',
      name: 'test-name',
      assignable: true
    };

    const mockResponse = { id: '123' };

    mockIdentityAttributesService.create.mockReturnValue(
      of(mockResponse).pipe(finalize(() => component.loading.set(false)))
    );

    component.saveAction(mockData);
    fixture.detectChanges();

    expect(mockIdentityAttributesService.create).toHaveBeenCalled();
    expect(component.loading()).toBe(false);
  });
});
