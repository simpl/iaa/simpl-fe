import { inject, Injectable } from "@angular/core";
import { ApiService } from "@fe-simpl/api";
import { Observable } from "rxjs";

export enum ParticipantTypeEnum {
  APPLICATION_PROVIDER = "APPLICATION_PROVIDER",
  DATA_PROVIDER = "DATA_PROVIDER",
  INFRASTRUCTURE_PROVIDER = "INFRASTRUCTURE_PROVIDER",
  CONSUMER = "CONSUMER",
}

export interface DtoIdentityAttributeCreate {
  code: string;
  name: string;
  description: string;
  assignableToRoles: boolean;
  enabled: boolean;
  participantTypes: ParticipantTypeEnum[];
}

export interface DtoIdentityAttributeDetails {
  id: string;
  code: string;
  name: string;
  description: string;
  assignableToRoles: boolean;
  enabled: boolean;
  creationTimestamp: string;
  updateTimestamp: string;
  participantTypes: ParticipantTypeEnum[];
}

@Injectable({ providedIn: "root" })
export class IdentityAttributesService {
  private apiService = inject(ApiService);

  detail(id: string): Observable<DtoIdentityAttributeDetails> {
    const encId = encodeURI(id);
    return this.apiService.get<DtoIdentityAttributeDetails>(
      `/sap-api/identity-attribute/${encId}`
    );
  }

  update(id: string, details: DtoIdentityAttributeDetails): Observable<void> {
    const encId = encodeURI(id);
    return this.apiService.put<void, DtoIdentityAttributeDetails>(
      `/sap-api/identity-attribute/${encId}`,
      details
    );
  }

  delete(id: string): Observable<void> {
    const encId = encodeURI(id);
    return this.apiService.delete<void>(
      `/sap-api/identity-attribute/${encId}`
    );
  }

  create(entity: DtoIdentityAttributeCreate) {
    return this.apiService.post<
      DtoIdentityAttributeDetails,
      DtoIdentityAttributeCreate
    >("/sap-api/identity-attribute", entity);
  }

  addToParticipantType(
    participantType: string,
    ids: string[]
  ): Observable<void> {
    const encPartType = encodeURI(participantType);
    return this.apiService.put<void, string[]>(
      `/sap-api/identity-attribute/add-participant-type/${encPartType}`,
      ids
    );
  }
}
