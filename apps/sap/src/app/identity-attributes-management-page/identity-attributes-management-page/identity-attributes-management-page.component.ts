import { Component, inject, signal } from "@angular/core";
import { CommonModule } from "@angular/common";
import { TranslocoDirective, TranslocoService } from "@jsverse/transloco";
import {
  ButtonActions,
  PilotType,
  SearchPageComponent,
  SearchPageInfo,
  TextFilter,
} from "@fe-simpl/search-page";
import { MatIconModule } from "@angular/material/icon";
import { MatButtonModule } from "@angular/material/button";
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { MatDialog } from "@angular/material/dialog";
import { ConfirmDialogComponent } from "@fe-simpl/shared-ui/confirm-dialog";
import { IdentityAttributesService } from "../identity-attributes-service";
import { LoaderComponent } from "@fe-simpl/loader";
import { finalize } from "rxjs";
import { AlertBannerService } from "@fe-simpl/alert-banner";

@Component({
  selector: "app-identity-attributes-management-page",
  standalone: true,
  imports: [
    CommonModule,
    TranslocoDirective,
    SearchPageComponent,
    MatIconModule,
    MatButtonModule,
    LoaderComponent,
    RouterModule,
  ],
  templateUrl: "./identity-attributes-management-page.component.html",
  styleUrl: "./identity-attributes-management-page.component.css",
})
export class IdentityAttributesManagementPageComponent {
  router = inject(Router);
  private readonly dialog = inject(MatDialog);
  private readonly translocoService = inject(TranslocoService);
  private readonly identityAttributeService = inject(IdentityAttributesService);
  private readonly banner = inject(AlertBannerService);
  private readonly route = inject(ActivatedRoute);
  loading = signal(false);

  pilot: PilotType = {};

  pageInfo: SearchPageInfo = {
    endpoint: "/sap-api/identity-attribute/search",
    columns: {
      id: {
        header: "identityAttributesManagementPage.identifier",
        sort: true,
      },
      code: {
        header: "identityAttributesManagementPage.code",
        sort: true,
      },
      assignableToRoles: {
        header: "identityAttributesManagementPage.assignableToRoles",
        sort: true,
      },
      enabled: {
        header: "identityAttributesManagementPage.enabled",
      },
      used: {
        header: "identityAttributesManagementPage.inUse",
      },
      creationTimestamp: {
        header: "identityAttributesManagementPage.creationTimestamp",
        mapper: (v) => (v ? new Date(v) : ""),
      },
      updateTimestamp: {
        header: "identityAttributesManagementPage.updateTimestamp",
        mapper: (v) => (v ? new Date(v) : ""),
        sort: true,
      },
      actions: {
        header: "simpleInput.actions",
        mapper: () =>
          new ButtonActions([
            {
              icon: "delete",
              theme: "secondary",
              action: (row) => this.deleteAction(row),
            },
          ]),
      },
    },
    filters: {
      name: new TextFilter("name", "filters.name"),
      code: new TextFilter("code", "filters.code"),
    },
  };

  rowAction(event: any) {
    this.router.navigate(['/identity-attributes', event.id], { relativeTo: this.route });

  }

  deleteAction(row: any) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: "simpleInput.confirmDeletion",
        message: this.translocoService.translate(
          "identityAttributesManagementPage.confirmDeletionMessage",
          { code: row.code }
        ),
        confirm: "simpleInput.delete",
        cancel: "simpleInput.cancel",
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.confirmDeleteAction(row);
      }
    });
  }

  confirmDeleteAction(row: any) {
    this.loading.set(true);
    this.identityAttributeService
      .delete(row.id)
      .pipe(finalize(() => this.loading.set(false)))
      .subscribe({
        next: () => {
          this.banner.show({
            type: "info",
            message: this.translocoService.translate(
              "identityAttributesManagementPage.deleteIdentityAttribueSuccess"
            ),
          });
          this.pilot?.["reload"]();
        },
      });
  }
}
