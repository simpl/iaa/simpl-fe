import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { IdentityAttributesManagementPageComponent } from './identity-attributes-management-page.component';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { TranslocoService, TranslocoTestingModule } from '@jsverse/transloco';
import { IdentityAttributesService } from '../identity-attributes-service';
import { AlertBannerService } from '@fe-simpl/alert-banner';
import { of } from 'rxjs';
import { By } from '@angular/platform-browser';
import { AppDomain, translocoTestConfiguration } from '@test-environment';
import { ApiService } from '@fe-simpl/api';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CommonModule } from '@angular/common';
import enTranslations from '../../../assets/i18n/en.json';
import { RouterTestingModule } from '@angular/router/testing';
import { IdentityAttributesCreateComponent } from '../identity-attributes-create/identity-attributes-create.component';

const mockApiService = {
  get: jest.fn().mockReturnValue(of([])),
  uploadFile: jest.fn().mockReturnValue(of({}))
};


describe('IdentityAttributesManagementPageComponent', () => {
  let component: IdentityAttributesManagementPageComponent;
  let fixture: ComponentFixture<IdentityAttributesManagementPageComponent>;

  let mockDialog: { open: jest.Mock };
  let mockIdentityAttributesService: { delete: jest.Mock };
  let mockBanner: { show: jest.Mock };
  let router: Router;
  let trs: TranslocoService;

  beforeEach(async () => {

    mockDialog = {
      open: jest.fn().mockReturnValue({ afterClosed: jest.fn(() => of(true)) })  // Mock di afterClosed
    };
    mockIdentityAttributesService = { delete: jest.fn(() => of({})) };
    mockBanner = { show: jest.fn() };

    await TestBed.configureTestingModule({
      imports: [
        IdentityAttributesCreateComponent,
        RouterTestingModule.withRoutes([
          { path: 'identity-attributes/_new', component: IdentityAttributesCreateComponent }
        ]),
        IdentityAttributesManagementPageComponent,
        TranslocoTestingModule.forRoot(
          translocoTestConfiguration(AppDomain.SAP)
        ),
        NoopAnimationsModule,
        HttpClientTestingModule,
        CommonModule
      ],
      providers: [
        { provide: MatDialog, useValue: mockDialog },
        { provide: IdentityAttributesService, useValue: mockIdentityAttributesService },
        { provide: AlertBannerService, useValue: mockBanner },
        { provide: ApiService, useValue: mockApiService }
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(IdentityAttributesManagementPageComponent);
    component = fixture.componentInstance;
    router = TestBed.inject(Router);
    trs = TestBed.inject(TranslocoService);
    fixture.detectChanges();
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it('should display the loader when loading is true', fakeAsync(() => {
    component.loading.set(true);
    fixture.detectChanges();
    tick();

    const loaderElement = fixture.debugElement.nativeElement.querySelector('#externalLoader');
    expect(loaderElement).toBeTruthy();
  }));

  it('should hide the loader and display content when loading is false',  fakeAsync(() => {
    component.loading.set(false);
    fixture.detectChanges();
    tick();

    const loaderElement = fixture.debugElement.nativeElement.querySelector('#externalLoader');
    const contentElement = fixture.debugElement.query(By.css('lib-search-page'));

    expect(loaderElement).toBeFalsy();
    expect(contentElement).toBeTruthy();
  }));

  it('should display the add button with correct text', fakeAsync(() => {
    component.loading.set(false);
    fixture.detectChanges();
    tick();

    const buttonElement = fixture.debugElement.query(By.css('button[mat-flat-button]'));
    expect(buttonElement.nativeElement.textContent).toContain(enTranslations.identityAttributesManagementPage.newAttribute);
  }));

  it.skip('should navigate to the new identity attribute page when the add button is clicked', fakeAsync(() => {
    component.loading.set(false);
    fixture.detectChanges();
    tick();

    const buttonElement = fixture.debugElement.nativeElement.querySelector("#buttonRouter");
    expect(buttonElement).toBeTruthy();

    buttonElement.click();

    tick();

    fixture.whenStable().then(() => {
      expect(router?.url).toBe('/sap/identity-attributes/_new');
    });
  }));

  it('should open the confirm dialog when deleteAction is called', () => {
    const row = { id: 1, code: 'test-code' };
    const expectedMessage = trs.translate('identityAttributesManagementPage.confirmDeletionMessage', { code: row.code });
    component.deleteAction(row);

    expect(mockDialog.open).toHaveBeenCalled();
    const dialogArgs = mockDialog.open.mock.calls[0][1].data;
    expect(dialogArgs.message).toContain(expectedMessage);
  });

  it('should call the delete service and show a banner after confirming deletion', () => {
    const row = { id: 1, code: 'test-code' };
    mockDialog.open.mockReturnValue({ afterClosed: () => of(true) });

    component.deleteAction(row);
    expect(mockIdentityAttributesService.delete).toHaveBeenCalledWith(1);
    expect(mockBanner.show).toHaveBeenCalled();
  });
});
