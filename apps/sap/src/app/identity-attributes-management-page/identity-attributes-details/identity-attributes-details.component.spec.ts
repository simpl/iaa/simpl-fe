import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IdentityAttributesDetailsComponent } from './identity-attributes-details.component';
import { ActivatedRoute, Router } from '@angular/router';
import { of } from 'rxjs';
import { IdentityAttributesService } from '../identity-attributes-service';
import { AlertBannerService } from '@fe-simpl/alert-banner';
import { TranslocoTestingModule } from '@jsverse/transloco';
import { DetailsPageComponent } from '@fe-simpl/details-page';
import { LoaderComponent } from '@fe-simpl/loader';
import { By } from '@angular/platform-browser';
import { AppDomain, translocoTestConfiguration } from '@test-environment';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CommonModule } from '@angular/common';

describe('IdentityAttributesDetailsComponent', () => {
  let component: IdentityAttributesDetailsComponent;
  let fixture: ComponentFixture<IdentityAttributesDetailsComponent>;
  let identityAttributesServiceMock: any;
  let routerMock: any;
  let alertBannerServiceMock: any;

  beforeEach(
    waitForAsync(() => {
      identityAttributesServiceMock = {
        detail: jest.fn(() => of({
          id: '1',
          code: '001',
          name: 'Test',
          assignableToRoles: true,
          enabled: true,
          description: 'Test description',
          creationTimestamp: '2023-01-01T00:00:00Z',
          updateTimestamp: '2023-01-02T00:00:00Z',
          participantTypes: []
        })),
        update: jest.fn(() => of({}))
      };

      routerMock = {
        navigateByUrl: jest.fn(() => Promise.resolve(true)),
        url: '/identity-attributes/1'
      };

      alertBannerServiceMock = {
        show: jest.fn()
      };

      TestBed.configureTestingModule({
        imports: [
          DetailsPageComponent,
          LoaderComponent,
          IdentityAttributesDetailsComponent,
          TranslocoTestingModule.forRoot(
            translocoTestConfiguration(AppDomain.SAP)
          ),
          NoopAnimationsModule,
          HttpClientTestingModule,
          CommonModule],
        providers: [
          { provide: ActivatedRoute, useValue: { snapshot: { paramMap: { get: () => '1' } } } },
          { provide: Router, useValue: routerMock },
          { provide: IdentityAttributesService, useValue: identityAttributesServiceMock },
          { provide: AlertBannerService, useValue: alertBannerServiceMock },
        ]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(IdentityAttributesDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should load details on init', () => {
    expect(identityAttributesServiceMock.detail).toHaveBeenCalledWith('1');
  });

  it('should render the details page when not loading', () => {
    component.loading.set(false);
    fixture.detectChanges();

    const detailsPage = fixture.debugElement.query(By.css('lib-details-page'));
    expect(detailsPage).toBeTruthy();

    const expectedEntity = component.entity();
    const actualEntity = detailsPage.componentInstance.entity();

    expect(actualEntity).toEqual(expectedEntity);
  });

  it('should show loader when loading is true', () => {
    component.loading.set(true);
    fixture.detectChanges();

    const loader = fixture.debugElement.query(By.css('lib-loader'));
    expect(loader).toBeTruthy();
  });

  it('should call saveAction on form save', () => {
    const saveSpy = jest.spyOn(component, 'saveAction');
    component.loading.set(false);
    fixture.detectChanges();

    const detailsPage = fixture.debugElement.query(By.css('lib-details-page'));
    detailsPage.triggerEventHandler('save', { name: 'Test' });

    expect(saveSpy).toHaveBeenCalledWith({ name: 'Test' });
  });

  it('should update assignable status when extra button is clicked', () => {
    const initialDetails = {
      id: '1',
      code: '001',
      name: 'Test',
      description: 'Test description',
      assignableToRoles: true,
      enabled: true,
      creationTimestamp: '2023-01-01T00:00:00Z',
      updateTimestamp: '2023-01-02T00:00:00Z',
      participantTypes: []
    };

    component.detailsResponse.set(initialDetails);
    fixture.detectChanges();

    const button = component.extraButtons()[0];
    expect(button.label).toContain('makeUnAssignable');

    button.action();

    expect(identityAttributesServiceMock.update).toHaveBeenCalledWith('1', {
      ...initialDetails,
      assignableToRoles: false,
    });
  });

  it('should display a success alert on save success', () => {
    component.detailsResponse.set({
      id: '1',
      code: '001',
      name: 'Test',
      description: 'Test description',
      assignableToRoles: true,
      enabled: true,
      creationTimestamp: '2023-01-01T00:00:00Z',
      updateTimestamp: '2023-01-02T00:00:00Z',
      participantTypes: []
    });
    fixture.detectChanges();

    component.saveAction({ name: 'Updated Name' });
    expect(alertBannerServiceMock.show).toHaveBeenCalledWith({
      type: 'info',
      message: expect.any(String)
    });
  });

  it('should reload the page after successful update', async () => {
    const reloadSpy = jest.spyOn(component, 'reloadPage');
    component.detailsResponse.set({
      id: '1',
      code: '001',
      name: 'Test',
      description: 'Test description',
      assignableToRoles: true,
      enabled: true,
      creationTimestamp: '2023-01-01T00:00:00Z',
      updateTimestamp: '2023-01-02T00:00:00Z',
      participantTypes: []
    });
    fixture.detectChanges();

    component.saveAction({ name: 'Updated Name' });

    fixture.detectChanges();

    expect(reloadSpy).toHaveBeenCalled();

    expect(routerMock.navigateByUrl).toHaveBeenCalledWith('/', { skipLocationChange: true });
  });

});
