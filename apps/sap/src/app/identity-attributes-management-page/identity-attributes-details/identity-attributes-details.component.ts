import { Component, computed, inject, OnInit, signal } from "@angular/core";
import { CommonModule } from "@angular/common";
import { TranslocoService } from "@jsverse/transloco";
import {
  BooleanDetail,
  Button,
  DateDetail,
  DetailsEntity,
  DetailsPageComponent,
  TextDetail,
} from "@fe-simpl/details-page";
import { ActivatedRoute, Router } from "@angular/router";
import {
  DtoIdentityAttributeDetails,
  IdentityAttributesService,
} from "../identity-attributes-service";
import { LoaderComponent } from "@fe-simpl/loader";
import { finalize } from "rxjs";
import { AlertBannerService } from "@fe-simpl/alert-banner";

@Component({
  selector: "app-identity-attributes-details",
  standalone: true,
  imports: [
    CommonModule,
    DetailsPageComponent,
    LoaderComponent,
  ],
  templateUrl: "./identity-attributes-details.component.html",
  styleUrl: "./identity-attributes-details.component.css",
})
export class IdentityAttributesDetailsComponent implements OnInit {
  private readonly activatedRoute: ActivatedRoute = inject(ActivatedRoute);
  private readonly router = inject(Router);
  private readonly alertBannerService = inject(AlertBannerService);
  private readonly transloco = inject(TranslocoService);

  loading = signal(false);

  id = signal<string>(this.activatedRoute.snapshot.paramMap.get("id")!);

  identityAttributesService = inject(IdentityAttributesService);

  detailsResponse = signal<DtoIdentityAttributeDetails | null>(null);

  extraButtons = computed<Button[]>(() => {
    const detail = this.detailsResponse();
    if (detail) {
      return [
        {
          icon: "check_circle",
          label: `identityAttributesDetail.${
            detail.assignableToRoles ? "makeUnAssignable" : "makeAssignable"
          }`,
          type: "basic",
          action: () => this.changeAssignableStatus(!detail.assignableToRoles),
        },
      ];
    } else {
      return [];
    }
  });

  entity = computed<DetailsEntity | null>(() => {
    const response = this.detailsResponse();
    if (response) {
      return {
        code: new TextDetail("identityAttributesDetail.code", response.code),
        name: new TextDetail("identityAttributesDetail.name", response.name),
        inUse: new BooleanDetail(
          "identityAttributesDetail.assignable",
          response.assignableToRoles
        ),
        date: new DateDetail(
          "identityAttributesDetail.date",
          response.creationTimestamp
        ),
        lastDate: new DateDetail(
          "identityAttributesDetail.lastDate",
          response.updateTimestamp
        ),
      };
    } else {
      return null;
    }
  });

  editForm = computed<DetailsEntity | null>(() => {
    const response = this.detailsResponse();
    if (response) {
      return {
        name: new TextDetail("identityAttributesDetail.name", response.name, {
          editable: true,
          required: true,
        }),
        code: new TextDetail("identityAttributesDetail.code", response.code, {
          editable: true,
          required: true,
        }),
        assignableToRoles: new BooleanDetail(
          "identityAttributesDetail.assignable",
          response.assignableToRoles,
          { editable: true }
        ),
        enabled: new BooleanDetail(
          "identityAttributesDetail.enabled",
          response.enabled,
          { editable: true }
        ),
      };
    } else {
      return null;
    }
  });

  ngOnInit(): void {
    this.loadDetails();
  }

  loadDetails(): void {
    this.loading.set(true);
    this.identityAttributesService
      .detail(this.id())
      .pipe(finalize(() => this.loading.set(false)))
      .subscribe({
        next: this.detailsResponse.set,
      });
  }

  saveAction(el: any) {
    const details = this.detailsResponse();
    if (details) {
      this.loading.set(true);
      const body = { ...details, ...el };
      this.identityAttributesService
        .update(details.id, body)
        .pipe(finalize(() => this.loading.set(false)))
        .subscribe({
          next: () => {
            this.alertBannerService.show({
              type: "info",
              message: this.transloco.translate(
                "identityAttributesDetail.editSuccess"
              ),
            });
            this.reloadPage();
          },
        });
    }
  }

  changeAssignableStatus(status: boolean) {
    this.saveAction({ assignableToRoles: status });
  }

  reloadPage() {
    const url = this.router.url;
    this.router.navigateByUrl("/", { skipLocationChange: true }).then(() => {
      this.router.navigateByUrl(`/${url}`).then(() => {});
    });
  }
}
