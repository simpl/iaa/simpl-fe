import { Component, OnInit } from '@angular/core';
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { AlertBannerComponent } from "@fe-simpl/alert-banner";
import { MatButton, MatIconButton } from '@angular/material/button';
import { MatProgressSpinner } from '@angular/material/progress-spinner';
import { KeycloakService } from 'keycloak-angular';
import { MatIcon } from '@angular/material/icon';
import { MatToolbar } from '@angular/material/toolbar';
import { MatMenu, MatMenuItem, MatMenuTrigger } from '@angular/material/menu';

@Component({
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    AlertBannerComponent,
    MatButton,
    MatProgressSpinner,
    MatIcon,
    MatToolbar,
    MatMenu,
    MatMenuItem,
    MatMenuTrigger,
    MatIconButton,
  ],
  selector: 'app-sap-entry',
  template: `
    <div>
      <mat-toolbar class="h-100">
        <div
          class="d-flex flex-row justify-content-between align-items-center flex-grow-1"
        >
          <div class="font-bold">
            <div>
              <img src="assets/images/logo/simpl-logo.svg" alt="simpl-logo" />
            </div>
          </div>
          <div class="d-flex flex-row gap-2 align-items-center">
            <div>
              <button
                mat-icon-button
                [matMenuTriggerFor]="menu"
                *ngIf="isLoggedIn"
              >
                <mat-icon>account_circle</mat-icon>
              </button>
              <mat-menu #menu="matMenu">
                <button mat-menu-item (click)="logOut()">
                  <mat-icon>logout</mat-icon>
                  <span>Logout</span>
                </button>
              </mat-menu>
            </div>
          </div>
        </div>
      </mat-toolbar>
      <lib-alert-banner class="position-fixed w-100 p-4"></lib-alert-banner>
      <router-outlet></router-outlet>
      <div class="ribbon" *ngIf="role">{{ role }}</div>
    </div>
  `,
  styles: [
    `
      .ribbon {
        font-size: 28px;
        font-weight: bold;
        color: #fff;

        --r: 0.4em;
        --c: #002B6E;
        --a: 0.001em;
        position: absolute;
        bottom: 20px;
        right: calc(-1 * var(--a));
        line-height: 1.8;
        padding: calc(2 * var(--r)) 0.5em 0;
        border-radius: 0 var(--r) var(--r) 0;
        background: radial-gradient(100% 50% at right, var(--c) 98%, #0000 101%)
            0 100%/0.5lh calc(100% - 2 * var(--r)),
          radial-gradient(100% 50% at left, #0005 98%, #0000 101%) 100% 0 /
            var(--r) calc(2 * var(--r)),
          conic-gradient(
              at calc(100% - var(--r)) calc(2 * var(--r)),
              var(--c) 75%,
              #0000 0
            )
            100% 0 / calc(101% - 0.5lh) 100%;
        background-repeat: no-repeat;
      }
    `,
  ],
})
export class RemoteEntryComponent implements OnInit {
  loading = false;
  isLoggedIn = false;
  roles: string[] = [];
  role = '';
  constructor(private _keycloakService: KeycloakService) {}

  ngOnInit() {
    this.isLoggedIn = this._keycloakService.isLoggedIn();
    this.roles = this._keycloakService.getUserRoles();
    this.ribbonText();
  }

  logOut() {
    this.loading = true;

    const baseUrl = window.location.origin;
    const onboardingPath = window.location.pathname.includes('/sap')
      ? '/sap/participant-types'
      : '/participant-types';

    this._keycloakService.logout(`${baseUrl}${onboardingPath}`).then(() => {
      this.loading = false;
    });
  }

  ribbonText() {
    if (this.roles.includes('APPLICANT')) {
      this.role = 'APPLICANT';
    } else if (this.roles.includes('NOTARY')) {
      this.role = 'NOTARY';
    } else if (this.roles.includes('T1UAR_M')) {
      this.role = 'T1UAR_M';
    } else if (this.roles.includes('IATTR_M')) {
      this.role = 'IATTR_M';
    } else if (this.roles.includes('T2IAA_M')) {
      this.role = 'T2IAA_M';
    } else {
      this.role = 'NO ROLE';
    }
  }
}
