import { Route } from "@angular/router";
import { authGuard } from "@fe-simpl/auth";

export const remoteRoutes: Route[] = [
  {
    path: "",
    redirectTo: "participant-types",
    pathMatch: "full",
  },
  {
    path: "participant-types",
    loadComponent: () =>
      import("../participant-types/participant-types.component").then(
        (m) => m.ParticipantTypesComponent
      ),
    canActivate: [authGuard],
    data: { roles: ["IATTR_M"] },
  },
  {
    path: "identity-attributes",
    loadComponent: () =>
      import("../identity-attributes-management-page/identity-attributes-management-page/identity-attributes-management-page.component").then(
        (m) => m.IdentityAttributesManagementPageComponent
      ),
    canActivate: [authGuard],
    data: { roles: ["IATTR_M"] },
  },
  {
    path: "identity-attributes/_new",
    loadComponent: () =>
      import("../identity-attributes-management-page/identity-attributes-create/identity-attributes-create.component").then(
        (m) => m.IdentityAttributesCreateComponent
      ),
    canActivate: [authGuard],
    data: { roles: ["IATTR_M"] },
  },
  {
    path: "identity-attributes/:id",
    loadComponent: () =>
      import("../identity-attributes-management-page/identity-attributes-details/identity-attributes-details.component").then(
        (m) => m.IdentityAttributesDetailsComponent
      ),
    canActivate: [authGuard],
    data: { roles: ["IATTR_M"] },
  },
  {
    path: "unauthorized",
    loadComponent: () =>
      import("@fe-simpl/landing-page").then((m) => m.UnauthorizedPageComponent)
  },
  {
    path: "error",
    loadComponent: () =>
      import("@fe-simpl/landing-page").then((m) => m.ErrorPageComponent)
  }
];
