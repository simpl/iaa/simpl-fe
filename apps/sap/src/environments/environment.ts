/* tslint:disable */
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
export const environment = {
  production: false,
// @ts-ignore
  api_Url: window["env"]["api_Url"] || "default",
// @ts-ignore
  keycloakConfig_url: window["env"]["keycloakConfig_url"],
// @ts-ignore
  keycloakConfig_realm: window["env"]["keycloakConfig_realm"],
// @ts-ignore
  keycloakConfig_clientId: window["env"]["keycloakConfig_clientId"],
};
