# Environment and Ingress Configuration

This document outlines the configuration required for setting up the application environment, CORS policy, and ingress within the Kubernetes cluster. The configuration includes host URLs for backend and frontend services, CORS allowed origins, and environment variables essential for the application's integration with Keycloak and other services.

## Overview

The configuration provides details on the backend and frontend host URLs, CORS allowed origins, ingress issuer, and environment variables for the onboarding application. These settings are crucial for the proper operation and security of the application within a cloud environment.

## Configuration Details

### Host Configuration

- **Backend Host**
  - `hostBe`: The hostname for the backend service.
    - Value: `my.backend.host`
  
- **Frontend Host**
  - `hostFe`: The hostname for the frontend service.
    - Value: `my.frontend.host`

### CORS Configuration

- **Allowed Origins**
  - `cors.allowOrigin`: Specifies the origins that are allowed to access the application resources via cross-origin requests.
    - Value: `https://my.frontend.host, https://participant.fe.aruba-simpl.cloud, http://localhost:4202, http://localhost:4203, http://localhost:3000`

### Ingress Configuration

- **Issuer**
  - `ingress.issuer`: The issuer for the ingress, which is typically used for managing TLS certificates.
    - Value: `issuer-ingress`

### Environment Variables

The following environment variables are defined for the application:

- **NAMESPACE_NAME**
  - `NAMESPACE_NAME`: The name visible on application.
    - Value: `SIMPL`
- **APPLICATION**
  - `APPLICATION`: The name of the application.
    - Value: `onboarding`
  
- **API_URL**
  - `API_URL`: The URL for the API backend.
    - Value: `https://my.backend.host`
  
- **Keycloak Configuration**
  - `KEYCLOAK_URL`: The URL for the Keycloak authentication service.
    - Value: `https://my.backend.host/auth`
  - `KEYCLOAK_REALM`: The Keycloak realm that the application uses for authentication.
    - Value: `authority`
  - `KEYCLOAK_CLIENT_ID`: The client ID used by the application to authenticate with Keycloak.
    - Value: `frontend-cli`

## Usage

These configuration values should be defined in your Kubernetes manifests or Helm chart to ensure that the application is correctly set up with the appropriate environment, CORS policy, and ingress settings.

### Example `values.yaml`

```yaml
hostBe: my.backend.host
hostFe: my.frontend.host

cors:
  allowOrigin: "https://my.frontend.host,https://participant.fe.aruba-simpl.cloud,http://localhost:4202,http://localhost:4203,http://localhost:3000"

ingress:
  issuer: issuer-ingress

env:
  - name: APPLICATION
    value: onboarding
  - name: API_URL
    value: "https://my.backend.host"
  - name: KEYCLOAK_URL
    value: "https://my.backend.host/auth"
  - name: KEYCLOAK_REALM
    value: "authority"
  - name: KEYCLOAK_CLIENT_ID
    value: "frontend-cli"
