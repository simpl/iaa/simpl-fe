FROM nginx:latest

COPY dist/apps /usr/share/nginx/html
COPY _docker/nginx.conf.template /nginx.conf.template
COPY _docker/docker-cmd.sh /nginx-cmd.sh

CMD ["/nginx-cmd.sh"]
