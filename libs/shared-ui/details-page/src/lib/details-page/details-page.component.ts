import {
  Component,
  computed,
  input,
  OnInit,
  output,
  signal,
} from "@angular/core";
import { CommonModule, KeyValue } from "@angular/common";
import { TranslocoDirective } from "@jsverse/transloco";
import { MatIconModule } from "@angular/material/icon";
import { MatDividerModule } from "@angular/material/divider";
import { MatButtonModule } from "@angular/material/button";
import {
  FormControl,
  FormGroup,
  ReactiveFormsModule,
  Validators,
} from "@angular/forms";

export interface DetailsEntity {
  [key: string]: Detail;
}

export interface DetailConfig {
  editable?: boolean;
  required?: boolean;
}

export abstract class Detail<T = any> {
  readonly label: string;
  readonly value: T;
  readonly editable: boolean;
  readonly config: DetailConfig;

  constructor(label: string, value: T, config?: DetailConfig) {
    this.label = label;
    this.value = value;
    this.editable = config?.editable ?? false;
    this.config = config ?? {};
  }

  toString(): string {
    return this.value?.toString() ?? "";
  }
}

export class TextDetail extends Detail<string> {}

export class BooleanDetail extends Detail<boolean> {}

export class DateDetail extends Detail<Date> {
  constructor(label: string, value: Date | string, config?: DetailConfig) {
    super(label, value instanceof Date ? value : new Date(value), config);
  }

  override toString(): string {
    return this.value.toLocaleDateString();
  }
}

export interface Button {
  icon: string;
  label: string;
  type: string;
  action: () => void;
}

@Component({
  selector: "lib-details-page",
  standalone: true,
  imports: [
    CommonModule,
    TranslocoDirective,
    MatButtonModule,
    MatDividerModule,
    MatIconModule,
    ReactiveFormsModule,
  ],
  templateUrl: "./details-page.component.html",
  styleUrl: "./details-page.component.css",
})
export class DetailsPageComponent implements OnInit {
  title = input("title");
  columnClass = input("col-lg-3 col-md-6 col-12");
  entity = input<DetailsEntity | null>(null);
  editForm = input<DetailsEntity | null>(null);
  showEditButton = input<boolean>(false);
  editMode = input(false);
  hideCancelButton = input(false);

  editButtonEditLabel = input("simpleInput.edit");
  save = output<KeyValue<string, any>>();
  cancel = output();

  edit = signal<boolean>(this.editMode());
  wasValidated = signal(false);

  extraButtons = input<Button[]>([]);
  customClass = input("");
  fullPage = input(true);
  detailClass = computed(() => {
    let classes = "d-flex row";
    if (this.customClass()) classes += " " + this.customClass();
    if (this.fullPage()) classes += " min-vh-100 justify-content-center";
    return classes;
  });

  ngOnInit(): void {
    if (this.editMode()) {
      this.edit.set(true);
    }
  }

  formGroup = computed<FormGroup>(() => {
    if (this.edit()) {
      const formGroup = new FormGroup({});
      Object.entries(this.editForm() ?? {}).forEach(([key, control]) => {
        formGroup.addControl(
          key,
          new FormControl(control.value, {
            validators: control.config?.required
              ? [
                  Validators.required,
                  Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/),
                ]
              : [],
          })
        );
      });
      return formGroup;
    }
    return new FormGroup({});
  });

  isTextDetail(
    detail: KeyValue<string, Detail<any>>
  ): KeyValue<string, TextDetail> | null {
    return detail.value instanceof TextDetail ? detail : null;
  }

  isbooleanDetail(
    detail: KeyValue<string, Detail<any>>
  ): KeyValue<string, BooleanDetail> | null {
    return detail.value instanceof BooleanDetail ? detail : null;
  }

  editAction() {
    this.edit.set(true);
  }

  saveAction() {
    this.wasValidated.set(true);
    if (this.formGroup().valid) {
      this.save.emit(this.formGroup().getRawValue());
    }
  }
  cancelAction() {
    this.edit.set(false);
  }

  noSort = (): number => {
    return 0;
  };
}
