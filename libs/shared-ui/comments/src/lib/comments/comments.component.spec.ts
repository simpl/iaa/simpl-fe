import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CommentsComponent } from './comments.component';
import { KeycloakService } from 'keycloak-angular';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { TranslocoTestingModule } from '@jsverse/transloco';
import { translocoTestConfiguration } from '@test-environment';

describe('CommentsComponent', () => {
  let component: CommentsComponent;
  let fixture: ComponentFixture<CommentsComponent>;
  const translations = {};
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        CommentsComponent,
        NoopAnimationsModule,
        TranslocoTestingModule.forRoot(
          translocoTestConfiguration(undefined, translations)
        ),
      ],
      providers: [
        KeycloakService
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(CommentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
