import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { MatFormField, MatHint, MatLabel } from '@angular/material/form-field';
import { MatInput } from '@angular/material/input';
import { TranslocoDirective } from '@jsverse/transloco';
import { MatButton } from '@angular/material/button';
import { FormControl, ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'lib-comments',
  standalone: true,
  imports: [CommonModule, MatFormField, MatInput, TranslocoDirective, MatButton, ReactiveFormsModule, MatLabel, MatHint, DatePipe],
  templateUrl: './comments.component.html',
  styleUrl: './comments.component.css',
})
export class CommentsComponent {

  @Input() labelComment: string;
  @Input() buttonComment: string;
  @Input() emptyComment: string;
  @Input() commentAuthor: string;
  @Input() set comments(value: any[]) {
    this._comments = value.map(comment => ({
      ...comment,
      creationTimestamp: comment.creationTimestamp ? new Date(comment.creationTimestamp) : this.today
    }));
  }
  get comments() {
    return this._comments;
  }
  private _comments: any[] = [];
  @Output() postComment = new EventEmitter();
  comment: FormControl = new FormControl('');
  today = new Date();

  onPostComment() {
    this.postComment.emit(this.comment.getRawValue());
    this.comment.reset('')
  }
}
