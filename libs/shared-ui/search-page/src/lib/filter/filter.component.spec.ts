import { TestBed } from '@angular/core/testing';
import { TranslocoTestingModule } from '@jsverse/transloco';
import { AppDomain, translocoTestConfiguration } from '@test-environment';
import { ComponentInput, OutputRefKeysWithCallback, render } from '@testing-library/angular';
import { findByLabelText, findByTestId, fireEvent, screen } from '@testing-library/dom';
import userEvent, { UserEvent } from '@testing-library/user-event';
import { DateFilter, DateRangeFilter, FilterComponent, OptionFilter, TextFilter } from './filter.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

async function findFilterComponent() {
  return await screen.findByTestId("filter-component");
}

async function clickOnFiltersToggle(user: UserEvent) {
  const component = await findFilterComponent();
  const button = await findByTestId(component, "filter-menu-toggle");
  return user.click(button);
}

async function selectFilterElement(user: UserEvent, filterPosition: number) {
  const select = screen.getByRole("combobox");
  await user.click(select);

  const filters = await screen.findAllByRole("option");
  await user.click(filters[filterPosition]);

}
async function getInputField() {
  return await screen.findByTestId("input-field");
}

async function getInputDateField() {
  const dateBox = await screen.findByTestId("date-field");
  return await findByLabelText(dateBox, /date/i);
}

async function updateInputDateField(value: Date) {
  const input = await getInputDateField();
  fireEvent.input(input, {target: { value: value.toLocaleDateString("en") }});
  console.log((input as HTMLInputElement).value);
}

async function updateInputField(value: string) {
  const input = await getInputField();
  fireEvent.input(input, {target: { value }});
}

async function submitFitlers() {
  const button = screen.getByTestId("submit-button");
  fireEvent.click(button);
}
async function resetFilters() {
  const button = screen.getByTestId("reset-button");
  fireEvent.click(button);
}

interface RenderConfig {
  inputs: ComponentInput<FilterComponent>,
  on?: OutputRefKeysWithCallback<FilterComponent>,
}
function renderComponent({inputs, on}: RenderConfig) {
  TestBed.configureTestingModule({
      providers: [],
      imports: [
        FilterComponent,
        TranslocoTestingModule.forRoot(translocoTestConfiguration(AppDomain.PARTICIPANT_UTILITY)),
        NoopAnimationsModule
      ],
  });
    return render(FilterComponent, {
      inputs,
      on,
    });
}

describe('FilterComponent', () => {

  it('should create', async () => {
    const component = await renderComponent({
      inputs: {
        filters: { }
      }
    });
    expect(component).toBeTruthy();
  });

  it('should submit form', async () => {
    const user = userEvent.setup();
    const submitHandler = jest.fn();
    await renderComponent({
      on: {
        submit: submitHandler,
      },
      inputs: {
        filters: {
          name: new TextFilter("name", "name"),
        }
      }
    });

    await clickOnFiltersToggle(user);
    await selectFilterElement(user, 0);
    await updateInputField("Testing Name");
    await submitFitlers();

    expect(submitHandler).toHaveBeenCalled();
    expect(submitHandler).toHaveBeenLastCalledWith({
      name: "Testing Name",
    });

  });

  it('should reset form', async () => {
    const user = userEvent.setup();
    const submitHandler = jest.fn();
    await renderComponent({
      on: {
        submit: submitHandler,
      },
      inputs: {
        filters: {
          name: new TextFilter("name", "name"),
        }
      }
    });

    await clickOnFiltersToggle(user);
    await selectFilterElement(user, 0);
    await updateInputField("Testing Name");
    await submitFitlers();

    expect(submitHandler).toHaveBeenCalled();
    expect(submitHandler).toHaveBeenLastCalledWith({
      name: "Testing Name",
    });

    await clickOnFiltersToggle(user);
    await resetFilters();
    await clickOnFiltersToggle(user);
    await submitFitlers();

    expect(submitHandler).toHaveBeenCalled();
    expect(submitHandler).toHaveBeenLastCalledWith({
      name: null,
    });
  });

  it('should submit form with different data types', async () => {
    const user = userEvent.setup();
    const submitHandler = jest.fn();
    const component = await renderComponent({
      on: {
        submit: submitHandler,
      },
      inputs: {
        filters: {
          text: new TextFilter("text", "text"),
          date: new DateFilter("date", "label.date"),
          dateRange: new DateRangeFilter("date", "label.dateRange", "dateFrom", "dateTo"),
          options: new OptionFilter("options", "label.options", [
            { label: "label.option1", value: "option.value1" },
            { label: "label.option2", value: "option.value2" },
          ]),
        }
      }
    });

    await clickOnFiltersToggle(user);

    await selectFilterElement(user, 0);
    screen.getByTestId("input-field");

    await selectFilterElement(user, 1);
    screen.getByTestId("date-field");

    await selectFilterElement(user, 2);
    screen.getByTestId("date-range-field");

    await selectFilterElement(user, 3);
    screen.getByTestId("options-field");

    expect(component).toBeTruthy();
  }, 10000);

});


describe('DateRangeFilter', () => {

  it('testing get value and reset', () => {
    const dateFrom = new Date("2024-09-26T18:45:08+02:00");
    const dateToExpected = new Date("2024-09-27T18:45:08+02:00");
    const dr = new DateRangeFilter("dateRange", "label.dateRange", "nameFrom", "nameTo");
    dr.controllerFrom.setValue(new Date("2024-09-26T18:45:08+02:00"));
    dr.controllerTo.setValue(new Date("2024-09-26T18:45:08+02:00"));
    const value = dr.value();

    expect(value?.data).toEqual({ nameFrom: dateFrom, nameTo: dateToExpected });

    dr.reset();

    expect(dr.value()?.data).toBeFalsy();
  });

});
