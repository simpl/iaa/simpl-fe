import { TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { ApiService } from '@fe-simpl/api';
import { TranslocoTestingModule } from '@jsverse/transloco';
import { ComponentInput, OutputRefKeysWithCallback, RenderResult, render } from '@testing-library/angular';
import { fireEvent, getAllByRole, getAllByTestId, getByTestId, getByText, screen, waitFor } from '@testing-library/dom';
import userEvent, { UserEvent } from '@testing-library/user-event';
import { of } from 'rxjs';
import {
    AppDomain,
    translocoTestConfiguration
} from "test-environment";
import { FilterComponent, SubmitData, TextFilter } from '../filter/filter.component';
import { Badge, ButtonActions, PilotType, SearchPageComponent, StatusBadge, dateMapperFn } from './search-page.component';
import { HttpParams } from '@angular/common/http';

jest.mock('@fe-simpl/api');

interface StubRowDto {
    id: string;
    name: string;
}
function stubRow(id: string, name: string): StubRowDto {
  return {
    id,
    name,
  }
}

interface SearchResponseDto<T = unknown> {
  content: T[],
  page: PageMetadataDto,
}

interface PageMetadataDto {
  size: number,
  page: number,
  totalElements: number,
  totalPages: number,
}

function stubSearchResponse<T>(content: T[], pagination: PageMetadataDto): SearchResponseDto<T> {
  return {
    content,
    page: pagination,
  };
}

function getChildFilterComponent({fixture}: RenderResult<SearchPageComponent, SearchPageComponent>): FilterComponent {
    return fixture.debugElement.query(By.directive(FilterComponent)).componentInstance as FilterComponent;
}


interface RenderConfig {
  inputs: ComponentInput<SearchPageComponent>,
  on?: OutputRefKeysWithCallback<SearchPageComponent>,
}
function renderComponent({inputs, on}: RenderConfig) {
  TestBed.configureTestingModule({
      providers: [ApiService],
      imports: [
        SearchPageComponent,
        TranslocoTestingModule.forRoot(translocoTestConfiguration(AppDomain.PARTICIPANT_UTILITY)),
      ],
  }).overrideComponent(FilterComponent, {
    set: {
      selector: "lib-filter",
      template: '<div data-testid="mock-filter">mock-filter</div>',
    }
  });
    return render(SearchPageComponent, {
      inputs,
      on,
    });
}

function mockPaginationResponse(page?: number, size?: number, totalPages?: number, totalElements?: number) {
  return {
    page: page || 1,
    size: size || 5,
    totalPages: totalPages || 3,
    totalElements: totalElements || 12,
  };
}

function mockApiService<T = StubRowDto>(rows?: T[]): SearchResponseDto<T> {
    const inputRows = rows || [
      stubRow("ID_0000", "ID_0000 row in test"),
      stubRow("ID_0001", "ID_0001 row in test"),
      stubRow("ID_0002", "ID_0002 row in test"),
      stubRow("ID_0003", "ID_0003 row in test"),
      stubRow("ID_0004", "ID_0004 row in test"),
    ];
    const response = stubSearchResponse(inputRows as T[], mockPaginationResponse());
    jest.mocked(ApiService).prototype.get.mockReturnValue(of(response));
    return response;
}

function getCheckbox(rowNumber: number): HTMLInputElement | undefined {
    const cxlist = screen.getAllByTestId("select-element");
    return cxlist[rowNumber].querySelector("input[type=checkbox]") as HTMLInputElement;
}

async function clickOnRowCheckbox(user: UserEvent, row: number) {
    const selectAllElement = await screen.findAllByTestId("select-element");
    const checkBox = selectAllElement[row].querySelector("input[type=checkbox]")!;
    return await user.click(checkBox);
}

async function clickOnSelectAllCheckbox(user: UserEvent) {
    const selectAllElement = await screen.findByTestId("select-all-element");
    const checkBox = selectAllElement.querySelector("input[type=checkbox]")!;
    await user.click(checkBox);
}

async function clickOnChangePageSize(user: UserEvent, option: number) {
    await screen.findByTestId("pagination");
    const pagesList = screen.getByRole("combobox");
    await user.click(pagesList);
    const listbox = await screen.findByRole("listbox");
    const options = getAllByRole(listbox, "option");
    return user.click(options[option]);
}

async function clickOnNextPage() {
    const pagination = await screen.findByTestId("pagination");
    const nextButton = pagination.querySelector(".mat-mdc-paginator-navigation-next")!;
    fireEvent.click(nextButton);
}

async function sortColumns(user: UserEvent, column: number) {
  const columns = await waitFor(() => {
    const cols = document.querySelectorAll(".mat-sort-header-container");
    expect(cols).toBeTruthy();
    expect(cols.length).toBeGreaterThan(0);
    return cols;
  });
  return user.click(columns[column]);
}

function getSearchParam(param: string) {
    const getApi = jest.mocked(ApiService).prototype.get;
    const params = getApi.mock.lastCall![1];
    return params?.get(param);
}

beforeEach(() => {
  jest.resetAllMocks();
});

describe('SearchPageComponent - Basic display', () => {

  it('should display information from search', async () => {
    const response = mockApiService();
    await renderComponent({inputs: {
      pageInfo: {
        endpoint: "unit-tests/search",
        columns: {
          id: { header: "element.id" },
          name: { header: "element.name" },
        }
      },
    }});

    for(const row of response.content) {
      await screen.findByText(row.id);
      await screen.findByText(row.name);
    }

  })

  it('should call endpoint with default params', async () => {
    mockApiService();
    await renderComponent({inputs: {
      pageInfo: {
        endpoint: "unit-tests/search",
        columns: {}
      },
    }});

    expect(ApiService.prototype.get).toHaveBeenCalledTimes(1);
    const args = jest.mocked(ApiService).prototype.get.mock.lastCall;
    expect(args).toBeTruthy();

    const [url, params] = args!;
    expect(url).toBe("unit-tests/search");

    expect(params?.keys()?.length).toBe(2);
    expect(params?.get("page")).toBe("0");
    expect(params?.get("size")).toBe("5");
  });

  it('should call endpoint with static parameters', async () => {
    mockApiService();
    await renderComponent({inputs: {
      staticFilters: { name: "my-custom-key", value: "my-custom-value" },
      pageInfo: {
        endpoint: "unit-tests/search",
      },
    }});

    expect(ApiService.prototype.get).toHaveBeenCalledTimes(1);
    const args = jest.mocked(ApiService).prototype.get.mock.lastCall;
    expect(args).toBeTruthy();

    const [url, params] = args!;
    expect(url).toBe("unit-tests/search");

    expect(params?.get("my-custom-key")).toBe("my-custom-value");
  });

  it('should display different columns types', async () => {
    const rows = [
      { text: "simple-text", badge: "APPROVED", date: "2024-09-23T11:14+02:00"}
    ];
    mockApiService(rows);
    await renderComponent({inputs: {
      pageInfo: {
        endpoint: "unit-tests/search",
        columns: {
          text: { header: "text-header" },
          badge: { header: "text-header", mapper: value => new StatusBadge(value) },
          date: { header: "text-header", mapper: value => new Date(value) },
        }
      },
    }});

    const elements = screen.getAllByTestId("element");
    expect(elements.length).toBe(3);

    const badge = screen.getByTestId("badge-element");
    expect(badge.classList.contains("bg-success")).toBeTruthy();
    getByText(badge, /approved/i);

    const date = screen.getByTestId("date-element");
    getByText(date, /23/);

    const text = screen.getByTestId("text-element");
    getByText(text, /simple-text/);

  });

  it('should display correct badge', async () => {
    const rows = [
      {
        approved: "APPROVED",
        rejected: "REJECTED",
        unknown: "UNKNOWN",
        custom: "CUSTOM",
      }
    ];
    mockApiService(rows);
    await renderComponent({inputs: {
      pageInfo: {
        endpoint: "unit-tests/search",
        columns: {
          approved: { header: "text-header", mapper: value => new StatusBadge(value) },
          rejected: { header: "text-header", mapper: value => new StatusBadge(value) },
          unknown: { header: "text-header", mapper: value => new StatusBadge(value) },
          custom: { header: "text-header", mapper: value => new Badge("custom", value) },
        }
      },
    }});

    const badges = screen.getAllByTestId("badge-element");
    const [ approved, rejected, unknown, custom ] = badges;

    approved.classList.contains("bg-success");
    getByText(approved, /approved/i);

    rejected.classList.contains("bg-dange");
    getByText(rejected, /rejected/i);

    unknown.classList.contains("bg-secondary");
    getByText(unknown, /unknown/i);

    custom.classList.contains("bg-custom");
    getByText(custom, /custom/i);
  });

  it('should display custom buttons', async () => {

    const rows = [ { id: "button-test" } ];
    mockApiService(rows);
    const buttonAction = jest.fn();
    await renderComponent({
        inputs: {
          pageInfo: {
            endpoint: "unit-tests/search",
            columns: {
              buttons: {
                header: "buttons",
                mapper: () => new ButtonActions([
                  { action: buttonAction },
                  { label: "Btn1", action: buttonAction },
                  { label: "Btn2", theme: "primary", action: buttonAction },
                  { label: "Btn3", color: "red", action: buttonAction },
                  { label: "Btn4", icon: "custom-icon", action: buttonAction },
                ]),
              }
            }
          },
        }
      });

    const buttonsGroup = await screen.findByTestId("button-element-group");
    const buttons = getAllByTestId(buttonsGroup, "button-element");

    expect(buttons.length).toBe(5);
    const [btndef, btn1, btn2, btn3, btn4 ] = buttons;

    fireEvent.click(btndef);
    expect(buttonAction).toHaveBeenCalledTimes(1);
    expect(buttonAction.mock.lastCall[0].id).toBe("button-test");

    getByText(btn1, /Btn1/);
    expect(btn2.classList).toContain("btn-primary");
    expect(btn3.classList).toContain("bg-red");

    getByText(getByTestId(btn4, "btn-icon"), /custom-icon/);

  });

  it('should support change page size', async () => {
    const user = userEvent.setup();
    mockApiService();
    await renderComponent({ inputs: {
      pageInfo: {
        endpoint: "unit-tests/search",
        columns: {
          id: { header: "element.id" },
          name: { header: "element.name" },
        }
      }
    }});

    await clickOnChangePageSize(user, 1);

    const getApi = jest.mocked(ApiService).prototype.get;
    expect(getApi).toHaveBeenCalledTimes(2);

    const param = getApi.mock.lastCall![1];
    expect(param?.get("size")).toBe("10");


  });

  it('should support change page number', async () => {
    mockApiService();
    await renderComponent({ inputs: {
      pageInfo: {
        endpoint: "unit-tests/search",
        columns: {
          id: { header: "element.id" },
          name: { header: "element.name" },
        }
      }
    }});

    await clickOnNextPage();

    const getApi = jest.mocked(ApiService).prototype.get;
    await waitFor(() => expect(getApi).toHaveBeenCalledTimes(2));

    const param = getApi.mock.lastCall![1];
    expect(param?.get("page")).toBe("1");
  });

});

describe('SearchPageComponent - sort operations', () => {

  it('should sort column', async () => {
    const user = userEvent.setup();
    mockApiService();
    renderComponent({inputs: {
      pageInfo: {
        endpoint: "unit-tests/search",
        columns: {
          id: { header: "element.id", sort: true},
        }
      },
    }});

    await sortColumns(user, 0);
    expect(getSearchParam("sort")).toBe("id,asc");

    await sortColumns(user, 0);
    expect(getSearchParam("sort")).toBe("id,desc");

    await sortColumns(user, 0);
    expect(getSearchParam("sort")).toBe("id,");
  });

});

describe('SearchPageComponent - selection operations', () => {

  it('should support select all', async () => {
    const user = userEvent.setup();
    const handleSelection = jest.fn();
    mockApiService();
    await renderComponent({
        inputs: {
          pageInfo: {
            endpoint: "unit-tests/search",
            columns: {
              select: { header: "select", isCheckbox: true },
              id: { header: "element.id" },
            }
          }
        },
        on: {
          checkboxSelectionEmitter: handleSelection,
        },
      });

    await clickOnSelectAllCheckbox(user);
    expect(handleSelection).toHaveBeenCalledTimes(1);
    expect(handleSelection).toHaveBeenLastCalledWith([ 'ID_0000', 'ID_0001', 'ID_0002', 'ID_0003', 'ID_0004' ]);

    await clickOnSelectAllCheckbox(user);
    expect(handleSelection).toHaveBeenCalledTimes(2);
    expect(handleSelection).toHaveBeenLastCalledWith([]);

  });

  it('should support select different rows', async () => {
    const user = userEvent.setup();
    const handleSelection = jest.fn();
    mockApiService();
    await renderComponent({
        inputs: {
          pageInfo: {
            endpoint: "unit-tests/search",
            columns: {
              select: { header: "select", isCheckbox: true },
              id: { header: "element.id" },
            }
          }
        },
        on: {
          checkboxSelectionEmitter: handleSelection,
        },
      });

    await clickOnRowCheckbox(user, 0);
    expect(handleSelection).toHaveBeenCalledTimes(1);
    expect(handleSelection).toHaveBeenLastCalledWith([ 'ID_0000' ]);
    expect(getCheckbox(0)?.checked).toBeTruthy();

    await clickOnRowCheckbox(user, 0);
    expect(handleSelection).toHaveBeenCalledTimes(2);
    expect(handleSelection).toHaveBeenLastCalledWith([]);
    expect(getCheckbox(0)?.checked).toBeFalsy();

    await clickOnRowCheckbox(user, 0);
    await clickOnRowCheckbox(user, 1);
    expect(getCheckbox(0)?.checked).toBeTruthy();
    expect(getCheckbox(1)?.checked).toBeTruthy();
    expect(handleSelection).toHaveBeenLastCalledWith([ 'ID_0000', 'ID_0001' ]);

  });
});

describe('SearchPageComponent - filters operations', () => {

  it('should map correct filter', async () => {
    const filters = {
      filter1: new TextFilter("name", "label"),
    };

    mockApiService();
    const renderResult = await renderComponent({
      inputs: {
        pageInfo: {
          endpoint: "/api/filters",
          filters,
        }
      }
    });

    expect(getChildFilterComponent(renderResult).filters()).toBe(filters);
  });

  it('should search with filters', async () => {
    const filters = {
      filter1: new TextFilter("filter1", "label1"),
    };

    mockApiService();
    const renderResult = await renderComponent({
      inputs: {
        pageInfo: {
          endpoint: "/api/filters",
          filters,
        }
      }
    });

    const filterComponent = getChildFilterComponent(renderResult);
    const submitData: SubmitData = {
      textFilter: "text-filter",
      dataFilter: new Date("2024-09-26T00:39:38+02:00"),
      undefined: undefined,
    };

    filterComponent.submit.emit(submitData);

    const getApi = jest.mocked(ApiService).prototype.get;
    expect(getApi).toHaveBeenCalledTimes(2);

    const param = getApi.mock.lastCall![1];
    expect(param?.get("textFilter")).toBe("text-filter");
    expect(param?.get("dataFilter")).toBe("2024-09-25T22:39:38.000Z");
    expect(param?.get("undefined")).toBe(null);
  });

});

describe('FilterFn', () => {

  it('DateFilterFn', () => {
    const date = dateMapperFn("2024-09-27T14:25:59+02:00");
    expect(date).toEqual(new Date("2024-09-27T14:25:59+02:00"));
    const emptyDate = dateMapperFn(null);
    expect(emptyDate).toBe("");
  });

});

describe('Pilot', () => {

  it('should be load', async () => {
    const testingPilot: PilotType = {};
    await renderComponent({inputs: {
      pilot: testingPilot,
      pageInfo: {
        endpoint: "unit-tests/search",
        columns: {
          id: { header: "element.id" },
        }
      },
    }});

    const getApi = jest.mocked(ApiService).prototype.get;
    const reloadFn = testingPilot["reload"];
    expect(typeof reloadFn).toBe("function");

    expect(getApi).toHaveBeenCalledTimes(1);

    reloadFn();
    expect(getApi).toHaveBeenCalledTimes(2);
  });
});

describe('SearchPageComponent - more tests for coverage', () => {
  it('should handle row actions', async () => {
    const rows = [ stubRow("ID_0000", "Test Row 1") ];
    const rowActionMock = jest.fn();
    mockApiService(rows);

    await renderComponent({
      inputs: {
        pageInfo: {
          endpoint: "unit-tests/search",
          columns: {
            id: { header: "element.id" },
            name: { header: "element.name" },
          }
        },
        clickable: true,
      },
      on: {
        rowAction: rowActionMock,
      },
    });

    const row = await screen.findByText("ID_0000");
    fireEvent.click(row);

    expect(rowActionMock).toHaveBeenCalledWith({ id: "ID_0000", name: "Test Row 1" });
  });

  it('should apply filters correctly', async () => {
    const filters = {
      name: new TextFilter("name", "Name")
    };

    const submitData: SubmitData = { name: "Test Name" };
    mockApiService();

    const renderResult = await renderComponent({ inputs: {
      pageInfo: {
        endpoint: "unit-tests/search",
        filters
      }
    }});

    const filterComponent = getChildFilterComponent(renderResult);
    filterComponent.submit.emit(submitData);

    const getApi = jest.mocked(ApiService).prototype.get;
    await waitFor(() => expect(getApi).toHaveBeenCalledWith(
      "unit-tests/search",
      expect.any(HttpParams)
    ));
    expect(getSearchParam("name")).toBe("Test Name");
  });
});
