import {
  Component,
  OnInit,
  computed,
  effect,
  inject,
  input,
  output,
  signal,
  OnChanges,
  SimpleChanges, ViewChild, Input
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslocoDirective } from '@jsverse/transloco';
import { MatTableModule } from '@angular/material/table';
import { RemoveUnderscorePipe } from '@fe-simpl/core/pipes';
import { RouterModule } from '@angular/router';
import { LoaderComponent } from '@fe-simpl/loader';
import { MatPaginator } from '@angular/material/paginator';
import { ApiService } from '@fe-simpl/api';
import { finalize, mergeMap, of, tap } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import { MatSortModule } from '@angular/material/sort';
import { MatCheckboxModule } from '@angular/material/checkbox';
import {
  FilterComponent,
  FilterGroup,
  FilterValueType,
  SubmitData
} from '../filter/filter.component';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { SelectionModel } from '@angular/cdk/collections';
import { RemovePlaceholderTempPipe } from '../pipes/remove-ph.pipe';

export type MapperFn = (value: any) => ValidTypesSearch;
export const dateMapperFn: MapperFn = (v) => (v ? new Date(v) : '');

export type ValidTypesSearch =
  | string
  | number
  | boolean
  | Badge
  | Date
  | ButtonActions;

export class Badge {
  readonly type: string;
  readonly text: string;

  constructor(type: string, text: string) {
    this.type = type;
    this.text = text;
  }
}

export class StatusBadge extends Badge {
  constructor(status: string) {
    let type = 'secondary';
    if(status === 'APPROVED'){
      type = 'success'
    } else if(status === 'REJECTED'){
      type = 'danger'
    }
    super(type,status);
  }
}

export class ButtonActions {
  readonly buttons: ButtonActionsDef[];

  constructor(buttons: ButtonActionsDef[]) {
    this.buttons = buttons;
  }

  toString() {
    return '';
  }
}

export interface ButtonActionsDef {
  action: (row: any) => void;
  theme?: string;
  color?: string;
  icon?: string;
  label?: string;
}

export interface ColumnInfo {
  def: string;
  header: string;
  key: string;
  sort: boolean;
  mapper: MapperFn;
  isCheckbox: boolean;
  classes?: Array<string>;
}

// Column configuration object
export interface ColumnDef {

  // The label displayed in the header row
  header: string;

  // Enables sorting of the column
  sort?: boolean;

  // Mapper function to transform the data from the
  // http response to displayed element
  mapper?: MapperFn;

  // Display a checkbox
  isCheckbox?: boolean;

  // Custom class for the column
  classes?: Array<string>;
}

// Search page configuration
export interface SearchPageInfo {

  // URL of a compatible search API.
  endpoint: string;

  // Filters configuration
  filters?: FilterGroup;

  // Columns configuration
  // Each key attribute of columns object becomes the configuration of a specific column.
  columns?: {
    [key: string]: ColumnDef;
  };
}

export interface SearchDataSource {
  content: any;
  page: {
    number: number;
    totalPages: number;
    totalElements: number;
    size: number;
  };
}

interface SortParams {
  active: string;
  direction: string;
}

export type PilotType = { [k: string]: (...args: any) => any } | null;


export interface StaticFilter {
  name: string,
  value: string,
}

/**
 * SearchPageComponent
 *
 * Simplifies the creation of a page with a table directly connected to a backend search endpoint.
 *
 * Features:
 * - Column definition
 * - Column sorting
 * - Pagination
 * - Filtering
 * - Row selection
 */
@Component({
  selector: 'lib-search-page',
  standalone: true,
  imports: [
    CommonModule,
    MatTableModule,
    RemoveUnderscorePipe,
    LoaderComponent,
    RouterModule,
    MatPaginator,
    TranslocoDirective,
    MatSortModule,
    FilterComponent,
    MatIconModule,
    MatButtonModule,
    MatCheckboxModule,
    RemovePlaceholderTempPipe
  ],
  templateUrl: './search-page.component.html',
  styleUrl: './search-page.component.css'
})
export class SearchPageComponent implements OnInit, OnChanges {

  // Input fields
  // Key reference for transloco key
  translocoPrefix = input('');

  //Inject fields
  private readonly apiService = inject(ApiService);

  @Input()
  disabledCheckbox = false;

  // Page info configuration
  pageInfo = input<SearchPageInfo>();

  // Make each row clickable to trigger action
  clickable = input<boolean>(false);
  offlinePaginator = input<boolean>(false);
  // Add a static filter in endpoint calls
  staticFilters = input<StaticFilter>();
  paramToUse = input<string>('id');
  allDataRow = input<boolean>(false);
  feature = input<'request-details'>();

  // Special input field
  pilot = input<PilotType>();
  checkboxToCheck = input<any[]>([]);
  // Output actions
  rowAction = output<any>();

  // Signal fields
  loading = signal(false);
  searchResponse = signal<SearchDataSource | null>(null);
  sort = signal<SortParams | undefined>(undefined);
  @ViewChild('offlinePaginatorElement') offlinePaginatorElement!: MatPaginator;
  // Filters fields
  filters: SubmitData;

  // Checkbox fields
  checkboxSelection = new SelectionModel<any>(true);
  unchecked: Array<string> = [];
  checkboxSelectionEmitter = output<any>();
  localDataSource: SearchDataSource = {
    content: [],
    page: {
      number: 0,
      totalPages: 0,
      totalElements: 0,
      size: 0,
    }
  } ;
  paginatedRows: SearchDataSource = {
    content: [],
    page: {
      number: 0,
      totalPages: 0,
      totalElements: 0,
      size: 0,
    }
  };

  constructor() {
    effect(() => {
      const pilot = this.pilot();
      if (pilot) {
        pilot['reload'] = () => this.reload();
      }
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (
      changes['pageInfo']?.previousValue &&
      changes['pageInfo']?.currentValue
    ) {
      this.searchAction(this.currentPage(), this.pageSize());
    }
  }

  filterGroup = computed(() => this.pageInfo()?.filters);
  dataSource = computed(() => {
    const response = this.searchResponse();

    const pageInfo = this.pageInfo();
    const data = response?.content ? response.content : response;

    if (response) {
      response.content = (data as Array<any>).map((row) => {
        Object.entries(pageInfo?.columns ?? {}).forEach(([k, columnInfo]) => {
          if (columnInfo) {
            const mapper = columnInfo.mapper;
            if (mapper) {
              row[k] = mapper(row[k]);
            }
          }
        });
        // When feature is request details, all checkbox are enabled at start by default
        if (
          this.feature() === 'request-details' &&
          !this.unchecked.find((item) => item === row?.[this.paramToUse()])
        ) {
          this.checkboxSelection.select(row?.[this.paramToUse()]);
        }
        return row;
      });

      response.content.forEach((row: any) => {
        if (this.checkboxToCheck().length === 0) {
          this.checkboxSelection.deselect(row?.[this.paramToUse()]);
        }
        if (this.allDataRow()) {
          const a = this.checkboxToCheck().find(item => item.id === row.id)
          if (a) {
            this.checkboxSelection.select(row);
          } else {
            this.checkboxSelection.deselect(row);
          }
        } else {
          const a = this.checkboxToCheck().find(item => item === row?.[this.paramToUse()])
          if (a) {
            this.checkboxSelection.select(a);
          } else {
            this.checkboxSelection.deselect(row?.[this.paramToUse()]);
          }
        }

      })
    }
    if (this.offlinePaginator() && response?.content) {
      this.localDataSource.content = response.content ? response.content : response;
    }
    return response;
  });

  columns = computed<ColumnInfo[]>(() =>
    generateColumnInfoList(this.pageInfo())
  );
  displayedColumns = computed<string[]>(
    () => this.columns()?.map((col) => col.def) ?? []
  );
  rows = computed(() => this.dataSource()?.content);
  totalElements = computed(() => this.dataSource()?.page?.totalElements ?? this.dataSource()?.content.length);
  pageSize = computed(() =>
    this.offlinePaginator()
      ? this.dataSource()?.page?.totalElements ?? 5
      : this.dataSource()?.page?.size ?? 5
  );
  currentPage = computed(() => this.dataSource()?.page?.number ?? 0);

  ngOnInit(): void {
    this.searchAction(0, 5);
  }

  reload() {
    this.searchAction(this.currentPage(), this.pageSize());
  }

  searchAction(page: number, size: number) {
    const sort = this.sort();
    const response = this.searchResponse();
    if (response) {
      this.searchResponse.set({
        ...response,
        content: []
      });
    }
    const info = this.pageInfo();
    const params = httpParam(page, size, this.filters, sort);
    const staticFilters = this.staticFilters();
    if (info) {
      of(true)
        .pipe(
          tap(this.loading.set),
          mergeMap(() =>
            this.apiService.get<SearchDataSource>(
              info.endpoint,
              staticFilters
                ? params.append(
                  staticFilters.name,
                  staticFilters.value
                )
                : params
            )
          ),
          finalize(() => this.loading.set(false)),
        )
        .subscribe({
          next: (response) => {
            if (response) {
              this.searchResponse.set(response);
              if (this.offlinePaginator()) {
                this.localDataSource.content = response.content ?? [];
                this.updatePaginatedRows(0, this.pageSize());
              }
            }
          }
        });
    }
  }

  updatePaginatedRows(pageIndex: number, pageSize: number) {
    const startIndex = pageIndex * pageSize;
    this.paginatedRows.content = this.localDataSource?.content?.slice(startIndex, startIndex + pageSize);
  }

  onSortChange(event: any) {
    if (this.offlinePaginator()) {

      const sortedContent = this.localDataSource?.content?.sort((a: any, b: any) => {
        const field = event.active;
        const direction = event.direction === 'asc' ? 1 : -1;

        if (a[field] < b[field]) {
          return -1 * direction;
        } else if (a[field] > b[field]) {
          return 1 * direction;
        } else {
          return 0;
        }
      });

      this.paginatedRows.content = [...sortedContent];
      this.updatePaginatedRows(this.offlinePaginatorElement.pageIndex, this.offlinePaginatorElement.pageSize);
    } else {
      this.sort.set({
        active: event.active,
        direction: event.direction
      });
      this.searchAction(this.currentPage(), this.pageSize());
    }

  }

  isBadge(element: ColumnDef): boolean {
    return element instanceof Badge;
  }

  isDate(element: ColumnDef): boolean {
    return element instanceof Date;
  }

  isButtonAction(element: ColumnDef): ButtonActionsDef[] | null {
    return element instanceof ButtonActions ? element.buttons : null;
  }

  isCheckbox(element: ColumnDef) {
    return element.isCheckbox;
  }

  isAllSelected() {
    let numSelected;
    if (this.allDataRow()) {
      numSelected = this.rows().filter((row: any) => {
        return this.checkboxSelection.selected.find((item) => item.id === row.id);
      }).length;
    } else {
      numSelected = this.rows().filter((row: any) => {
        return this.checkboxSelection.selected.find((item) => item === row?.[this.paramToUse()]);
      }).length;
    }

    const numRows = this.rows().length;
    return numSelected === numRows;
  }

  toggleAllRows() {
    if (this.isAllSelected()) {
      if (this.allDataRow()) {
        this.rows().forEach((row: any) => {
          this.unchecked.push(row);
          this.checkboxSelection.deselect(row);
        });
      } else {
        this.rows().forEach((row: any) => {
          this.unchecked.push(row?.[this.paramToUse()]);
          this.checkboxSelection.deselect(row?.[this.paramToUse()]);
        });
      }

    } else {

      if (this.allDataRow()) {
        this.rows().forEach((row: any) => {
          if (!this.checkboxSelection.selected.find((item) => item.id === row.id)) {
            this.unchecked = this.unchecked.filter((item) => item !== row);
            this.checkboxSelection.select(row);
          }
        });
      } else {
        this.rows().forEach((row: any) => {
          if (!this.checkboxSelection.selected.find((item) => item === row?.[this.paramToUse()])) {
            this.unchecked = this.unchecked.filter((item) => item !== row?.[this.paramToUse()]);
            this.checkboxSelection.select(row?.[this.paramToUse()]);
          }
        });
      }

    }
    // When feature is request details, only the unselected rows are emitted
    this.checkboxSelectionEmitter.emit(
      this.feature() === 'request-details'
        ? this.unchecked
        : this.checkboxSelection.selected
    );
  }

  handleChangeRowCheckbox(row: string) {
    if (!this.checkboxSelection.isSelected(row)) {
      this.unchecked = this.unchecked.filter((item) => item !== row);
      this.checkboxSelection.select(row);
    } else {
      this.unchecked.push(row);
      this.checkboxSelection.deselect(row);
    }
    // When feature is request details, only the unselected rows are emitted
    this.checkboxSelectionEmitter.emit(
        this.feature() === 'request-details'
        ? this.unchecked
        : this.checkboxSelection.selected
    );
  }

  onPageChange(event: any) {
    if (this.offlinePaginator()) {
      this.updatePaginatedRows(event.pageIndex, event.pageSize);
    } else {
      this.searchAction(event.pageIndex, event.pageSize);
    }
  }

  onFilterSubmit(form: SubmitData) {
    this.filters = form;
    this.searchAction(0, this.pageSize());
  }
}

function generateColumnInfoList(info?: SearchPageInfo): ColumnInfo[] {
  return Object.keys(info?.columns ?? {}).map((k) => {
    const conf = info!.columns![k];
    return {
      def: k,
      header: conf.header,
      key: k,
      mapper: conf.mapper ?? ((v) => v),
      sort: conf.sort ?? false,
      isCheckbox: conf.isCheckbox ?? false,
      classes: conf.classes
    };
  });
}

function httpParam(
  page: number,
  size: number,
  filters: SubmitData,
  sort?: SortParams
): HttpParams {
  let params = new HttpParams()
    .set('page', page)
    .set('size', size);
  if (sort) {
    params = params.set('sort', sort.active + ',' + sort.direction);
  }

  if (filters) {
    params = Object.keys(filters).reduce(
      (params, k) =>
        filters[k] ? params.set(k, mapControlValue(filters[k])) : params,
      params
    );
  }
  return params;
}

function mapControlValue(value: FilterValueType): string {
  if (value) {
    if (value instanceof Date) {
      return value.toISOString();
    } else {
      return String(value);
    }
  } else {
    return '';
  }
}
