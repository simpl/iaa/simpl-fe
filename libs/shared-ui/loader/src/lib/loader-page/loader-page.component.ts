import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoaderComponent } from '../loader/loader.component';

@Component({
  selector: 'lib-loader-page',
  standalone: true,
  imports: [CommonModule, LoaderComponent],
  templateUrl: './loader-page.component.html',
  styleUrl: './loader-page.component.css',
})
export class LoaderPageComponent {}
