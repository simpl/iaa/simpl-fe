import { ComponentFixture, TestBed } from "@angular/core/testing";
import { ConfirmDialogComponent, DialogData } from "./confirm-dialog.component";
import { TranslocoTestingModule } from "@jsverse/transloco";
import { translocoTestConfiguration } from "@test-environment";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
describe("confirmDialogComponent", () => {
  let component: ConfirmDialogComponent;
  let fixture: ComponentFixture<ConfirmDialogComponent>;
  const mockDialogData: DialogData = {
    title: "dialogTitle",
    message: "dialogMessage",
    message2: "dialogMessage2",
    confirm: "confirmButton",
    cancel: "cancelButton",
  };
  const translations = {
    title: "Default Title",
    message: "Default Message",
    cancel: "Default Cancel Button",
    confirm: "Default Confirm Button",
    dialogTitle: "Dialog Title",
    dialogMessage: "First Message",
    dialogMessage2: "Second Message",
    confirmButton: "Confirm Button",
    cancelButton: "Cancel Button",
  };
  beforeEach(async () => {
    TestBed.configureTestingModule({
      imports: [
        ConfirmDialogComponent,
        TranslocoTestingModule.forRoot(
          translocoTestConfiguration(undefined, translations)
        ),
      ],
      providers: [
        {
          provide: MAT_DIALOG_DATA,
          useValue: mockDialogData,
        },
        { provide: MatDialogRef, useValue: {} },
      ],
    });
  });

  it("should create", () => {
    TestBed.compileComponents();
    fixture = TestBed.createComponent(ConfirmDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it("should render UI correctly with default labels", () => {
    TestBed.overrideProvider(MAT_DIALOG_DATA, {
      useValue: {},
    });
    TestBed.compileComponents();
    fixture = TestBed.createComponent(ConfirmDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    const title = fixture.debugElement.nativeElement.querySelector("h2");
    const dialogActions =
      fixture.debugElement.nativeElement.querySelector("mat-dialog-actions");
    const buttons = dialogActions.querySelectorAll("button");

    // Assert HTML elements are rendered
    expect(title).not.toBeNull();
    expect(dialogActions).not.toBeNull();
    expect(buttons.length).toBeTruthy();

    // Assert translations are correct
    expect(title.textContent).toEqual(translations.title);
    expect(buttons[0].textContent).toEqual(translations.cancel);
    expect(buttons[1].textContent).toEqual(translations.confirm);
  });

  it("should render UI correctly without noCloseButtons", () => {
    TestBed.compileComponents();
    fixture = TestBed.createComponent(ConfirmDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    const title = fixture.debugElement.nativeElement.querySelector("h2");
    const messages = fixture.debugElement.nativeElement.querySelectorAll("p");
    const dialogActions =
      fixture.debugElement.nativeElement.querySelector("mat-dialog-actions");
    const buttons = dialogActions.querySelectorAll("button");

    // Assert HTML elements are rendered
    expect(title).not.toBeNull();
    expect(messages.length).toBeTruthy();
    expect(dialogActions).not.toBeNull();
    expect(buttons.length).toBeTruthy();

    // Assert translations are correct
    expect(title.textContent).toEqual(translations.dialogTitle);
    expect(messages[0].textContent).toEqual(translations.dialogMessage);
    expect(messages[1].textContent).toEqual(translations.dialogMessage2);
    expect(buttons[0].textContent).toEqual(translations.cancelButton);
    expect(buttons[1].textContent).toEqual(translations.confirmButton);
  });

  it("should render UI correctly with noCloseButtons", () => {
    TestBed.overrideProvider(MAT_DIALOG_DATA, {
      useValue: {
        ...mockDialogData,
        noCloseButtons: true,
      },
    });
    TestBed.compileComponents();
    fixture = TestBed.createComponent(ConfirmDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    const title = fixture.debugElement.nativeElement.querySelector("h2");
    const messages = fixture.debugElement.nativeElement.querySelectorAll("p");
    const dialogActions =
      fixture.debugElement.nativeElement.querySelector("mat-dialog-actions");

    // Assert HTML elements are rendered
    expect(title).not.toBeNull();
    expect(messages.length).toBeTruthy();
    expect(dialogActions).toBeNull();

    // Assert translations are correct
    expect(title.textContent).toEqual(translations.dialogTitle);
    expect(messages[0].textContent).toEqual(translations.dialogMessage);
    expect(messages[1].textContent).toEqual(translations.dialogMessage2);
  });
});
