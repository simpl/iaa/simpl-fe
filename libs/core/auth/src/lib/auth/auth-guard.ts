import {
  ActivatedRouteSnapshot,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from "@angular/router";
import { inject } from "@angular/core";
import { KeycloakService } from "keycloak-angular";
import { UserService } from '@eui/core';

export const authGuard = async (
  route: ActivatedRouteSnapshot,
  state: RouterStateSnapshot
): Promise<boolean | UrlTree> => {
  const keycloak = inject(KeycloakService);
  const router = inject(Router);
  const userService: UserService = inject(UserService);

  if (!keycloak.isLoggedIn()) {
    await keycloak.login({
      redirectUri: document.baseURI.replace(/\/$/, "") + state.url,
    });
  }

  userService.init({
    userId: keycloak.getKeycloakInstance()?.tokenParsed?.['participant_id'],
    firstName: keycloak.getKeycloakInstance()?.tokenParsed?.['given_name'],
    lastName: keycloak.getKeycloakInstance()?.tokenParsed?.['family_name'],
    fullName: keycloak.getKeycloakInstance()?.tokenParsed?.['name']
  })

  const requiredRoles = route.data["roles"];
  let granted = false;
  const roles = keycloak.getUserRoles();

  if (!requiredRoles || requiredRoles.length === 0) {
    granted = true;
  } else {
    for (const requiredRole of requiredRoles) {
      if (roles.indexOf(requiredRole) > -1) {
        granted = true;
        break;
      }
    }
  }

  if (!granted) {
    await router.navigate(["/unauthorized"]);
  }
  return keycloak.isLoggedIn() && granted;
};
