import {
    ActivatedRouteSnapshot,
    Router,
    RouterStateSnapshot
  } from "@angular/router";
  import { authGuard } from "./auth-guard";
  import { KeycloakService } from "keycloak-angular";
  import { inject } from "@angular/core";
import { UserService } from '@eui/core';

  jest.mock("@angular/core", () => ({
    ...jest.requireActual("@angular/core"),
    inject: jest.fn(),
  }));

describe("authGuard", () => {
    let mockKeycloakService: jest.Mocked<KeycloakService>;
    let mockRouter: jest.Mocked<Router>;
    let mockUserService: jest.Mocked<UserService>;

    beforeEach(() => {
      mockKeycloakService = {
        isLoggedIn: jest.fn(),
        login: jest.fn(),
        getUserRoles: jest.fn(),
        getKeycloakInstance: jest.fn().mockReturnValue({
          tokenParsed: {
            participant_id: '123',
            given_name: 'John',
            family_name: 'Doe',
            name: 'John Doe'
          }
        })
      } as unknown as jest.Mocked<KeycloakService>;

      mockRouter = {
        navigate: jest.fn(),
      } as unknown as jest.Mocked<Router>;

      mockUserService = {
        init: jest.fn()
      } as unknown as jest.Mocked<UserService>;

      (inject as jest.Mock).mockImplementation((token) => {
        if (token === KeycloakService) {
          return mockKeycloakService;
        }
        if (token === Router) {
          return mockRouter;
        }
        if (token === UserService) {
          return mockUserService;
        }
        throw new Error(`Unexpected token ${token}`);
      });
    });

    afterEach(() => {
      jest.clearAllMocks();
    });

    it("should allow access if the user is logged in and roles match", async () => {
      (mockKeycloakService.isLoggedIn as jest.Mock).mockResolvedValue(true);
      mockKeycloakService.getUserRoles.mockReturnValue(["ROLE_ADMIN"]);
      const route = {
        data: { roles: ["ROLE_ADMIN"] },
      } as unknown as ActivatedRouteSnapshot;
      const state = {} as RouterStateSnapshot;
      const result = await authGuard(route, state);
      expect(result).toBe(true);
      expect((mockKeycloakService.isLoggedIn as jest.Mock)).toHaveBeenCalled();
      expect(mockKeycloakService.getUserRoles).toHaveBeenCalled();

      expect(mockUserService.init).toHaveBeenCalledWith({
        userId: '123',
        firstName: 'John',
        lastName: 'Doe',
        fullName: 'John Doe'
      });
    });
  });
