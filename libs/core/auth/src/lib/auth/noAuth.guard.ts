import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Route, Router, RouterStateSnapshot, UrlSegment, UrlTree } from '@angular/router';
import { Observable, of } from 'rxjs';
import { KeycloakService } from 'keycloak-angular';

@Injectable({
  providedIn: 'root'
})
export class NoAuthGuard
{
  /**
   * Constructor
   */
  constructor(
    private readonly _keycloakService: KeycloakService,
    private readonly _router: Router
  )
  {
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Can activate
   *
   * @param route
   * @param state
   */
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean
  {
    return this._check();
  }

  /**
   * Can activate child
   *
   * @param childRoute
   * @param state
   */
  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree
  {
    return this._check();
  }

  /**
   * Can load
   *
   * @param route
   * @param segments
   */
  canLoad(route: Route, segments: UrlSegment[]): Observable<boolean> | Promise<boolean> | boolean
  {
    return this._check();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Private methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Check the authenticated status
   *
   * @private
   */
  private _check(): Observable<boolean>
  {
    if (this._keycloakService.isLoggedIn()){


      const roles = this._keycloakService.getUserRoles();

      if (roles.includes('APPLICANT')) {
        this._router.navigate(['/onboarding/application/additional-request']);
      }

      if (roles.includes('NOTARY')) {
        this._router.navigate(['/onboarding/administration']);
      }

      if (roles.includes('T2IAA_M')) {
        this._router.navigate(['/onboarding/administration/management/onboarding-procedures']);
      }

      if (roles.includes('IATTR_M')) {
        this._router.navigate(['/onboarding/administration/management/participant']);
      }

      if (roles.includes('T1UAR_M')) {
        this._router.navigate(['/users-roles/identity-attributes-info']);
      }

      return of(false);
    } else {
      return of(true);
    }
  }
}
