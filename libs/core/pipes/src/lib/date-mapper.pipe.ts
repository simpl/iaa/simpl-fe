import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dateMapper',
  standalone: true,
})
export class DateMapperPipe implements PipeTransform {
  transform(value: string, ...args: unknown[]): unknown {
    if (value) {
      return new Date(value)
    }
    return ''
  }
}
