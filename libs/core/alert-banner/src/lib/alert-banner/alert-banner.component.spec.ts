import { ComponentFixture, TestBed } from "@angular/core/testing";
import { AlertBannerComponent } from './alert-banner.component';
import { AlertBannerStore } from "./alert-banner.store";
describe("AlertBannerComponent", () => {
  let component: AlertBannerComponent;
  let fixture: ComponentFixture<AlertBannerComponent>;
  let store: any;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AlertBannerComponent],
      providers: [AlertBannerStore],
    }).compileComponents();

    fixture = TestBed.createComponent(AlertBannerComponent);
    component = fixture.componentInstance;
    store = TestBed.inject(AlertBannerStore);
    store.showAlert({
      type: "info",
      message: "message",
    });
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should show alert", () => {
    expect(component.$banners().length).toBeTruthy();
    const alert =
      fixture.debugElement.nativeElement.querySelector(".alert-info");
    expect(alert.textContent).toEqual(" message");
    const closeButton = alert.querySelector("[type=button]");
    expect(closeButton).not.toBeNull();
  });

  it("should dismiss alert", () => {
    const alert =
      fixture.debugElement.nativeElement.querySelector(".alert-info");
    expect(alert).not.toBeNull();
    expect(alert.textContent).toEqual(" message");
    const closeButton = alert.querySelector("[type=button]");
    expect(closeButton).not.toBeNull();
    const spyOnComponentDismiss = jest.spyOn(component, "dismiss");
    const spyOnStoreDismiss = jest.spyOn(store, "dismiss");
    closeButton.click();
    fixture.detectChanges();
    expect(spyOnStoreDismiss).toHaveBeenCalled();
    expect(spyOnComponentDismiss).toHaveBeenCalled();
  });
});
