import {
  signalStore,
  withState,
  withMethods,
  patchState,
  WritableStateSource,
  StateSignals
} from '@ngrx/signals';
import { AlertBanner, AlertBannerStoreData } from './alert-banner.service';

export const AlertBannerStore = signalStore(
  { providedIn: 'root' },
  withState<AlertBannerStoreData>({
    banners: []
  }),
  withMethods((store) => ({
    showAlert: (banner: AlertBanner) => addAlertBanner(store, banner),
    dismiss: (banner: AlertBanner) => removeAlertBanner(store, banner),
  })),
);

function addAlertBanner(
  store: WritableStateSource<AlertBannerStoreData> & StateSignals<AlertBannerStoreData>,
  banner: AlertBanner
) {
  const old = store.banners();
  patchState(store, { banners: [...old, banner] });
}

function removeAlertBanner(
  store: WritableStateSource<AlertBannerStoreData> & StateSignals<AlertBannerStoreData>,
  banner: AlertBanner
) {
  const old = store.banners();
  patchState(store, { banners: old.filter(el => el !== banner) });
}
