import { Injectable, inject } from "@angular/core";
import { HttpClient, HttpParams, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { API_URL } from "./api-url.token";

@Injectable({ providedIn: "root" })
export class ApiService {
  private readonly http = inject(HttpClient);
  private readonly api_url = inject(API_URL);

  get<T>(url: string, params: HttpParams = new HttpParams()): Observable<T> {
    return this.http.get<T>(`${this.api_url}${url}`, {
      headers: this.headers,
      params,
    });
  }

  post<T, D>(url: string, data?: D): Observable<T> {
    return this.http.post<T>(`${this.api_url}${url}`, JSON.stringify(data), {
      headers: this.headers,
    });
  }

  upload<T, D>(url: string, data?: D): Observable<T> {
    return this.http.post<T>(`${this.api_url}${url}`, data, {
      headers: this.headersForUpload,
    });
  }

  uploadFile<T>(url: string, data: FormData): Observable<T> {
    return this.http.post<T>(`${this.api_url}${url}`, data);
  }

  patch<T>(url: string): Observable<T> {
    return this.http.patch<T>(`${this.api_url}${url}`, null);
  }

  put<T, D>(url: string, data?: D): Observable<T> {
    return this.http.put<T>(`${this.api_url}${url}`, JSON.stringify(data), {
      headers: this.headers,
    });
  }

  delete<T>(url: string): Observable<T> {
    return this.http.delete<T>(`${this.api_url}${url}`, {
      headers: this.headers,
    });
  }

  deleteWithBody<T>(url: string, data: T): Observable<void> {
    return this.http.request<void>("delete", `${this.api_url}${url}`, {
      headers: this.headers,
      body: JSON.stringify(data),
    });
  }

  get headersForUpload(): HttpHeaders {
    const headersConfig = {
      "Content-Type": "multipart/form-data",
    };

    return new HttpHeaders(headersConfig);
  }

  get headers(): HttpHeaders {
    const headersConfig = {
      "Content-Type": "application/json",
    };

    return new HttpHeaders(headersConfig);
  }
}
