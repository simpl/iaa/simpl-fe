import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { EuiDropdownModule } from '@eui/components/eui-dropdown';
import { EuiButtonModule } from '@eui/components/eui-button';
import { EuiLabelModule } from '@eui/components/eui-label';
import { EuiIconModule } from '@eui/components/eui-icon';
import { EuiInputGroupModule } from '@eui/components/eui-input-group';
import { EuiInputTextModule } from '@eui/components/eui-input-text';
import { EuiSelectModule } from '@eui/components/eui-select';
import { StopPropagationDirective } from '@fe-simpl/core/directives';
import { EuiChipModule } from '@eui/components/eui-chip';
import { EuiDatepickerModule } from '@eui/components/eui-datepicker';
import { EuiDateRangeSelectorModule } from '@eui/components/eui-date-range-selector';
import { FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';

@Component({
  selector: 'lib-table-filters',
  standalone: true,
  imports: [
    CommonModule,
    EuiDropdownModule,
    EuiButtonModule,
    EuiLabelModule,
    EuiIconModule,
    EuiInputGroupModule,
    EuiInputTextModule,
    EuiSelectModule,
    StopPropagationDirective,
    EuiChipModule,
    EuiDatepickerModule,
    EuiDateRangeSelectorModule,
    ReactiveFormsModule
  ],
  templateUrl: './table-filters.component.html',
  styleUrl: './table-filters.component.css',
})
export class TableFiltersComponent implements OnInit {

  @Input({ required: false })
  filters: any[] = [
    {label: 'Nome', type: 'text', placeholder: 'Inserisci il nome'},
    {label: 'Data', type: 'date', placeholder: 'Inserisci la data'}
  ];

  @Output() filtersApplied = new EventEmitter<any>();

  filtersFormGroup: FormGroup;
  selectedFilters: any[] = [];

  ngOnInit() {
    this.filtersFormGroup = new FormGroup({
      filter: new FormControl('', [Validators.required]),
      value:  new FormControl('', [Validators.required])
    })
  }

  onChangeDropdownStatus(event: boolean) {
  }

  onAddFilter() {
    this.selectedFilters.push(this.filtersFormGroup.getRawValue());
    this.filtersApplied.emit(this.selectedFilters);
    this.filtersFormGroup.reset();
  }
}


