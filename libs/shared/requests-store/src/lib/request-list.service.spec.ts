import { TestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { RequestListService } from './request-list.service';
import { ApiService, API_URL } from '@fe-simpl/api';
import {
  PagedParticipantDto,
  RequestListProperties,
} from '@fe-simpl/api-types';
import { of } from 'rxjs';

describe('RequestListService', () => {
  let service: RequestListService;
  let httpTestingController: HttpTestingController;
  let mockApiService: jest.Mocked<ApiService>;

  const apiUrl = 'https://mock-api-url.com';

  beforeEach(() => {
    mockApiService = {
      get: jest.fn(),
    } as unknown as jest.Mocked<ApiService>;

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        RequestListService,
        { provide: ApiService, useValue: mockApiService },
        { provide: API_URL, useValue: apiUrl },
      ],
    });

    service = TestBed.inject(RequestListService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
    jest.clearAllMocks();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should call the API with the correct parameters', () => {
    const mockResponse: PagedParticipantDto = {
      content: [],
      page: { number: 0, totalPages: 1, totalElements: 0, size: 5 },
    };
    const params: RequestListProperties = {
      email: 'test@mail.com',
      status: 'APPROVED',
      page: 0,
      size: 10,
      sort: ['name,asc'],
    };

    mockApiService.get.mockReturnValueOnce(of(mockResponse));
    service.search(params).subscribe((response) => {
      expect(response).toEqual(mockResponse);
    });

    expect(mockApiService.get).toHaveBeenCalledWith(
      `/onboarding-api/onboarding-request`,
      expect.anything()
    );
  });

  it('should call the API with default parameters if no params are provided', () => {
    const mockResponse: PagedParticipantDto = {
      content: [],
      page: { number: 0, totalPages: 1, totalElements: 0, size: 5 },
    };
    mockApiService.get.mockReturnValueOnce(of(mockResponse));
    service.search().subscribe((response) => {
      expect(response).toEqual(mockResponse);
    });

    expect(mockApiService.get).toHaveBeenCalledWith(
      `/onboarding-api/onboarding-request`,
      expect.anything()
    );
  });

  it('should send a patch request with the correct parameters for approval', () => {
    const mockRequestId = '123';
    const mockIdAttrList = ['attr1', 'attr2'];
    const mockRejectionCause = '';
    const mockResponse = {};

    service
      .approveParticipant(
        mockRequestId,
        true,
        mockIdAttrList,
        mockRejectionCause
      )
      .subscribe((response) => {
        expect(response).toEqual(mockResponse);
      });

    const req = httpTestingController.expectOne(
      `${apiUrl}/onboarding-api/onboarding-request/${encodeURI(
        mockRequestId
      )}/status?identityAttributesExcluded=attr1,attr2`
    );
    expect(req.request.method).toBe('PATCH');
    expect(req.request.body).toEqual({ value: 'APPROVED' });

    req.flush(mockResponse);
  });

  it('should send a patch request with the correct parameters for rejection', () => {
    const mockRequestId = '123';
    const mockIdAttrList: string[] = [];
    const mockRejectionCause = 'Invalid documents';
    const mockResponse = {};

    service
      .approveParticipant(
        mockRequestId,
        false,
        mockIdAttrList,
        mockRejectionCause
      )
      .subscribe((response) => {
        expect(response).toEqual(mockResponse);
      });

    const req = httpTestingController.expectOne(
      `${apiUrl}/onboarding-api/onboarding-request/${encodeURI(
        mockRequestId
      )}/status?rejectionCause=${encodeURIComponent(mockRejectionCause)}`
    );
    expect(req.request.method).toBe('PATCH');
    expect(req.request.body).toEqual({ value: 'REJECTED' });

    req.flush(mockResponse);
  });
});
