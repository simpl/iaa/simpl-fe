import { API_URL } from "@fe-simpl/api";
import { Translation, TranslocoTestingOptions } from "@jsverse/transloco";
import enTranslationsOnboarding from "apps/onboarding/src/assets/i18n/en.json";
import enTranslationsParticipantUtility from "apps/participant-utility/src/assets/i18n/en.json";
import enTranslationsSap from "apps/sap/src/assets/i18n/en.json";
import enTranslationsUsersAndRoles from "apps/usersandroles/src/assets/i18n/en.json";

export enum AppDomain {
  ONBOARDING = "Onboarding",
  PARTICIPANT_UTILITY = "ParticipantUtility",
  SAP = "Sap",
  USERS_AND_ROLES = "UsersAndRoles",
}

export const translocoTestConfiguration = (
  domain?: AppDomain,
  translation?: any
): TranslocoTestingOptions => {
  // Loading correspondant json based on which app the test is from
  // If no app is given, try to check is a custom translation is given
  let translationsJson: Translation = {};
  switch (domain) {
    case AppDomain.ONBOARDING:
      translationsJson = enTranslationsOnboarding;
      break;
    case AppDomain.PARTICIPANT_UTILITY:
      translationsJson = enTranslationsParticipantUtility;
      break;
    case AppDomain.SAP:
      translationsJson = enTranslationsSap;
      break;
    case AppDomain.USERS_AND_ROLES:
      translationsJson = enTranslationsUsersAndRoles;
      break;
    default:
      translationsJson = translation ?? {};
      break;
  }
  return {
    translocoConfig: {
      availableLangs: ["en"],
      defaultLang: "en",
    },
    langs: {
      en: translationsJson,
    },
  };
};

export const provideApiUrl = {
  provide: API_URL,
  useValue: {
    production: false,
    api_Url: "default",
    keycloakConfig_url: "default",
    keycloakConfig_realm: "default",
    keycloakConfig_clientId: "default",
  },
};
