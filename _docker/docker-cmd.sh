#!/usr/bin/env bash

function update_all_language_files() {
  find  /usr/share/nginx/html/ -type f -path "*/assets/*" -name "*.json" | (
    while read line; do
      envsubst '$DATASPACE_NAME' < $line > /tmp/langf;
      mv /tmp/langf $line;
    done;
  )
}

export THE_APP=${APPLICATION:-onboarding}
export APP_BASE_PATH=${APP_BASE_PATH:-$THE_APP}
export DATASPACE_NAME=${DATASPACE_NAME:-"SIMPL"}

echo "..${THE_APP}.."
echo "..${APP_BASE_PATH}.."

envsubst < /usr/share/nginx/html/"${THE_APP}"/assets/env.template.js > /usr/share/nginx/html/"${THE_APP}"/assets/env.js && \
envsubst '$THE_APP $APP_BASE_PATH' < /nginx.conf.template > /etc/nginx/nginx.conf
update_all_language_files

# Change base href in index.html
sed \
    -iv '/<base/{s!href="[^"]*"!href="'"$APP_BASE_PATH"'/"!;s!//!/!}' \
    "/usr/share/nginx/html/${THE_APP}/index.html"

exec nginx -g 'daemon off;'
